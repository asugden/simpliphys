# README #

Simpliphys is a python program designed to simplify the analysis of electrophysiological data. Simply put all of your recordings associated with cells or pairs of cells in a single folder. Add metadata file describing the cells and daily metadata files describing the experiments. All metadata then become accessible for selecting subsets of your data. The power of Simpliphys comes from analysis modules, designed to run on a single trace, a single file, or on an entire cell. Example modules are included. Update these to match your own data or write your own. By using the file routines that have been already written, it's simple to measure all sorts of parameters associated with your files.

After selecting particular files, you can pull out X and Y values to print or graph. No more copying and pasting values from PClamp!

Simpliphys can handle Axon ABF and Cambridge CES files. Support for these files is provided by the STFIO library.

### What is this repository for? ###

* Use for individual or paired recordings in voltage or current clamp
* Version 1.1
### How do I get set up? ###

* Install Python version 2.7 and the following modules:
numpy, matplotlib, stfio

* Then, simply run the Simpliphys file and interact with it at the command line. The command help within Simpliphys describes all functions

