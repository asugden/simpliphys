# Updated: 141023

# Extra function added on is getpair
# The path to save the file is already in the args passed to a premadeplot

# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Cross_Correlation(object):
	# Only the init function is called.
	def __init__(self, args, f, p, tr, grapher):
		self.grapher = grapher
		self._graph(args, f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v', 'white ramp 0.5-5v', 'white ramp 5v', 'blue ramp 1-5v', 'blue ramp 0.5-5v', 'blue ramp 5v', 'white ramp 4v', 'scott ramp'],
		'amplifier': ['A']
	}

	# ================================================================================== #
	# ANYTHING ELSE

	def _color(self, p):
		genotype = {
			'cx36ko': 'red',
			'cx36wt': 'gray',
		}
		region = {
			'ventral': 'orange',
		}

		if p['genotype'] in genotype: clr = genotype[p['genotype']]
		elif p['region'] in region: clr = region[p['region']]
		else: clr = 'gray'

		return self.grapher.color(clr)

	def _graph(self, args, f, p, tr):
		p2, tr2 = self.getpair()
		if tr2 < 0: return False

		if p['clamp'] == 'voltage' and p2['clamp'] == 'voltage':
			if p['vclamp'] == p2['vclamp']:
				args['title'] = 'Cross-correlation of cells %s and %s|voltage clamped at %i mV with %.3f mW LED' % (p['cell-id'], p2['cell-id'], p['vclamp'], p['milliwatts-led-white'][tr])
			else:
				args['title'] = 'Cross-correlation of cells %s at %i mV and|%s at %i mV with %.3f mW LED' % (p['cell-id'], p['vclamp'], p2['cell-id'], p2['vclamp'], p['milliwatts-led-white'][tr])

		args['xtitle'] = 'Time (ms)'
		args['ytitle'] = 'Cross Correlation'

		fig, ax = self.grapher.startgraph(args)

		# Select which amplifier we want first
		myamp = f.amplifier()
		otheramp = (myamp + 1)%2

		t, ccorl = f.correlation(tr, amp1=myamp, amp2=otheramp, trange=self._times(p, tr), tunits='s', highpass=3)
		t = t*1000
		ax.axvline(0, 0, 1, linewidth=1, color='black', alpha=0.5)
		ax.plot(t, ccorl, color=self._color(p), linewidth=0.5)
		ax.fill_between(t, 0, ccorl, facecolor=self._color(p), alpha=0.3)

		ax.set_xlim([-200, 200])

		ax.set_xlabel('Time (ms)')
		ax.set_ylabel('Cross Correlation')

		self.grapher.finishgraph(fig, ax, args)

	def _times(self, p, tr):
		times = {
			'white ramp 1-5v': [(1.518, 4), (1.267, 4), (1.181, 4), (1.139, 4), (1.112, 4)],
			'white ramp 0.5-5v': [(1.717, 4), (1.518, 4), (1.371, 4), (1.267, 4), (1.209, 4), (1.181, 4), (1.154, 4), (1.139, 4), (1.132, 4), (1.112, 4)],
			'white ramp 4v': [(1.139, 4) for i in range(10)],
			'white ramp 5v': [(1.112, 4) for i in range(10)],
			'blue ramp 1-5v': [(1, 4) for i in range(5)],
			'blue ramp 0.5-5v': [(1, 4) for i in range(10)],
			'blue ramp 5v': [(1, 4) for i in range(10)],
			'scott ramp': [(4, 7) for i in range(30)],
		}
		return times[p['protocol']][tr]
