# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Gamma_Spectrum_Scott(object):
	# Only the init function is called.
	def __init__(self, args, file, protocol, trace, grapher):
		self.grapher = grapher
		self._graph(args, file, protocol, trace)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v', 'white ramp 0.5-5v', 'white ramp 5v', 'blue ramp 1-5v', 'blue ramp 0.5-5v', 'blue ramp 5v', 'white ramp 4v', 'scott ramp'],
	}

	# ================================================================================== #
	# ANYTHING ELSE

	def _graph(self, args, f, p, tr):
		fig, ax = self.grapher.startgraph(args)
		color = 'red' if p['genotype'] == 'cx36ko' else 'gray' if p['genotype'] == 'cx36wt' else 'orange' if p['region'] == 'ventral' else 'gray'
		color = self.grapher.color(color)
		spectrum = f.powerspectrum(tr, trange=p['gamma-scott-time'][tr], frange=(10, 100), tunits='s')
		ax.plot(spectrum[0], spectrum[1], color=color, linewidth=0.5)
		ax.fill_between(spectrum[0], 0, spectrum[1], facecolor=color, alpha=0.3)
		ax.set_xlim([10, 100])
		ax.set_xlabel('Frequency (Hz)')
		ax.set_ylabel('Power (pA$^2$/Hz)')
		self.grapher.finishgraph(fig, ax, args)
