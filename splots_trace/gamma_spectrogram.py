import matplotlib as mpl
import matplotlib.pyplot as plt

# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Gamma_Spectrogram(object):
	# Only the init function is called.
	def __init__(self, args, file, protocol, trace, grapher):
		self.grapher = grapher
		self._graph(args, file, protocol, trace)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v', 'white ramp 0.5-5v', 'white ramp 5v', 'blue ramp 1-5v', 'blue ramp 0.5-5v', 'blue ramp 5v', 'white ramp 4v', 'scott ramp'],
	}

	# ================================================================================== #
	# ANYTHING ELSE

	def _graph(self, args, f, p, tr):
		fig, ax = self.grapher.startgraph(args)

		color = 'red' if p['genotype'] == 'cx36ko' else 'gray' if p['genotype'] == 'cx36wt' else 'orange' if p['region'] == 'ventral' else 'gray'
		color = self.grapher.color(color)

		times, freqs, power = f.spectrogram(tr, ms=500, frange=(10, 100), overlap=0.95)
		if 'max' in args and args['max'] > 0:
			im = ax.pcolormesh(times, freqs, power, vmin=0, vmax=int(args['max']), cmap=self._jetonwhite())
		else:
			im = ax.pcolormesh(times, freqs, power, vmin=0, cmap=self._jetonwhite())
		fig.colorbar(im)

		if 'xtitle' not in args or args['xtitle'] == '': args['xtitle'] = 'Time (s)'
		if 'ytitle' not in args or args['ytitle'] == '': args['ytitle'] == 'Frequency (Hz)'
		args['xmin'] = 4 if p['protocol'] == 'scott ramp' else 1
		args['xmax'] = 7 if p['protocol'] == 'scott ramp' else 4.5
		args['ymin'] = 10
		args['ymax'] = 100

#		ax.set_xlim([1, 7])
#		ax.set_ylim([10, 100])
#		ax.set_xlabel()
		ax.set_ylabel('Frequency (Hz)')
		self.grapher.finishgraph(fig, ax, args)


	# Makes a color map with the span of jet PLUS black
	def _jetonwhite(self):
		supmap = {
			'red': (
				(0.0, 1, 1),
				(0.5, 0, 0), # Mapped with (1 - (1 - old)*0.9)
				(1.0, 1, 1), # Moved Jet start here
			),
			'green': (
				(0.0, 1, 1),
				(0.5, 0, 0),
				(1.0, 0, 0),
			),
			'blue': (
				(0.0, 1, 1),
				(0.5, 0, 0),
				(1.0, 0, 0), # Moved Jet start here
			)
		}

		return mpl.colors.LinearSegmentedColormap('my_colormap', supmap, 256)
#		return plt.get_cmap('binary')

	# Makes a color map with the span of jet PLUS black
	def _jetonblack(self):
		supmap = {
			'red': (
				(0., 0, 0),
				(0.050, 0, 0), # Moved Jet start here
				(0.383, 0, 0), # Mapped with (1 - (1 - old)*0.9)
				(0.677, 1, 1),
				(0.896,1, 1),
				(1, 0.5, 0.5)
			),
			'green': (
				(0., 0, 0),
				(0.050, 0, 0), # Moved Jet start here
				(0.169,0, 0),
				(0.406,1, 1),
				(0.658,1, 1),
				(0.915,0,0),
				(1, 0, 0)
			),
			'blue': (
				(0., 0, 0),
				(0.050, 0.5, 0.5), # Moved Jet start here
				(0.156, 1, 1),
				(0.373, 1, 1),
				(0.668,0, 0),
				(1, 0, 0)
			)
		}

		return mpl.colors.LinearSegmentedColormap('my_colormap', supmap, 256)
