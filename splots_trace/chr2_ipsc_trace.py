import matplotlib as mpl

# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class PSC_Trace(object):
	# Only the init function is called.
	def __init__(self, args, file, protocol, trace, grapher):
		self._graph(args, file, protocol, trace, grapher)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['blue pulses', 'blue pulses no analog'],
		'genotype':['somchr2', 'vgatchr2', 'pvchr2', 'vipchr2', 'gad2chr2'],
	}

	# ================================================================================== #
	# ANYTHING ELSE

	def _graph(self, args, f, p, tr, grapher):
		fig, ax = grapher.startgraph(args)
		#ax = self._noaxis(ax)

		fstart = 180 # beginning of figure in ms
		fend = 300 # end of figure in ms

		psc = f.fastpsc(traceN=tr, trange=(200, 300), tunits='ms', direction='inward')

		x, p = f.plot(tr, (fstart, fend), tunits='ms')
		ax.plot(x, p, color='gray', linewidth=0.5)

		x, lp = f.plot(tr, (fstart, fend), tunits='ms', lowpass=1000)
		ax.plot(x, lp, color='black', linewidth=1, alpha=0.8)

		if psc['psc']:
			xpsc, ypsc = f.plotpsc(psc['a-fit'], psc['rise-phase-fit'], psc['fall-phase-fit'], psc['onset-ms'], psc['offset-ms'])
			ax.plot(xpsc, psc['polarity']*ypsc + psc['baseline'], dashes=[6, 8], dash_capstyle='round', color='blue', linewidth=4, alpha=0.5)

		rec = mpl.patches.Rectangle((200, ax.get_ylim()[0]), 5, ax.get_ylim()[1] - ax.get_ylim()[0], color='#4AC4ED', alpha=0.5)
		ax.add_patch(rec)

		ax.set_xlim([fstart, fend])
		grapher.finishgraph(fig, ax, args)

	# Set axis to just a scale bar. Ref: https://gist.github.com/dmeliza/3251476
	def _noaxis(self, ax):
		ax.axis('off')
		return ax
