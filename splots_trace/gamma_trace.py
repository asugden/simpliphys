import math

# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Gamma_Trace(object):
	# Only the init function is called.
	def __init__(self, args, file, protocol, trace, grapher):
		self._graph(args, file, protocol, trace, grapher)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v', 'white ramp 0.5-5v', 'white ramp 5v', 'blue ramp 1-5v', 'blue ramp 0.5-5v', 'blue ramp 5v', 'white ramp 4v', 'scott ramp'],
	}

	# ================================================================================== #
	# ANYTHING ELSE

	def _graph(self, args, f, p, tr, grapher):
		fig, ax = grapher.startgraph(args)

		ax = self._noaxis(ax)
		x, p = f.plot(tr, (1, 4.5), tunits='s')
		ax.plot(x, p, color='gray', linewidth=0.5)
		ax.set_xlim([1, 4.5])

		ylims = ax.get_ylim()
		yrange = ylims[1] - ylims[0]
		yscalebar = math.pow(10, math.floor(math.log10(yrange/2.0)))
		yscale = 1.0 - yscalebar/yrange

		ax.axvline(1.1, yscale, 1, linewidth=1, color='black', alpha=0.5)
		print 'Y Scalebar is %i pA' % (int(yscalebar))
		print 'X is 3.5 s total'

		grapher.finishgraph(fig, ax, args)

	# Set axis to just a scale bar. Ref: https://gist.github.com/dmeliza/3251476
	def _noaxis(self, ax):
		ax.axis('off')
		return ax
