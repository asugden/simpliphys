# All cell classes are ONLY given all of the protocols of the cell

import numpy as np

# Updated: 141110
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class CellResistance(object):
	def __init__(self, ps): pass

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol': [['a steps d50', {'amplifier':'a'}], ['b steps d50', {'amplifier':'b'}]],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	# Although the potential is in metadata, recalculate it
	# Also, we use the steps d50 because it's usually the first thing run

	def resistance_best(self, ps):
		res = -1
		for p in ps:
			if res == -1 and 'resistance-steps' in p: res = p['resistance-steps']
			if 'resistance' in p: res = p['resistance']
		return res

	def tau_best(self, ps):
		tau = -1
		for p in ps:
			if tau == -1 and 'tau-steps' in p: tau = p['tau-steps']
			if 'tau' in p: tau = p['tau']
		return tau

	def capacitance_best(self, ps):
		cap = -1
		for p in ps:
			if cap == -1 and 'capacitance-steps' in p: cap = p['capacitance-steps']
			if 'capacitance' in p: cap = p['capacitance']
		return cap

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

