# All cell classes are ONLY given all of the protocols of the cell

# Updated: 150307
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class GammaMax(object):
	def __init__(self, ps):
		self.gmax = -1.0
		self.fmax = 'none'
		self.lmax = -1.0
		for p in ps:
			if 'gamma-max' in p:
				for i, v in enumerate(p['gamma-max']):
					if isinstance(v, float) and v > self.gmax:
						self.gmax = v
						self.fmax = str(p['file-number']) + '-' + str(i)
						self.lmax = p['milliwatts-led-white'][i] if 'milliwatts-led-white' in p else -1

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':[
			'white ramp 1-5v',
			'white ramp 0.5-5v',
			'white ramp 5v',
			'blue ramp 1-5v',
			'blue ramp 0.5-5v',
			'blue ramp 5v',
			'white ramp 4v',
			'scott ramp',
		],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def gamma_max_cell(self, ps): return self.gmax

	def gamma_max_cell_id(self, ps): return self.fmax

	def gamma_max_cell_light(self, ps): return self.lmax

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

