# All file classes get file, protocol, and traces. Skipped traces are hidden.
# Traces will always be sequential

import numpy as np
from scipy import optimize

# Updated: 141108
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Stepsd50File(object):
	def __init__(self, f, p, trs):
		self.rh = self._rheo(f, p, trs)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol': [['a steps d50', {'amplifier':'a'}], ['b steps d50', {'amplifier':'b'}]],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	# Although the potential is in metadata, recalculate it
	# Also, we use the steps d50 because it's usually the first thing run
	def membrane_potential(self, f, p, trs):
		bl, stdev = f.baseline(trs[0], self._times()[0], 'ms')
		return bl

	# AHP burst is only at -150 pA
	def ahp_burst(self, f, p, trs):
		if p['current'][0] != -150: return 0

		# Count the spikes and return them otherwise
		# Trace 0
		spikes = f.spikes(0, (self._times()[1], self._times()[1] + 100), 'ms')
		return len(spikes)

	# Sag only defined at -150 pA
	def sag(self, f, p, trs):
		if p['current'][0] != -150: return 0

		# Calculate the min and max over the course of the negative injected current
		# Trace 0
		(mn, mx) = f.minmax(0, self._times(), 'ms')
		# Get the average value at the very end of the negative injection
		end, stdev = f.baseline(0, self._times()[1], 'ms')

		# Return a sag that's guaranteed to be >= 0
		return max(0, end - mn)

	# Calculate rheobase
	def rheobase(self, f, p, trs): return p['current'][self.rh]

	def spike_width(self, f, p, trs): return p['spike-width-trace'][self.rh]

	def spike_freq_adaptation_median(self, f, p, trs):
		adaps = [i for i in p['spike-freq-adaptation'] if i > 0]
		return np.median(adaps) if len(adaps) > 0 else -1

	def spike_height_adaptation_slope(self, f, p, trs): return self._sha_fit(f, p, trs)

	def spike_freq_slope(self, f, p, trs): return self._sf_fit(f, p, trs)

	def spike_height(self, f, p, trs):
		if p['spike-height-trace'][self.rh] > 0: return p['spike-height-trace'][self.rh]
		else:
			for sh in p['spike-height-trace'][self.rh:]:
				if sh > 0: return sh
			return -1


	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	# Return the times
	def _times(self): return (100, 700)

	# Get the rheobase
	def _rheo(self, f, p, trs):
		rheo = -1
		for tr in trs:
			if p['spike-n'][tr] > 0 and p['current'][tr] > 0 and rheo == -1:
				rheo = tr
				break;
		return rheo

	# Get the best fit line for the spike-height adaptation
	def _sha_fit(self, f, p, trs):
		def fit(x, y): # trace, baseline
			# All of the values go to position 0, 1 so fix B
			fitfun = lambda ad, x: -1*ad[0]*x + 1.0
			errfun = lambda ad, x, y: fitfun(ad, x) - y # Error function
			adcalc, success = optimize.leastsq(errfun, np.array((0.00083)), args=(x, y))
			return adcalc[0]

		x = np.array([p['current'][i] for i in trs if p['spike-height-adaptation'][i] > 0])
		y = np.array([p['spike-height-adaptation'][i] for i in trs if p['spike-height-adaptation'][i] > 0])
		return fit(x, y)*100.0 if len(x) > 2 else -1

	# Get a best fit line to the spike frequency
	def _sf_fit(self, f, p, trs):
		def fit(x, y): # trace, baseline
			# All of the values go to position 0, 0 so set B to 0
			fitfun = lambda ad, x: ad[0]*x
			errfun = lambda ad, x, y: fitfun(ad, x) - y # Error function
			adcalc, success = optimize.leastsq(errfun, np.array((0.15)), args=(x, y))
			return adcalc[0]

		x = np.array([p['current'][i] for i in trs if p['spike-freq'][i] > 0])
		y = np.array([p['spike-freq'][i] for i in trs if p['spike-freq'][i] > 0])
		return fit(x, y) if len(x) > 2 else -1
