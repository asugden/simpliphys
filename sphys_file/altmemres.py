# All file classes get file, protocol, and traces. Skipped traces are hidden.
# Traces will always be sequential

import numpy as np

# Updated: 141110
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class AltMemResistance(object):
	def __init__(self, f, p, trs):
		self.pars = self._amr(f, p, trs)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol': ['alternating membrane resistance'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	# Although the potential is in metadata, recalculate it
	# Also, we use the steps d50 because it's usually the first thing run

	def resistance(self, f, p, trs): return self.pars['res']

	def tau(self, f, p, trs): return self.pars['tau']

	def capacitance(self, f, p, trs): return self.pars['cap']


	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _times(self): return (100, 700)

	# Get the alternating traces with current injections and return the average injected
	# current
	def _currenttraces(self, f, p, trs):
		f.read('voltage')
		c = []
		safetrs = []
		for tr in trs:
			up, stdu = f.average(tr, (0, self._times()[0]), tunits='ms')
			dn, stdd = f.average(tr, self._times(), tunits='ms')
			if abs(up - dn) > 4.0:
				c.append(abs(up - dn))
				safetrs.append(tr)
		f.read('current')
		return safetrs, np.mean(np.array(c))

	def _amr(self, f, p, trs):
		# Get the alternating traces and the injected current
		trs, c = self._currenttraces(f, p, trs)

		# Skip any wild traces
		stds = []
		for tr in trs:
			up, stdu = f.average(tr, (0, self._times()[0]), tunits='ms')
			dn, stdd = f.average(tr, (self._times()[1] - 100, self._times()[1]), tunits='ms')
			stds.append(max(stdu, stdd))

		safe = np.array(trs)[np.where(np.array(stds) < 3*np.min(stds))]
		tau, vchange = f.tau(safe, self._times(), 'ms')

		return {
			'res': abs(vchange/c*1000),
			'tau': tau,
			'cap': abs(tau/(vchange/c)),
		}
