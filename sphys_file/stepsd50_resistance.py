# All file classes get file, protocol, and traces. Skipped traces are hidden.
# Traces will always be sequential

import numpy as np

# Updated: 141110
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class StepsResistance(object):
	def __init__(self, f, p, trs):
		self.pars = self._amr(f, p, trs)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol': [['a steps d50', {'amplifier':'a'}], ['b steps d50', {'amplifier':'b'}]],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	# Although the potential is in metadata, recalculate it
	# Also, we use the steps d50 because it's usually the first thing run

	def resistance_steps(self, f, p, trs): return self.pars['res']

	def tau_steps(self, f, p, trs): return self.pars['tau']

	def capacitance_steps(self, f, p, trs): return self.pars['cap']


	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _times(self): return (100, 700)

	# Get the alternating traces with current injections and return the average injected
	# current
	def _currenttrace(self, f, p, trs):
		f.read('voltage')
		c = []
		for tr in trs:
			up, stdu = f.average(tr, (0, self._times()[0]), tunits='ms')
			dn, stdd = f.average(tr, self._times(), tunits='ms')
			cur = int(round((up - dn)/10))*10

			if cur < 60 and cur > 0: c.append((up - dn, tr))

		f.read('current')
		if len(c) == 0: return (-1, -1)
		return min(c)

	def _amr(self, f, p, trs):
		# Get the alternating traces and the injected current
		c, tr = self._currenttrace(f, p, trs)
		if tr == -1: return {'res': -1, 'tau': -1, 'cap': -1}

		tau, vchange = f.tau(tr, self._times(), 'ms')

		return {
			'res': abs(vchange/c*1000),
			'tau': tau,
			'cap': abs(tau/(vchange/c)),
		}
