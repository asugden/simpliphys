# WARNING: Meta charts are hard to work with. Unlike all other analysis and chart types,
# these are designed to be highly flexible. With flexibility comes danger. Save paths are
# not assigned in advance. Arguments are not checked. Only use this style if necessary.

# This class has a built-in method, self.file(protocol), which takes a protocol as input
# and returns a tuple of a file and the traces that can be referenced.

# This class also has a built-in variable, path, which is the base path with which to
# save output.

import numpy as np

# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Cross_Correlation_Multi(object):
	# Only the init function is called.
	# You are given a list of arguments including the default chart arguments and
	# anything else included in the command line. Also, a list of all protocols and a
	# list of all of the cells that the protocols cover. Finally, the grapher object.
	def __init__(self, args, ps, cells, grapher):
		self.grapher = grapher

		self.trange = (-200, 200)
		if ('xmin' in args and
			(isinstance(args['xmin'], int) or isinstance(args['xmin'], float)) and
			args['xmin'] < -10):
			self.trange = (args['xmin'], self.trange[1])
		if ('xmax' in args and
			(isinstance(args['xmax'], int) or isinstance(args['xmax'], float)) and
			args['xmax'] < -10):
			self.trange = (self.trange[0], args['xmax'])
		self._cc(args, ps, cells)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':[
			'white ramp 1-5v',
			'white ramp 0.5-5v',
			'white ramp 5v',
			'blue ramp 1-5v',
			'blue ramp 0.5-5v',
			'blue ramp 5v',
			'white ramp 4v',
			'scott ramp',
		],
	}

	# ================================================================================== #
	# ANYTHING ELSE

	def _cc(self, args, ps, cells):
		separator = ''
		if 'by' in args: separator = args['by']
		print 'Plotting by %s' % (separator)

		ccorls = self._getccs(ps, separator)
		bys = [g for g in ccorls]
		f, trs = self.file(ps[0])

		if 'save' in args and len(args['save']) > 0: args['save'] = self.path + args['save']

		args['xtitle'] = 'Time (ms)'
		args['ytitle'] = 'Cross Correlation'

		fig, ax = self.grapher.startgraph(args)
		ax.axvline(0, 0, 1, linewidth=1, color='black', alpha=0.5)

		for g in bys:
			x, y = self._averageccs(ccorls[g])
			x, y = f.simplify(x, y, width=1500)

			ax.plot(x, y, color=self._color(g), linewidth=0.5)
			ax.fill_between(x, 0, y, facecolor=self._color(g), alpha=0.3)

		ax.set_xlim([-200, 200])

		ax.set_xlabel('Time (ms)')
		ax.set_ylabel('Cross Correlation')

		self.grapher.finishgraph(fig, ax, args)


	def _getccs(self, ps, separator):
		out = {}

		for i, p in enumerate(ps):
			print 'Cross-correlating file %i of %i' % (i + 1, len(ps))
			if 'gamma-max-cell-id' in p and p['gamma-max-cell-id'].split('-')[0] == str(p['file-number']):
				tr = int(p['gamma-max-cell-id'].split('-')[1])
				f, trs = self.file(p)

				if tr in trs:
					amp1 = f.amplifier()
					amp2 = (amp1 + 1)%2
					t, ccorl = f.correlation(tr, amp1, amp2, trange=self._times(p, tr), tunits='s', highpass=3, width=-1)

					by = 'extra' if len(separator) == 0 or separator not in p else p[separator]
					if by not in out:
						out[by] = [(t*1000, ccorl)]
					else:
						out[by].append((t*1000, ccorl))
		return out

	def _averageccs(self, l):
		mn, mx, pmx, pmn, pln, dtm = self.trange[0], self.trange[1], 0, 0, 0, 0

		for t in l:
			dt = t[0][1] - t[0][0]
			if dtm == 0: dtm = dt
			elif abs(dtm - dt) > 0.0001: print 'WARNING: dt problems ahead %f, %f.' % (dt, dtm)

			mn = np.min(t[0]) if np.min(t[0]) > mn or mn == 0 else mn
			mx = np.max(t[0]) if np.max(t[0]) < mx or mx == 0 else mx

			strt = np.where(np.abs(t[0] - mn) < 0.0001)[0][0]
			end = np.where(np.abs(t[0] - mx) < 0.0001)[0][0]

			pmn = strt if str > pmn or pmn == 0 else pmn
			pmx = strt if str < pmx or pmx == 0 else pmx
			pln = end - strt if end - strt < pln or pln == 0 else pln

		y = np.zeros(pln)

		for i, t in enumerate(l):
			strt = np.where(np.abs(t[0] - mn) < 0.0001)[0][0]
			end = np.where(np.abs(t[0] - mx) < 0.0001)[0][0]
			if end - strt != pln:
				if strt + pln >= len(t[0]):
					end = strt + pln
				else:
					strt = end - pln

			if i == 0: x = t[0][strt:end]
			y += t[1][strt:end]/len(l)

		return x, y


	def _color(self, g):
		genotype = {
			'cx36ko': 'red',
			'cx36wt': 'gray',
		}
		region = {
			'ventral': 'orange',
		}

		if g in genotype: clr = genotype[g]
		else: clr = 'gray'

		return self.grapher.color(clr)


	def _times(self, p, tr):
		times = {
			'white ramp 1-5v': [(1.518, 4), (1.267, 4), (1.181, 4), (1.139, 4), (1.112, 4)],
			'white ramp 0.5-5v': [(1.717, 4), (1.518, 4), (1.371, 4), (1.267, 4), (1.209, 4), (1.181, 4), (1.154, 4), (1.139, 4), (1.132, 4), (1.112, 4)],
			'white ramp 4v': [(1.139, 4) for i in range(10)],
			'white ramp 5v': [(1.112, 4) for i in range(10)],
			'blue ramp 1-5v': [(1, 4) for i in range(5)],
			'blue ramp 0.5-5v': [(1, 4) for i in range(10)],
			'blue ramp 5v': [(1, 4) for i in range(10)],
			'scott ramp': [(4, 7) for i in range(30)],
		}
		return times[p['protocol']][tr]
