import numpy as np
import os.path as opath
from os import mkdir, listdir
from Errors import *

# Open AMetadata with path for analysis and protocols
class AMetadata():
	def __init__(self, path, protocols):
		self.pwd = path if path[-1] == '/' else path + '/'
		self.protocols = protocols

	# Return protocols after analyzing
	def getprotocols(self): return self.protocols

	# Check if is analysis file
	def isanalysis(self, path): return True if path[-7:] == '-an.txt' else False

	# Retrieve all files with a recursive function based on the directory from Metadata
	def readfiles(self, path=''):
		if path == '': path = self.pwd
		if opath.exists(path):
			fs = listdir(path)
			for file in fs:
				file = path + file
				if self.isanalysis(file): self.addanalysis(file)
				elif opath.isdir(file): self.readfiles(file + '/')
			return self

	# Add a single analysis file to protocols
	def addanalysis(self, path):
		fp = open(path, 'r')
		lines = fp.read().split('\n')
		fp.close()

		id = self.index(path)
		if id > -1:
			for line in lines:
				if line.find(':') > -1:
					line = line.strip().split(':')
					key = line[0][:line[0].find('[')]
					type = line[0].strip()[line[0].find('[')+1:-1]
					self.protocols[id][key] = self.metadata_type(type, line[1].strip())

	# Get index of protocols for writing to with id matching the names of saved files
	def index(self, path):
		if not hasattr(self, 'protocol_index'):
			self.protocol_index = {}
			for j, p in enumerate(self.protocols):
				self.protocol_index['%s-C%2i' % (p['name'], p['cell-number'])] = j

		path = path[path.rfind('/') + 1:-7]
		if path in self.protocol_index: return self.protocol_index[path]
		else: return -1

	# Set the metdata to type, taking into account lists or tuples
	def metadata_type(self, type, val):
		if (val[:2] == '[[' and val[-2:] == ']]'):
			out = []
			vals = val[2:-2].replace('], [', '][').split('][')
			for l in vals:
				out.append([])
				l = l.split(',')
				for v in l: out[-1].append(int(v) if type == 'int' else float(v) if type == 'float' else str(v))
			return out
		elif (val[0] == '[' and val[-1] == ']') or (val[0] == '(' and val[-1] == ')'):
			out = []
			vals = val[1:-1].split(',')
			for v in vals: out.append(int(v) if type == 'int' else float(v) if type == 'float' else str(v))
			if val[0] == '(': return tuple(out)
			else: return out
		else:
			return int(val) if type == 'int' else float(val) if type == 'float' else str(val)

	# ==================================================================================
	# SAVING VARIABLES

	# Write files to a directory based on analysis type (first name before dash) and experiment name in protocol
	def writefiles(self, updated):
		for up in updated: # up is index of protocol
			updated[up].sort() # sort the protocol keys to be saved
			fo = open(self.analysispath(self.protocols[up]), 'a') # open the protocol index with the experiment type
			for val in updated[up]: # val is protocol key to be saved
				try:
					out, type = self.stringify(self.protocols[up][val])
					fo.write('{:>24s}:{:s}\n'.format('%s[%s]' % (val, type), out))
				except SaveError:
					raise SaveError('Analysis of file %s failed, returning None for %s.' % (self.protocols[up]['name'], val))
			fo.close()

	# Save analysis path- analysis type / experiment / protocol-number -C cell-number -an.txt
	def analysispath(self, protocol):
		spath = self.pwd + protocol['experiment'].lower().replace(' ', '-')
		if not opath.exists(spath): mkdir(spath)
		return spath + '/%s-C%2i-an.txt' % (protocol['name'], protocol['cell-number'])

	# Group updates to be saved by analysis type
	def groupupdates(self, update):
		out = {}
		for up in update:
			key = up.split('-', 1)[0]
			if key in out: out[key].append(up)
			else: out[key] = [up]
		return out

	# Convert val to string, return type, and force everything into 4 sig figs
	def stringify(self, s, vtype='unknown'):
		s = self.listify(s)
		if isinstance(s, list) or isinstance(s, tuple):
			aout = []
			rtype = ''
			for el in s:
				nel, vtype = self.stringify(el, vtype)
				rtype = vtype if rtype == '' or rtype == 'int' and vtype == 'float' or vtype == 'str' else rtype
				aout.append(nel)
			return ('[' + ', '.join(aout) + ']', vtype)
		elif isinstance(s, float) or isinstance(s, int):
			if vtype == 'unknown': vtype = 'float' if isinstance(s, float) else 'int'
			elif vtype == 'int' and isinstance(s, float): vtype = 'float'
			return ('%.4g' % (s), vtype)
		elif s is None:
			raise SaveError('Analysis module returned values of None.')
		else:
			if vtype == 'unknown': vtype = 'str'
			return (s.strip(), vtype)

	# Turn all numpy arrays into lists
	def listify(self, val):
		if type(val).__module__ == np.__name__: val = val.tolist()
		elif isinstance(val, list):
			for i in range(len(val)):
				val[i] = self.listify(val[i])
		return val

def getanalysis(path, type, protocols):
	path = path if path[-1] == '/' else path + '/'
	path = path + type if type[-1] == '/' else path + type + '/'
	am = AMetadata(path, protocols)
	am.readfiles()
	return am.getprotocols()

def setanalysis(path, protocols, updated):
	am = AMetadata(path, protocols)
	am.writefiles(updated)
