from copy import deepcopy
import re, numpy as np

class IOData():
	def __init__(self): pass
	
	defaults = {
		'tolerance': 0.0001, # Tolerance within one should combine xs
		'combine-like-xs': ['mean', 'median', 'max'], # How to combine points with matching Xs
		'skip-zeros': False, # Skip values equal to 0
		'skip-minus-ones': False, # Skip values equal to -1
		'titles': True, # Add titles to the output matrix
		'decimal-places': 2, # Number of decimal places to stringify
	}
	
		
# 	def xorys(self, args, data):
# 		"""Take a dataset and return a matrix of just Y values (or just X values). Used for different measures with single values"""
# 		data = self.combine(args, data)
# 		fby = data[0]['by']
# 		
# 		ys = sorted([(subset[args['xory']], i) for i, subset in enumerate(data)])
# 		bys = sorted([(subset[args['xory']
# 		
		
		
		
	
	def xys(self, args, data):
		"""Take a dataset and return a matrix of Y values by X. Used for the same measure with multiple values."""
		data = self.combine(args, data)
		fx = ''
		fy = ''
		fby = ''
		
		xs = []
		for subset in data:
			if fx == '' or fy == '':
				fx, fy, by = subset['x'], subset['y'], subset['by']

			if subset['x'] == fx and subset['y'] == fy:
				for cell in subset['data']:					
					for x, y in subset['data'][cell]:
						if x not in xs:
							xs.append(x)
						
		xs.sort()
		out = []
		sections = []
		
		if args['titles']: out.append([[''] + [self.stringify(args, p) for p in xs]])
		for subset in data:
			if subset['x'] == fx and subset['y'] == fy:
				sections.append(subset['by'])
				out.append([])
				
				# Sort by cell name in subset
				cells = [cell for cell in subset['data']]
				cells.sort()
				
				for cell in cells:
					ln = [self.stringify(args, cell)] if args['titles'] else []
					# Unpack x, y points
					subset['data'][cell].sort()
					
					p = 0
					for i in range(len(xs)):
						if abs(xs[i] - subset['data'][cell][p][0]) <= args['tolerance']:
							ln.append(self.stringify(args, subset['data'][cell][p][1]))
							p += 1
						else:
							ln.append('')
					
					if len(ln) > 0:
						out[-1].append(ln)

		return (fby, fx, fy), sections, self.justify(out)			
		
	def stringify(self, args, p):
		if isinstance(p, str): return p
		if isinstance(p, float): return '%.{:g}f'.format(args['decimal-places']) % (p)
		if isinstance(p, int): return '%i' % (p)
		else: return str(p)
		
	# Set each value in a column equal to the maximum width of the text in the column
	def justify(self, mtrx):
		mws = [0 for i in mtrx[0][0]]
		
		for group in mtrx:
			for ln in group:
				for i, val in enumerate(ln):
					if len(val) > mws[i]:
						mws[i] = len(val)
						
		for i in range(len(mtrx)): # i is group
			for j in range(len(mtrx[i])): # j is ln
				for k in range(len(mtrx[i][j])): # k is value
					mtrx[i][j][k] = mtrx[i][j][k].rjust(mws[k])		
					
		return mtrx
	
	# Combine like Xs in a dataset, return a copy of the input
	def combine(self, args, data):
		args = self._setargs(args)
		
		out = deepcopy(data)
		for i, subset in enumerate(data):
			for cell in subset['data']:
				x, y = self._order_and_combine_data(subset['data'][cell], args)
				out[i]['data'][cell] = zip(x, y)
		return out
		
	# Add the default arguments to the input arguments
	# Make sure that the arguments are the correct types
	def _setargs(self, args):
		out = {}
		# Set defaults of the argument
		for key in self.defaults:
			if type(self.defaults[key]) == list:
				out[key] = self.defaults[key][0]
			else: out[key] = self.defaults[key]
		
		# Add the input arguments and check them for correction
		for key in args:
			if key in self.defaults:
				if type(self.defaults) == list and args[key].lower() in self.defaults[key]:
					out[key] = args[key].lower()
				else:
					out[key] = args[key]
		return out
	
	# Order X values from low to high and combine Y values with equal Xs or Xs within tolerance
	def _order_and_combine_data(self, data, args):
		data.sort()
		i = 0
		xl = []
		yl = []

		while i < len(data):
			x = data[i][0]
			y = [data[i][1]]
			n = 1
			while i + n < len(data) and (isinstance(data[i + n][0], str) or data[i + n][0] - x <= args['tolerance']):
				if not isinstance(data[i + n][0], str):
					if not args['skip-zeros'] or data[i+n][1] != 0:
						if not args['skip-minus-ones'] or data[i+n][1] != -1:
							y.append(data[i+n][1])
				n += 1
			
			oy = self._combine(y, args)
			if not args['skip-zeros'] or oy != 0:
				if not args['skip-minus-ones'] or oy != -1:
					xl.append(x)
					yl.append(oy)

			i += n
			
		return (np.array(xl), np.array(yl))
		
	# Combine like xs
	def _combine(self, data, args):
		if len(data) == 0: return 0
		elif args['combine-like-xs'] == 'average' or args['combine-like-xs'] == 'mean': return np.mean(data)
		elif args['combine-like-xs'] == 'median': return np.median(data)
		else: return np.max(data)