# Clustering algorithm from the following paper
# Rodriguez, A., & Laio, A. (2014). Clustering by fast search and find of density peaks. Science (New York, NY), 344(6191), 1492 1496. doi:10.1126/science.1242072
# Give data as a list of distances [[id#1, id#2, distances between them], ...]
import numpy as np

from resources.data_conversion import datatomatrix
from resources.suppress_stdout_stderr import suppress_stdout_stderr

class ClusterDP:
	def __init__(self):
		self.ids = []
		self.data = []
		self.distances = []
		self.cluster_centers = []

	defaults = {
		'save-decision-graph': True,
		'decision-path': '2-graphs/pullplots/decision.pdf',
		'neighbor-percentage': 2.0,
		'sigma': 1.0,
		'return-by-id': True,
	}

	def run(self, pars):

		for key in pars:
			if key != 'distances' and key != 'data': self.defaults[key] = pars[key]

		if 'distances' or 'data' in pars:
			if 'distances' in pars: self.init_distances(pars['distances'])
			else: tf = self.init_physalize(pars['data'], 0 if 'x' in pars and pars['x'] else 1)
			if not tf: return ({}, {})
			self.calculate_distance_density()
			self.decision_graph()
			self.assign_clusters()


	# 1. Begin with list of ids and distances or keys and values
	# Initialize with data that is list of lists [[id#1, id#2, distances between them], ...]
	def init_distances(self, ds):
		for d in ds:
			if d[0] not in self.ids: self.ids.append(d[0])
			if d[1] not in self.ids: self.ids.append(d[1])

		mapper = {}
		for i in range(len(self.ids)): mapper[self.ids[i]] = i
		self.data = np.zeros((len(self.ids), len(self.ids)), dtype=np.float32)

		for d in ds:
			id1 = mapper[d[0]]
			id2 = mapper[d[1]]
			self.data[id1, id2] = d[2]
			self.data[id2, id1] = d[2]
			self.distances.append(d[2])

	# Use pulled data from physalize and generate a list of distances
	def init_physalize(self, ds, xory=1):
		cells, measurements, vs = datatomatrix(ds, xory=xory, normalize=True)
		self.data = np.zeros((len(cells), len(cells)), dtype=np.float32)
		for i in range(len(cells)):
			for j in range(i + 1, len(cells)):
				d = np.sqrt(np.nansum((vs[i,:] - vs[j,:])*(vs[i,:] - vs[j,:])))
				self.data[i, j] = d
				self.data[j, i] = d
				self.distances.append(d)

		self.ids = cells
		return True

	# Initialize with data that is a list of lists [[id if titles, val1, ..., valN]]
	def init_dimensions(self, ds, titles=True, xory=1):
		vs = []
		if titles:
			for type in ds:
				tvs = []
				for d in type['data']:
					self.ids.append(str(d))
					tvs.append(type['data'][d][0][xory])
				vs.append(tvs)
		else:
			vs.append(ds)
			self.ids = [str(i + 1) for i in range(len(ds))]

		if len(vs) > 0:
			vs = np.float32(vs)
			mns = np.min(vs, axis=1)
			mxs = np.max(vs, axis=1)
			for i in range(len(vs)): vs[i] = (vs[i] - mns[i])/(mxs[i] - mns[i])
			# Right now, vs is an array (across data types) of arrays (across cells)
			vs = np.transpose(vs)

			self.data = np.zeros((len(self.ids), len(self.ids)), dtype=np.float32)
			for i in range(len(vs)):
				for j in range(i + 1, len(vs)):
					d = np.sqrt(np.sum((vs[i] - vs[j])*(vs[i] - vs[j])))
					self.data[i, j] = d
					self.data[j, i] = d
					self.distances.append(d)

	# Step one of the algorithm
	def calculate_distance_density(self, neighbor_percentage=-1):
		if neighbor_percentage < 0: neighbor_percentage = self.defaults['neighbor-percentage']

		self.distances.sort()
		dc = self.distances[int(np.round(len(self.distances)*neighbor_percentage/100. - 1))]

		rho = np.zeros(len(self.ids))
		for i in range(len(self.ids)):
			for j in range(i + 1, len(self.ids)):
				rho[i] = rho[i] + np.exp(-(self.data[i, j]/dc)*(self.data[i, j]/dc))
				rho[j] = rho[j] + np.exp(-(self.data[i, j]/dc)*(self.data[i, j]/dc))

		maxdistance = np.max(np.max(self.distances))
		order_rho = np.argsort(rho)[::-1]

		delta = np.zeros(len(self.ids))
		delta[order_rho[0]] = -1
		nneighbors = np.zeros(len(self.ids))

		for i in range(1, len(self.ids)):
			delta[order_rho[i]] = maxdistance
			for j in range(i):
				if self.data[order_rho[i], order_rho[j]] < delta[order_rho[i]]:
					delta[order_rho[i]] = self.data[order_rho[i], order_rho[j]]
					nneighbors[order_rho[i]] = order_rho[j]

		delta[order_rho[0]] = np.max(delta)

		self.rho = rho
		self.delta = delta
		self.nneighbors = nneighbors
		self.order_rho = order_rho
		self.dc = dc

	# Fit the decision graph so that we can calculate the decision graph automagically
	def fit_decision(self, x, y):
		from scipy import optimize, seterr

		x = np.copy(x)/np.max(x)
		y = np.copy(y)/np.max(y)

		fitfun = lambda vs, x: 1.0/(vs[0]*(np.power(x + vs[2], vs[1]))) + vs[3]
		errfun = lambda vs, x, y: fitfun(vs, x) - y

		#with suppress_stdout_stderr:
		seterr(invalid='ignore')
		vscalc, success = optimize.leastsq(errfun, [1, 0.2, 0, 2], args=(x, y))

		# 1/a(x + c)^b + d
		plotfun = lambda x: 1.0/(vscalc[0]*(np.power(x + vscalc[2], vscalc[1]))) + vscalc[3]
		invfun = lambda x: np.power(1.0/(vscalc[0]*(x - vscalc[3])), 1.0/vscalc[1]) - vscalc[2]
		ds = [y - plotfun(x)]
		return (plotfun, invfun, np.std(ds))

#	def fit_sigma(self, sigma):
		#fitfun, vs, stdev = self.fit_decision()

	# INTERNAL METHOD: SIMPLE AXIS
	# Set axis to look like my axes from Keynote into Illustrator
	def _simpleaxis(self, ax):
		ax.yaxis.grid(True, linestyle='solid', linewidth=0.75, color='#AAAAAA')
		ax.tick_params(axis='x', pad = 15)
		ax.tick_params(axis='y', pad = 15)
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.spines['left'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_xaxis().set_tick_params(direction='out')
		ax.get_yaxis().set_ticks_position('none')
		ax.set_axisbelow(True)
		ax.xaxis.labelpad = 15
		ax.yaxis.labelpad = 15
		return ax

	# Sets font to Gotham and linewidths to narrow
	def _font(self, title=False):
		import matplotlib as mpl
		import matplotlib.pyplot as plt
		font = {'family':'Gotham', 'weight':'light', 'size':20}
		mpl.rc('font', **font)
		mpl.rcParams['lines.linewidth'] = 0.75
		return self

	# Draw the decision graph
	def decision_graph(self, sigma=-1):
		import matplotlib as mpl
		import matplotlib.pyplot as plt

		if sigma < 0: sigma = self.defaults['sigma']
		self._font()
		colors = ['#E86E0A', '#D61E21', '#7C7C7C', '#000000', '#75D977', '#C880D1', '#47AEED', '#F2E205']

		fitfun, invfun, stdev = self.fit_decision(self.rho, self.delta)
		wheresigs = np.where(self.rho/np.max(self.rho) > sigma*stdev)
		self.cluster_centers = wheresigs[0][np.where(self.delta[wheresigs]/np.max(self.delta) > fitfun(self.rho[wheresigs]/np.max(self.rho)) + sigma*stdev)]

		fig = plt.figure(figsize=(7.5, 6))
		ax = plt.subplot(111)
		ax = self._simpleaxis(ax)
		ax.scatter(self.rho, self.delta, facecolor='#666666', marker='o', lw=1, s=70, edgecolor='#333333', alpha=0.4)
		for i, cen in enumerate(self.cluster_centers):
			ax.scatter(self.rho[cen], self.delta[cen], facecolor='none', marker='o', lw=2, s=300, edgecolor=colors[i%len(colors)], alpha=0.4)

		xlims = ax.get_xlim()
		ylims = ax.get_ylim()
		xs = np.linspace(invfun(1.5), 1.5, 1000)
		ax.plot(xs*np.max(self.rho), fitfun(xs)*np.max(self.delta), lw=2, color='#888888')
		ln, = ax.plot((xs + sigma*stdev)*np.max(self.rho), (fitfun(xs) + sigma*stdev)*np.max(self.delta), '-', lw=1, color='#888888')
		ln.set_dashes([4, 4])
		ax.set_xlim(xlims)
		ax.set_ylim(ylims)
#		ax.set_ylim([0, 0.8])
		ax.set_xlabel('Local density')
		ax.set_ylabel('Minimum neighbor distance')
		plt.suptitle("\nCLUSTERING DECISION GRAPH", **{'family':'Gotham', 'weight':'book', 'size':24, 'va':'top', 'y':0.995, 'color':'black'})
		plt.subplots_adjust(bottom=0.20, left=0.20, top=0.84)

		with suppress_stdout_stderr():
			if self.defaults['save-decision-graph']: plt.savefig(self.defaults['decision-path'], transparent=True)
			else: plt.show()
		plt.close(fig)

	# Assign each ID to a cluster
	def assign_clusters(self):
		cls = np.zeros(len(self.ids)) - 1
		cls[self.cluster_centers] = np.arange(len(self.cluster_centers))

		# Assign
		for i in range(len(cls)):
			if cls[self.order_rho[i]] == -1:
				cls[self.order_rho[i]] = cls[self.nneighbors[self.order_rho[i]]]

		# Halo
		halo = np.copy(cls)
		if len(self.cluster_centers) > 1:
			border_rho = np.zeros(len(self.cluster_centers))
			for i in range(len(self.ids) - 1):
				for j in range(i + 1, len(self.ids)):
					if cls[i] != cls[j] and self.data[i, j] <= self.dc:
						rho_average = (self.rho[i] + self.rho[j])/2.0
						if rho_average > border_rho[cls[i]]: border_rho[cls[i]] = rho_average
						if rho_average > border_rho[cls[j]]: border_rho[cls[j]] = rho_average

			for i in range(len(self.ids)):
				if self.rho[i] < border_rho[cls[i]]: halo[i] = -1

		self.clusters = halo
		self.clusters_no_halo = cls

	def get_clusters_by_position(self):
		return (self.clusters, self.cluster_centers)

	def get_clusters_by_id(self):
		out = {}
		centers = {}
		for i, id in enumerate(self.ids): out[id] = int(self.clusters[i])
		for i in range(len(self.cluster_centers)): centers[i] = self.ids[self.cluster_centers[i]]
		return (out, centers)

def clusterdp(pars):
	c = ClusterDP(pars)
	if 'return-by-id' in pars and not pars['return-by-id']: return c.get_clusters_by_position()
	else: return c.get_clusters_by_id()

if __name__ == '__main__':
	a = []
	f = open('somdists.dat', 'r')
	d = f.read().split('\n')
	f.close()

	for line in d:
		line = line.split(' ')
		if len(line) == 3:
			a.append([int(line[0]), int(line[1]), float(line[2])])

	print clusterdp({'distances':a})
