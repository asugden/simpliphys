# Grapher
# Data format, necessary:
# [{'data':{'variable-name-cell-1':[(x1,y1),(x2,y2)], 'variable-name-cell-2':[(xb1, yb1),(xb2,yb2)]}}, ...]
# Optional keys for containing list: color, title, x, y
# Note, to add a linebreak to a title, use the pipe | symbol

# Data stored as points to pass between functions, plotted as list x, list y

import numpy as np

#import matplotlib.cm as cm
#import matplotlib.gridspec as gs
#from matplotlib.mlab import PCA as mlabpca

from copy import deepcopy
from resources.suppress_stdout_stderr import suppress_stdout_stderr
from resources.data_conversion import datatomatrix, firstonly

class Grapher():
	def __init__(self, graphpath='.', fullwidth=14.35, halfwidth=7.5):
		self.widths = [fullwidth, halfwidth]
		self.gwd = graphpath if graphpath[-1] == '/' else graphpath + '/'

	defaults = {
		'type': ['line', 'scatter', 'bar', 'pca'], # | 'bar' # Type of graph,
		'dots': True, # Plot as dots if true, else lines
		'combine-like-xs': ['mean', 'median', 'max', 'average'], # | 'median' | 'max'
		'tolerance': 0.01,
		'error': False, # Plots error of type error-type if true
		'error-type': ['stdev', 'stderr'], # | 'stderr' NOTE: plots relative error for 'stdev' in log plot
		'skip-zeros': False, # Skip zero values for averaging and plotting
		'skip-minus-ones': True, # Skip the standard value put in by analysis
		'skip-x-zeros': False,
		'skip-x-minus-ones': True, # Skip the standard value put in by analysis
		'switch-xy': False, # Switch the x and y axis values

		'xmin': None,
		'xmax': None,
		'ymin': None,
		'ymax': None,
		'fit': ['mean', 'none', 'log-mean', 'log-sigmoid'], # | 'log-sigmoid'

		'title': '',
		'xtitle': '',
		'ytitle': '',
		'column-names': False,
		'title-color': 'black',
		'font': ['Gotham', 'Helvetica Neue', 'Helvetica', 'Arial'],
		'xscale': ['linear', 'log', 'sym-log'], # | 'log' | 'sym-log'
		'yscale': ['linear', 'log', 'sym-log'], # | 'log' | 'sym-log'
		'hide-y': False, # hides y axis
		'half-width': False, # sets width to half of a slide

		'labels':{}, # List of labels of individual cells

		'save': '', # non-zero len str is save path
		'format': ['pdf', 'png'], # | 'png'
	}

	# ================================================================================== #
	# EXTERNAL FUNCTIONS
	# Return color by name, defaults to gray
	def color(self, name='gray'):
		from re import match as rematch

		colors = {
			'orange': '#E86E0A',
			'red': '#D61E21',
			'gray': '#7C7C7C',
			'black': '#000000',
			'green': '#75D977',
			'mint': '#47D1A8',
			'purple': '#C880D1',
			'indigo': '#5E5AE6',
			'blue': '#47AEED',
			'yellow': '#F2E205',
		}

		if isinstance(name, int):
			clrs = [colors[key] for key in colors]
			return clrs[name%len(clrs)]
		elif name in colors: return colors[name]
		elif rematch('^(#[0-9A-Fa-f]{6})|(rgb(a){0,1}\([0-9,]+\))$', name): return name
		else: return colors['gray']

	# Print the default values
	def getdefaults(self):
		print 'Graphing Defaults'
		for key in self.defaults:
			val = ', '.join(self.defaults[key]) if type(self.defaults[key]) == list else self.defaults[key]
			print '\t%s: %s' % (key, val)

	# Main function, RUN GRAPH
	def graph(self, args, data):
		import matplotlib as mpl
		import matplotlib.pyplot as plt
		mpl.use('Agg', warn=False)

		if len(data) == 0:
			print '\tNo data to plot.'
			return False

		# Fix the arguments and set the look of the graphs
		gtype, args = self._setargs(args)
		self._setfont()
		self._setlines()

		# Set the width of the figure to be half-width if the half-width flag is true
		# Initialize the figure and axis
		fig = plt.figure(figsize=(self.widths[1] if args['half-width'] else self.widths[0], 6))
		ax = plt.subplot(111)

		# Switch the X-Y if necessary
		data = self._setdata(data, args)

		# Call the graphing function
		getattr(self, gtype)(ax, args, data)

		# Fix the axes, dependent on graph type
		# Must be done after the plotting
		# Then set the values of the titles
		self.setaxes(ax, args)
		self.settitles(ax, args)

		# Save the graph if desired, otherwise show the graph
		with suppress_stdout_stderr():
			if len(args['save']) > 0: plt.savefig(''.join([self.gwd, args['save'], '.', args['format']]), transparent=True)
			else: plt.show()
		plt.close(fig)

	def startgraph(self, args={}):
		import matplotlib as mpl
		import matplotlib.pyplot as plt
		mpl.use('Agg', warn=False)

		gtype, args = self._setargs(args)
		self._setfont()
		self._setlines()

		# Set the width of the figure to be half-width if the half-width flag is true
		# Initialize the figure and axis
		fig = plt.figure(figsize=(self.widths[1] if args['half-width'] else self.widths[0], 6))
		ax = plt.subplot(111)
		ax = self._simpleaxis(ax)

		return fig, ax

	def finishgraph(self, fig, ax, args={}, close=True):
		import matplotlib.pyplot as plt
		gtype, args = self._setargs(args)

		# Fix the axes, dependent on graph type
		# Must be done after the plotting
		# Then set the values of the titles
		self.setaxes(ax, args)
		self.settitles(ax, args)

		# Save the graph if desired, otherwise show the graph
		if close:
			with suppress_stdout_stderr():
				if len(args['save']) > 0: plt.savefig(''.join([self.gwd, args['save'], '.', args['format']]), transparent=True)
				else: plt.show()
			plt.close(fig)
		else:
			plt.show()

	# ==================================================================================
	# GRAPH FUNCTIONS

	# Line graph on axis
	def line(self, ax, args, data):
		ax = self._simpleaxis(ax)

		for subset in data:
			av = []

			for cell in subset['data']:
				clr = self._getlabelcolor(cell, args, subset['color'] if 'color' in subset else '', subset['by'] if 'by' in subset else '')
				x, y, ignoreerr = self._order_and_combine_data(subset['data'][cell], args)
				for xd, yd in zip(x, y): av.append((xd, yd))

				if not args['error']:
					if args['dots']:
						ax.plot(x, y, linewidth=0.5, alpha=0.2, color=clr)
						ax.scatter(x, y, facecolor='none', marker='o', lw=2, s=70, edgecolor=clr, alpha=0.4)
					else: ax.plot(x, y, linewidth=0.5, alpha=0.5 if args['fit'] == 'none' else 0.2, color=clr)

			if args['fit'] != 'none':
				x, y, std = self._fit(av, args, subset['title'] if 'title' in subset else '')

				if args['error']:
					bot, top = self._error(x, y, std, args)
					ax.fill_between(x, bot, top, color=clr, alpha=0.3)

				ax.plot(x, y, linewidth=5, alpha=1, color=clr, solid_capstyle='round')
				#if not args['dots']: ax.plot(x, y, linewidth=5, alpha=0.2, color=self.color('black'), solid_capstyle='round')

	# Scatter plot on axis
	def scatter(self, ax, args, data):
		ax = self._simpleaxis(ax)

		for subset in data:
			for cell in subset['data']:
				clr = self._getlabelcolor(cell, args, subset['color'] if 'color' in subset else '', subset['by'] if 'by' in subset else '')

				x, y, ignoreerr = self._order_and_combine_data(subset['data'][cell], args)
				ax.scatter(x, y, facecolor=clr, marker='o', lw=0.5, s=70, edgecolor='#333333', alpha=0.4)

	# Bar plot on axis
	def bar(self, ax, args, data):
		ax = self._simpleaxis(ax)

		for i, subset in enumerate(data):
			ys = []

			for cell in subset['data']:
				clr = self._getlabelcolor(cell, args, subset['color'] if 'color' in subset else '', subset['by'] if 'by' in subset else '')
				x, y, ignoreerr = self._order_and_combine_data(subset['data'][cell], args)
				ys.append(self.combine(y, args))

			ax.bar(0.6 + 1.6*i, self.combine(ys, args), width=1.0, color=clr, zorder=1, log=True if args['xscale'].find('log') > -1 else False, **{'edgecolor':'none'})
			if args['dots']:
				ax.scatter([1.1 + 1.6*i for j in range(len(ys))], ys, facecolor='none', marker='o', lw=2, s=70, edgecolor=clr, alpha=0.4, zorder=2)
				ax.scatter([1.1 + 1.6*i for j in range(len(ys))], ys, facecolor='none', marker='o', lw=2, s=70, edgecolor='black', alpha=0.2, zorder=3)

		ax.set_xlim([0, 0.6 + 1.6*len(data)])
		ax.get_xaxis().set_tick_params(bottom='off')


	# Run PCA and pass to scatter plot
	def pca(self, ax, args, data):
		cells, measurements, d = datatomatrix(data, normalize=True)
		data, indx = firstonly(data)
		coeff, res = self._pca(d)
		for c in range(len(cells)):
			cell = cells[c]
			data[indx[cell]]['data'][cell].append((res[0, c], res[1, c]))

		if len(args['xtitle']) == 0: args['xtitle'] = 'PCA 1'
		if len(args['ytitle']) == 0: args['ytitle'] = 'PCA 2'
		self.scatter(ax, args, data)

	# Principal component analysis, like Matlab's
	def _pca(self, d):
		# http://glowingpython.blogspot.it/2011/07/pca-and-image-compression-with-numpy.html
		M = (d - np.mean(d.T, axis=1)).T
		latent, coeff = np.linalg.eig(np.cov(M))
		p = np.size(coeff, axis=1)
		idx = np.argsort(latent)
		idx = idx[::-1]
		coeff = coeff[:, idx]
		latent = latent[idx]
		score = np.dot(coeff.T, M)

		return (coeff, score)


	# ==================================================================================
	# OTHER-- FITTING
	# Fit data to an arbitrary function. Now only includes log-sigmoid (i.e. sigmoid on
	# a log-log plot)
	# Expects data as a series of points
	def _fit(self, data, args, name=''):
		xs, ys = [[p[i] for p in data] for i in (0, 1)]
		ftargs = deepcopy(args)
		ftargs['combine-like-xs'] = 'mean'

		if args['fit'] == 'mean':
			x, y, err = self._order_and_combine_data(data, ftargs)

		elif args['fit'] == 'log-mean':
			logys = np.log10(ys)
			x, ly, lerr = self._order_and_combine_data(zip(xs, logys), ftargs)
			y = np.power(10, ly)
			err = np.power(10, lerr)

		elif args['fit'] == 'log-sigmoid':
			from scipy import optimize

			fitfun = lambda vs, xs: vs[0]/(1 + np.exp(-np.log10(xs)/vs[2] - vs[1]))
			initial = np.array([np.max(ys), np.average(np.log10(xs)), 1.])
			errfun = lambda vs, xs, ys: fitfun(vs, xs) - ys
			vscalc, success = optimize.leastsq(errfun, initial, args=(xs, ys))

			if args['xscale'].find('log') > -1: x = np.logspace(np.log10(av[0][0]), np.log10(av[-1][0]), 100)
			else: x = np.linspace(av[0][0], av[-1][0], 100)
			y = fitfun(x)
			err = [0 for i in y]

			print '\tLOG SIGMOID FIT: Group %s has maximum %3g and peak change at %3g.' % (name, vscalc[0], vscalc[1])
		return 	(x, y, err)

	# Calculate the error. Either log error or straight error
	def _error(self, x, y, err, args):
		if args['yscale'].find('log') < 0:
			top = y + err
			bot = y - err
		else:
			# http://faculty.washington.edu/stuve/log_error.pdf
			top = np.power(np.ones(y.size)*10, np.log10(y) + 0.434*(err/y))
			bot = np.power(np.ones(y.size)*10, np.log10(y) - 0.434*(err/y))
			print '\tWARNING: Printing relative error, not absolute error'
		return bot, top

	# Order X values from low to high and combine Y values with equal Xs or Xs within tolerance
	def _order_and_combine_data(self, data, args):
		data.sort()
		i = 0
		xl = []
		yl = []
		err = []
		while i < len(data):
			if (not args['skip-x-minus-ones'] or abs(data[i][0] + 1.0) > args['tolerance']) and (not args['skip-x-zeros'] or abs(data[i][0]) > args['tolerance']):
				x = data[i][0]
				y = [data[i][1]]
				n = 1
				while i + n < len(data) and (isinstance(data[i + n][0], str) or data[i + n][0] - x <= args['tolerance']):
					if not isinstance(data[i + n][0], str):
						if not args['skip-zeros'] or data[i+n][1] != 0:
							if not args['skip-minus-ones'] or abs(data[i+n][1] + 1.0) > args['tolerance']:
								y.append(data[i+n][1])
					n += 1

				oy = self.combine(y, args)
				if not args['skip-zeros'] or oy != 0:
					if not args['skip-minus-ones'] or abs(oy + 1.0) > args['tolerance']:
						xl.append(x)
						yl.append(oy)
						err.append(np.std(y))
						err[-1] = err[-1]/len(y) if args['error-type'] == 'stderr' else err[-1]

				i += n
			else: i += 1

		return (np.array(xl), np.array(yl), np.array(err))

	# Combine like xs
	def combine(self, data, args):
		if len(data) == 0: return 0
		elif args['combine-like-xs'] == 'average' or args['combine-like-xs'] == 'mean': return np.mean(data)
		elif args['combine-like-xs'] == 'median': return np.median(data)
		else: return np.max(data)


	# ================================================================================== #
	# STUPID REQUIRED FUNCTIONS
	# Those annoying functions that need to go somewhere

	# Add the default arguments to the input arguments
	# Make sure that the arguments are the correct types
	def _setargs(self, args):
		out = {}
		# Set defaults of the argument
		for key in self.defaults:
			if type(self.defaults[key]) == list:
				out[key] = self.defaults[key][0]
			else: out[key] = self.defaults[key]

		# Add the input arguments and check them for correction
		for key in args:
			if key in self.defaults:
				if type(self.defaults) == list:
					if args[key].lower() in self.defaults[key]:
						out[key] = args[key].lower()
				else:
					out[key] = args[key]
		return out['type'], out

	# Switch the X and Y of the data if necessary, otherwise just return the data
	def _setdata(self, data, args):
		if args['switch-xy']:
			data = deepcopy(data)
			for i in range(len(data)):
				for cell in data[i]['data']:
					for j in range(len(data[i]['data'][cell])):
						data[i]['data'][cell][j] = (data[i]['data'][cell][j][1], data[i]['data'][cell][j][0])
		return data

	# Return a color in a label, if it exists
	# This function is dependent on the input formatting
	def _getlabelcolor(self, name, args, clr, title=''):
		clr = self.color(clr)
		if 'labels' in args:
			if str(name) in args['labels']:
				return self.color(args['labels'][str(name)])
			elif title != '' and title in args['labels']:
				return self.color(args['labels'][str(title)])
		return clr


	# ================================================================================== #
	# FORMATTING
	# Set formatting. This is to personal taste

	# Set axis to just a scale bar. Ref: https://gist.github.com/dmeliza/3251476
	def _noaxis(self, ax):
		ax.axis('off')
		return ax

	# Set axis to look like my axes from Keynote into Illustrator
	def _simpleaxis(self, ax):
		ax.yaxis.grid(True, linestyle='solid', linewidth=0.75, color='#AAAAAA')
		ax.tick_params(axis='x', pad = 15)
		ax.tick_params(axis='y', pad = 15)
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.spines['left'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_xaxis().set_tick_params(direction='out')
		ax.get_yaxis().set_ticks_position('none')
		ax.set_axisbelow(True)
		ax.xaxis.labelpad = 15
		ax.yaxis.labelpad = 15
		return ax

	# Set font for numerals, etc. to Gotham
	# Alternatives could be Helvetica Neue, Helvetica, Arial
	def _setfont(self, font="Gotham"):
		import matplotlib as mpl
		f = {'family':font, 'weight':'light', 'size':20}
		mpl.rc('font', **f)
		return self

	# Set the line widths to be subtle, like my Keynote graphs
	def _setlines(self):
		import matplotlib as mpl
		mpl.rcParams['lines.linewidth'] = 0.75

	# Hide y if necessary
	def setaxes(self, ax, args):
		if args['hide-y']:
			ax.get_yaxis().set_ticklabels([])
			ax.set_ylabel(' ')

		if args['type'] != 'bar':
			ax.set_xscale(args['xscale'])
			if args['xmin'] != None: ax.set_xlim([args['xmin'], ax.get_xlim()[1]])
			if args['xmax'] != None: ax.set_xlim([ax.get_xlim()[0], args['xmax']])
			ax.set_yscale(args['yscale'])
		else:
			ax.get_xaxis().set_ticklabels([])
			if args['ymin'] == None: args['ymin'] = 0
			if args['yscale'].find('log') > -1: ax.set_yscale(args['yscale'], nonposy='clip')
			else: ax.set_yscale(args['yscale'])

		if args['ymin'] != None: ax.set_ylim([args['ymin'], ax.get_ylim()[1]])
		if args['ymax'] != None: ax.set_ylim([ax.get_ylim()[0], args['ymax']])

	# Set the X, Y, and supertitles
	def settitles(self, ax, args):
		import matplotlib.pyplot as plt

		if len(args['xtitle']) > 0:
			ax.set_xlabel(args['xtitle'])
			plt.subplots_adjust(bottom=0.20)

		if len(args['ytitle']) > 0:
			ax.set_ylabel(args['ytitle'])
			if args['half-width']: plt.subplots_adjust(left=0.20)

		if len(args['title']) > 0:
			#correctedtitle = args['title'].replace("\\n", "\n") if args['title'].find("\n") > -1 else "\n" + args['title']
			correctedtitle = args['title'].replace("|", "\n") if args['title'].find("|") > -1 else "\n" + args['title']
			if 'save' in args and args['save'] != '':
				plt.suptitle(correctedtitle, **{'family':str(args['font']), 'weight':'book', 'size':24, 'va':'top', 'y':0.995, 'color':self.color(args['title-color'])})
			else:
				# Due to an error in the MacOSX backend
				plt.suptitle(correctedtitle, **{'size':24, 'va':'top', 'y':0.995, 'color':self.color(args['title-color'])})
			plt.subplots_adjust(top=0.84)

