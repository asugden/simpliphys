from Grapher import Grapher
from ABF import ABF

class Tracer:
	def __init__(self):
		self.grapher = Grapher()
		for key in self.grapher.defaults:
			if key not in self.defaults:
				self.defaults[key] = self.grapher.defaults[key]
	
	defaults = {
		'xtitle': 'TIME (s)',
	}
	
	def run(self, args, prots):
		self.args = args
		
		self.keys = sorted([key for key in prots])
		self.ps = prots
		
		self.n = 0
		self.onlytr = args['trace'] if 'trace' in args and args['trace'] > -1 else -1
		
		p = self.ps[self.keys[self.n]][0]
		self.file = ABF(p['path'])
		if self.onlytr > -1:
			self.tr = self.onlytr if self.onlytr < self.file.n() else self.file.n() - 1
		else:
			self.tr = 0
		
		self.starttrace(p)
		
	def starttrace(self, p):
		self.fig, self.ax = self.grapher.startgraph(self.args)
		self.cid = self.fig.canvas.mpl_connect('key_press_event', self.nexttrace)
		self.trace(p)
		
	def trace(self, p):
		self.ax.clear()
		x, y = self.file.plot(self.tr, (self.args['xmin'] if 'xmin' in self.args else -1, self.args['xmax'] if 'xmax' in self.args else -1), tunits='s')
		self.ax.plot(x, y, color='gray', linewidth=0.5)
		
		self.ax.set_xlabel(self.args['xtitle'])
		self.ax.set_ylabel('POTENTIAL (mV)' if p['clamp'] == 'current' else 'CURRENT (pA)')

		self.grapher.setaxes(self.ax, self.args)
		self.grapher.settitles(self.ax, self.args)
		
		self.args['title'] = 'CELL %s TRACE %i' % (p['cell-id'], self.tr)
		self.args['ytitle'] = 'POTENTIAL (mV)' if p['clamp'] == 'current' else 'CURRENT (pA)'

		self.fig.canvas.draw()
		self.grapher.finishgraph(self.fig, self.ax, self.args, close=False)
		
	def nexttrace(self, event):
		if event.key == '/' or (event.key == '.' and self.onlytr > -1):
			self.n += 1
			self.n = self.n%len(self.keys)
			
			p = self.ps[self.keys[self.n]][0]
			self.file = ABF(p['path'])
			
			if self.onlytr > -1: self.tr = self.onlytr
			if self.tr > self.file.n(): self.tr = self.file.n() - 1
			self.trace(p)
			
		elif event.key == 'm' or (event.key == ',' and self.onlytr > -1):
			self.n += 1
			self.n = self.n%len(self.keys)
			
			p = self.ps[self.keys[self.n]][0]
			self.file = ABF(p['path'])
			
			if self.onlytr > -1: self.tr = self.onlytr
			if self.tr > self.file.n(): self.tr = self.file.n() - 1
			self.trace(p)
			
		elif event.key == '.':
			self.tr += 1
			if self.tr >= self.file.n():
				self.tr = 0
				self.n += 1
				self.n = self.n%len(self.keys)
			
				p = self.ps[self.keys[self.n]][0]
				self.file = ABF(p['path'])
			else:
				p = self.ps[self.keys[self.n]][0]
			self.trace(p)
		
		elif event.key == ',':
			self.tr -= 1
			if self.tr < 0:
				self.n -= 1
				self.n = self.n%len(self.keys)
			
				p = self.ps[self.keys[self.n]][0]
				self.file = ABF(p['path'])
				self.tr = self.file.n() - 1
			else:
				p = self.ps[self.keys[self.n]][0]
			self.trace(p)
