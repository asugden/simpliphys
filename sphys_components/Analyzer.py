from importlib import import_module
#from copy import deepcopy
import os, types
from types import MethodType

from ABF import ABF
from Grapher import Grapher

class Analyzer():
	def __init__(self, database, testing=False):
		self.grapher = Grapher('.')
		self.db = database

		# Load all of the file analyzer classes
		self._loadanalyzers()
		if testing:
			self._loadtestanalyzers()

	# Check whether protocols have been analyzed
	def check(self, data):
		if not isinstance(data, dict) or len(data) == []: return 0, []

		nprotocols = 0
		unanalyzed = 0
		morekeys = []

		# Iterate over all of the data from metadata
		for cell in data:
			for p in data[cell]:
				nprotocols += 1

				# Get the unmatched keys for each protocol, first trace-specific and then
				# file specific
				un = self._matchanalysis(p, self.tans)
				un.extend(self._matchanalysis(p, self.fans))
				un.extend(self._matchanalysis(p, self.cans))

				# Add the keys to the unanalyzed keys if possible
				if un != []:
					unanalyzed += 1
					for u in un:
						if u not in morekeys:
							morekeys.append(u)
		return (unanalyzed, morekeys)


	# Analyze a list of protocols, update the output. Can be forced or tested.
	# Analyzes three ways-- by trace, by file, and by cell.
	# If test is true, it only analyzes using modules prefixed "test" and does not save
	# the results.
	def analyze(self, data, force=False, test=False):
		if not isinstance(data, dict) or len(data) == []: return False

		# Loop through all cells
		for cell in data:
			# And loop through all protocols in a particular cell
			for p in data[cell]:
				# Get the classes and unanalyzed keys (returned as dict) for both trace-
				# specific and file-specific analysis types
				unt = self._matchanalysis(p, self.testtans if test else self.tans, True, force=force)
				unf = self._matchanalysis(p, self.testfans if test else self.fans, True, force=force)

				# If there are unanalyzed keys to a protocol, analyze them
				if unt != {} or unf != {}:
					print '\tAnalyzing cell %s file %s...' % (p['cell-id'], p['name'])

					# Open the file for passing
					f, trs = self._getfiletraces(p)

					# Analyze trace, then file
					results = self._analyze_trace(unt, f, p, trs)
					if not test: self.db.update(p, results)

					results = self._analyze_file(unf, f, p, trs)
					if not test: self.db.update(p, results)

		# Loop again for cell-by-cell analysis
		for cell in data:
			for p in data[cell]:
				unc = self._matchanalysis(p, self.testcans if test else self.cans, True, force=force)

				# If there are unanalyzed keys to a protocol, analyze them
				if unc != {}:
					print '\tAnalyzing cell %s...' % (p['cell-id'])
					results = self._analyze_cell(unc, p)
					if not test: self.db.update(p, results)

		# Analysis is only saved to files at the very end
		self.db.saveanalysis()

	# Analyze all traces from a file separately
	def _analyze_trace(self, unt, f, p, trs):
		if unt == {}: return {}

		# Includes classes, each of which might have multiple values to set
		out = {val:[] for key in unt for val in unt[key]}
		for tr in trs:
			# Loop through all classes that need to be applied
			for cls in unt:
				c = object.__new__(cls)

				# Inject methods such as getpair()
				setattr(c, '_analyzer_module', self)
				setattr(c, '_an_file', f)
				setattr(c, '_an_protocol', p)
				setattr(c, '_an_trace', tr)
				c.getpair = MethodType(self._get_pair_trace_for_modules, c)

				# Call init if necessary
				if '__init__' in vars(cls):
					c.__init__(f, p, tr)

				# And then through keys set by the class
				for u in unt[cls]:
					out[u].append(getattr(c, u.replace('-', '_'))(f, p, tr))
		return out

	# Analyze all traces from a file separately
	def _analyze_file(self, unf, f, p, trs):
		out = {}
		if unf == {}: return out

# Search through the classes first
		for cls in unf:
			c = object.__new__(cls)

			# Inject methods such as getpair()
			setattr(c, '_analyzer_module', self)
			setattr(c, '_an_file', f)
			setattr(c, '_an_protocol', p)
			setattr(c, '_an_trace', trs)
			c.getpair = MethodType(self._get_pair_file_for_modules, c)

			# Call init if necessary
			if '__init__' in vars(cls):
				c.__init__(f, p, trs)

			# And then through each of the keys associated with the class
			for u in unf[cls]:
				out[u] = getattr(c, u.replace('-', '_'))(f, p, trs)
		return out

	# Analyze a whole cell after traces and files have been analyzed.
	# NOTE: getpair not implemented yet
	def _analyze_cell(self, unc, p):
		out = {}

		# Search through the classes first
		for cls in unc:
			ps = self.db.getcell(p)
			c = object.__new__(cls)

			# Call init if necessary
			if '__init__' in vars(cls):
				c.__init__(ps)

			# And then through each of the keys associated with the class
			for u in unc[cls]:
				out[u] = getattr(c, u.replace('-', '_'))(ps)
		return out

	# Pull data. Handles xscale, yscale, and trace
	def pull(self, args, data):
		if not isinstance(data, dict) or len(data) == []: return []

		everfoundx = False
		everfoundy = False
		everfoundby = False

		self.analyze(data, force=args['force'])
		out = {}
		for cell in data:
			for p in data[cell]:
				foundby, by = self._getby(p, args['by'])
				if args['x'] == '1':
					foundx, x = True, ' '
				else:
					foundx, x = self._getpattr(p, args['x'], args['trace'])
				foundy, y = self._getpattr(p, args['y'], args['trace'])

				if foundx: everfoundx = True
				if foundy: everfoundy = True
				if foundby: everfoundby = True

				if foundby and foundx and foundy:
					if by not in out:
						out[by] = {
							'data': {},
							'by': by,
							'x': args['x'],
							'y': args['y'],
							'color': self.defaultcolor(p, args) if args['color'] == 'default' else self.grapher.color(args['color']),
						}
					out[by]['data'] = self._adddata(out[by]['data'], cell, x, y, args)

		if not everfoundx: print '\tWARNING: Could not find -x value of %s' % (args['x'])
		if not everfoundy: print '\tWARNING: Could not find -y value of %s' % (args['y'])
		if not everfoundby: print '\tWARNING: Could not find -by value of %s' % (args['by'])

		return [out[key] for key in out]

	# Determine the default color to be graphed
	def defaultcolor(self, p, args):
		if args['by'] == 'genotype' or (isinstance(args['by'], list) and 'genotype' in args['by']):
			return self.grapher.color('red') if p['genotype'].lower() == 'cx36ko' else self.grapher.color()
		elif args['by'] == 'region' or (isinstance(args['by'], list) and 'region' in args['by']):
			return self.grapher.color('orange') if p['region'].lower() == 'ventral' else self.grapher.color('green') if p['region'].lower() == 'somatosensory' else self.grapher.color()
		elif args['by'] == 'type' or (isinstance(args['by'], list) and 'type' in args['by']):
			if p['type'] == 'rs': return self.grapher.color('gray')
			elif p['type'] == 'lts' and p['fluortarget'] == 'x94': return self.grapher.color('yellow')
			elif p['type'] == 'lts': return self.grapher.color('red')
			elif p['type'] == 'fs' and p['fluortarget'] == 'som': return self.grapher.color('indigo')
			elif p['type'] == 'fs': return self.grapher.color('blue')
			elif p['type'] == 'vip': return self.grapher.color('mint')
			elif p['type'] == 'unknown': return self.grapher.color('orange')
			else: return self.grapher.color()
		else: return self.grapher.color()

	# Get the title of the attribute(s) to select by
	def _getby(self, p, by):
		if isinstance(by, list):
			passed = True
			out = []
			for a in by:
				found, name = self._getpattr(p, a)
				if not found or isinstance(name, list): passed = False
				else: out.append(name)
			return passed, ','.join([str(i) for i in out])
		else: return self._getpattr(p, by)

	# Get an attribute from a protocol, taking into account that it may have to be computed
	# Skips any members that need to be skipped
	def _getpattr(self, p, attribute, trace=-1):
		if isinstance(attribute, int): return True, attribute
		if attribute not in p: return False, 0

		if isinstance(p[attribute], list):
			out = []
			for i, el in enumerate(p[attribute]):
				if (trace < 0 or trace == i) and ('skip' not in p or i + 1 not in p['skip']): out.append(el)

			if len(out) == 0: return False, 0
			else: return True, out
		else: return True, p[attribute]

	# Add new x and ys to get out data points
	def _adddata(self, out, name, x, y, args):
		if name not in out: out[name] = []
		if isinstance(y, list) and not isinstance(x, list): out[name].extend([(x, i) for i in y])
		elif isinstance(x, list) and not isinstance(y, list): out[name].extend([(i, y) for i in x])
		elif isinstance(x, list) and isinstance(y, list) and len(x) == len(y): out[name].extend(zip(x, y))
		elif isinstance(x, list) and isinstance(y, list) and len(x) != len(y): print '\tWARNING: Nonmatching lengths for x and y'
		else:
			if (x,y) not in out[name]:
				out[name].append((x, y))

		if 'xscale' in args and args['xscale'] != 1:
			for i in range(len(out)):
				out[i][0] = out[i][0]*args['xscale']
		if 'yscale' in args and args['yscale'] != 1:
			for i in range(len(out)):
				out[i][1] = out[i][1]*args['yscale']

		return out

	# Compare a protocol to screening parameters
	def _compare_screen_protocol(self, s, p):
		matched = True
		for skey in s:
			if isinstance(s[skey], list):
				submatch = False
				for mtch in s[skey]:
					if isinstance(mtch, list):
						if p[skey] == mtch[0]:
							submatch = self._compare_screen_protocol(mtch[1], p)
					elif skey in p and p[skey] == mtch: submatch = True
				if not submatch: matched = False
			elif isinstance(p[skey], str) and p[skey].lower() != s[skey].lower(): matched = False
			elif not isinstance(p[skey], str) and p[skey] != s[skey]: matched = False
		return matched

	# Find unset analysis keys for a protocol and their associated classes
	def _matchanalysis(self, p, an, wclasses=False, force=False):
		unclasses = {}
		unlist = []
		self.db.load(p)
		if p['protocol'] in an:
			for a in an[p['protocol']]:
				if self._compare_screen_protocol(a['screen'], p):
					out = []
					for s in a['sets']:
						if s not in p or force:
							if s not in unlist:
								unlist.append(s)
							out.append(s)

					if len(out) > 0:
						if a['class'] not in unclasses:
							unclasses[a['class']] = out
						else:
							unclasses[a['class']].extend(out)

		return unclasses if wclasses else unlist

	# Get a file and the traces to use
	def _getfiletraces(self, p, args={}):
		f = ABF(p['path'])

		# Get a list of skipped traces, switch to 0-indexed
		skips = []
		if 'skip' in p:
			for i in p['skip']:
				skips.append(i - 1)

		# Open the file, skipping the correct indices
		f.amplifier(p['amplifier']).clamp(p['clamp']).skip(skips).read()
		trs = []
		for i in range(f.n()):
			# We use 1-indexed traces in Sipmliphys
			if 'trace' not in args or args['trace'] < 0 or args['trace'] - 1 == i:
				trs.append(i)
		return f, trs

	# Get traces for a paired file
	def _gettraces(self, f, p):
		if 'lost' in p: return []

		# Get a list of skipped traces, switch to 0-indexed
		skips = []
		if 'skip' in p:
			for i in p['skip']:
				skips.append(i - 1)

		trs = []
		for i in range(f.n()):
			if i not in skips:
				trs.append(i)
		return trs

	# Pass through for updating all members of a cell
	#def updatecell(self, p, args):
	#	self.db.updatecell(p, args)

    # Load all analyzers into dicts assigned to the type of protocol
	def _loadanalyzers(self):
		# The trace, file, and meta analyzers by screened protocol
		self.tans = self._linkanalyzers(self._getanalyzers('sphys_trace'))
		self.fans = self._linkanalyzers(self._getanalyzers('sphys_file'))
		self.cans = self._linkanalyzers(self._getanalyzers('sphys_cell'))

	def _loadtestanalyzers(self):
		# The trace, file, and meta analyzers by screened protocol
		self.testtans = self._linkanalyzers(self._getanalyzers('sphys_trace', True))
		self.testfans = self._linkanalyzers(self._getanalyzers('sphys_file', True))
		self.testcans = self._linkanalyzers(self._getanalyzers('sphys_cell', True))

    # Link all analyzer classes them with the appropriate protocols and what they set
	def _linkanalyzers(self, anclasses):
		out = {}
		for c in anclasses:
			# Get the keys upon which the class is screened
			screens = getattr(c, 'screen')
			for p in screens['protocol']:
				prot = p[0] if isinstance(p, list) else p

				if prot not in out: out[prot] = []
				# Set the other variables that are screened, the variables that are set,
				# and the class
				sets = [s.replace('_', '-') for s in vars(c) if s[0] != '_' and isinstance(getattr(c, s, None), types.MethodType)]
				out[prot].append({'screen':{}, 'sets':sets, 'class':c})

				for key in screens:
					if key == 'protocol' and isinstance(screens[key], list):
						pscreen = []
						for mtch in screens[key]:
							if isinstance(mtch, list):
								pscreen.append(mtch)
						if len(pscreen) > 0: out[prot][-1]['screen'][key] = pscreen
					if key != 'protocol':
						out[prot][-1]['screen'][key] = screens[key]
		return out

	# Load all analyzer classes from all modules from a path and return the classes
	def _getanalyzers(self, path, test=False):
		out = []
		if os.path.exists(path):
			files = os.listdir(path)
			for f in files:
				if f[-3:] == '.py' and f[0] != '_':
					if (not test and f[:4] != 'test') or (test and f[:4] == 'test'):
						module = import_module('%s.%s' % (path, f[:-3]))
						out.extend(self._screenanalyzer(module))
		return out

	# Check that all analyzer classes have the correct requirements
	def _screenanalyzer(self, module):
		classes = [getattr(module, m) for m in vars(module) if m[0] != '_' and isinstance(getattr(module, m, None), type)]
		cutclasses = [c for c in classes if hasattr(c, 'screen') and 'protocol' in getattr(c, 'screen')]
		return cutclasses


	# Functions to pass to analyzer modules
	# These functions return the paired f, p, tr[s]
	# More functions could be added here if necessary

	# This method gets injected into each class before __init__ is called (if init exists)
	# Offers the getpair() function with only traces that match between files
	def _get_pair_file_for_modules(self, moduleobject):
		if not 'pair-id' in moduleobject._an_protocol or moduleobject._an_protocol['pair-id'] < 0: return {}, -1
		pairp = self.db.pair(moduleobject._an_protocol)
		trs = self._gettraces(moduleobject._an_file, pairp)

		pairtrs = list(set(trs) | set(moduleobject._an_trace))
		if len(pairtrs) > 0:
			return p, pairtrs
		else: return {}, -1

	# This method gets injected into each class before __init__ is called (if init exists)
	# Offers the getpair() function with only if the trace from the analyzed file exists
	def _get_pair_trace_for_modules(self, moduleobject):
		if not 'pair-id' in moduleobject._an_protocol or moduleobject._an_protocol['pair-id'] < 0: return {}, -1
		pairp = self.db.pair(moduleobject._an_protocol)
		trs = self._gettraces(moduleobject._an_file, pairp)

		if moduleobject._an_trace in trs:
			return pairp, moduleobject._an_trace
		else: return {}, -1

	# This method is currently only used in premadeplotter for the meta style of analysis.
	def _get_file_for_modules(self, moduleobject, p, args={}):
		return self._getfiletraces(p, args)
