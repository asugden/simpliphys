import re, numpy as np
from sphys_components.IOData import IOData
from sphys_components.data_conversion import firstonly, datatomatrix
from copy import deepcopy

class Printer():
	def __init__(self, data, lbls):
		self.d = data
		self.l = lbls
		self.io = IOData()

	defaults = {
		'xy': True,
		'x': False,
		'y': False,
		'rows': False,
		'normalize': False,
		'tolerance': 0.0001,
		'combine-like-xs': ['mean', 'median', 'max'], # How to combine points with matching Xs
		'skip-zeros': False, # Skip values equal to 0
		'skip-minus-ones': True, # Skip values equal to -1
		'titles': True, # Add titles to the output matrix
		'decimal-places': 2, # Number of decimal places to print
		'xory': ['guess', 'xy', 'y', 'x', 'xandy'], # Print an X, Y matrix or print a list of values
		'labels': True, # Print labels as well
	}

	# Guess whether multiple Xs or Ys should be printed or just XY
	def _guessxy(self, args, data):
		xs = list(set([subset['x'] for subset in data if isinstance(subset['x'], str) and not re.match('^(-)?[\d]+$', subset['x']) and not re.match('^(-)?[\d.]+$', subset['x'])]))
		ys = list(set([subset['y'] for subset in data if isinstance(subset['y'], str) and not re.match('^(-)?[\d]+$', subset['y']) and not re.match('^(-)?[\d.]+$', subset['y'])]))
		comb = sorted(list(set(xs + ys)))

		if len(xs) == 1 and len(ys) == 1: return 'xy', comb
		if len(xs) == 0 and len(ys) > 0: return 'y', comb
		if len(xs) > 0 and len(ys) == 0: return 'x', comb
		if len(xs) > 0 and len(ys) > 0: return 'xandy', comb
		if len(xs) == 0 and len(ys) == 0: return 'fail', comb

	# Print a list of individaul values
	def _xory(self, args, data, titles, printlabels):
		"""Take a dataset and return a matrix of individual values."""
		# Combine multiple Y values within the same cell
		data = self.io.combine(args, data)
		bys = sorted(list(set([(subset['by'], i) for i, subset in enumerate(data)])))
		cells = sorted(list(set([(cell, by[0]) for by in bys for cell in data[by[1]]['data']])))
		types = sorted(list(set([c[1] for c in cells])))
		cellsbytype = [[c[0] for c in cells if c[1] == t] for t in types]

		# Output begins with cell names, fill out the entire matrix with blank spaces
		out = deepcopy(cellsbytype)
		for i in range(len(out)):
			for j in range(len(out[i])):
				if printlabels:
					lbl = self.l[out[i][j]] if out[i][j] in self.l else ''
					out[i][j] = [out[i][j], self.io.stringify(args, lbl)] + ['' for t in titles]
				else: out[i][j] = [out[i][j]] + ['' for t in titles]

		# Set each of the values using indices from the titles.
		# FIX, should I combine here?

		# Set offset given labels or lack thereof
		off = 2 if printlabels else 1
		for i, type in enumerate(types):
			for subset in data:
				x, y = titles.index(subset['x']) if subset['x'] in titles else -1, titles.index(subset['y']) if subset['y'] in titles else -1

				for cell in subset['data']:
					if cell in cellsbytype[i]:
						ci = cellsbytype[i].index(cell)

						if len(subset['data'][cell]) > 0 and len(subset['data'][cell][0]) > 1:
							if x > -1: out[i][ci][x+off] = self.io.stringify(args, subset['data'][cell][0][0])
							if y > -1: out[i][ci][y+off] = self.io.stringify(args, subset['data'][cell][0][1])

		# Justify everything, add titles
		if printlabels: out.insert(0, [['Cell', 'Label'] + titles])
		else: out.insert(0, [['Cell'] + titles])
		out = self.io.justify(out)

		# And print the results, title first
		print '\t'.join(out[0][0])
		for i, section in enumerate(out[1:]):
			print types[i]
			for cell in section:
				print '\t'.join(cell)

	# Print X, Y matrix
	def _xys(self, args, data, printlabels):
		"""Take a dataset and return a matrix of Y values by X. Used for the same measure with multiple values."""
		# Combine multiple Y values within the same cell, and collect x, y, and by
		data = self.io.combine(args, data)
		fx, fy, fby = data[0]['x'], data[0]['y'], data[0]['by']

		# Get unique Xs and Bys
		xs = sorted(list(set([x[0] for subset in data for cell in subset['data'] for x in subset['data'][cell]])))
		bys = sorted(list(set([(subset['by'], i) for i, subset in enumerate(data) if (subset['x'], subset['y']) == (fx, fy)])))

		# Print the key information
		print 'Pulled\tby: %s\tx: %s\ty: %s' % (fby, fx, fy)
		if printlabels: print '\t'.join(['Cell ID  ', 'Label'] + [self.io.stringify(args, p) for p in xs])
		else: print '\t'.join(['Cell ID  '] + [self.io.stringify(args, p) for p in xs])

		out = [[] for by in bys]
		# Iterate through the groups that have been ordered
		for byi, (by, n) in enumerate(bys):
			subset = data[n]
			# Order all of the cells and iterate
			cells = sorted([cell for cell in subset['data']])
			for cell in cells:
				subset['data'][cell].sort()

				# Add elements to the line. Blanks if no match
				p = 0
				ln = [self.io.stringify(args, cell)]
				if printlabels:
					if cell in self.l: ln.append(self.io.stringify(args, self.l[cell]))
					else: ln.append('')
				for i in range(len(xs)):
					if p >= len(subset['data'][cell]): ln.append('')
					else:
						# m0 = is not equal to 0 or skip zeros, m1 = is not equal to -1
						# or skip minus ones
						m0 = not args['skip-zeros'] or abs(subset['data'][cell][p][1]) >= args['tolerance']
						m1 = not args['skip-minus-ones'] or abs(subset['data'][cell][p][1] + 1) >= args['tolerance']

						if abs(xs[i] - subset['data'][cell][p][0]) <= args['tolerance'] and m0 and m1:
							ln.append(self.io.stringify(args, subset['data'][cell][p][1]))
							p += 1
						else: ln.append('')

				if len(ln) > 0: out[byi].append(ln)

		# Print the lines
		out = self.io.justify(out)
		for i, section in enumerate(out):
			print bys[i][0]
			for cell in out[i]:
				print '\t'.join(cell)

	# Run it
	def run(self, args):
		args = self._setargs(args)
		args['xory'], titles = self._guessxy(args, self.d) if args['xory'] == 'guess' else args['xory']
		printlabels = True if args['labels'] and self.l != {} else False

		if args['xory'] == 'fail': print 'WARNING: No data to pull'
		elif args['xory'] == 'xy': self._xys(args, self.d, printlabels)
		else: self._xory(args, self.d, titles, printlabels)



# 	def run(self, args):
# 		args = self._setargs(args)
#
# 		if 'x' in args and args['x']: return self.printsingle(args, 'x')
# 		elif 'y' in args and args['y']: return self.printsingle(args, 'y')
# 		else: return self._printxy(args)
#
# 		xygroups = {}
# 		for i in range(len(self.d)):
# 			title = self.d[i]['x'] + '/' + self.d[i]['y']
# 			if title not in xygroups: xygroups[title] = [i]
# 			else: xygroups[title].append(i)
#
# 		ordered = [key for key in xygroups]
# 		ordered.sort()
# 		for key in ordered:
# 			self.xys(args, [self.d[i] for i in xygroups[key]])
#
#
# 	# Print only X's or only Y's
# 	# Split data into groups separated by what X values were pulled on and send to singles
# 	def printsingle(self, args, xory = 'x'):
# 		if firstonly(self.d) or args['rows']: return self.printmatrix(args, xory)
#
# 		groups = {}
# 		for i in range(len(self.d)):
# 			title = self.d[i][xory]
# 			if title not in groups: groups[title] = [i]
# 			else: groups[title].append(i)
#
# 		ordered = [key for key in groups]
# 		ordered.sort()
# 		for key in ordered:
# 			self.singles(args, [self.d[i] for i in groups[key]], xory)
#
# 	def printmatrix(self, args, xory):
# 		cells, measurements, data = datatomatrix(self.d, 1 if xory == 'y' else 0, normalize=True if args['normalize'] else False)
# 		print '\t' + '\t'.join(measurements)
# 		for i in range(len(cells)):
# 			print '%s\t%s' % (cells[i], '\t'.join(['%.2f' % (j) if not np.isnan(j) else '' for j in data[i]]))
#
# 	# Prints values pulled on a single X or Y value
# 	def singles(self, args, data, xory = 'x'):
# 		if len(data) == 0:
# 			print '\tNo values to print'
# 			return False
#
# 		cells = {}
# 		bys = []
#
# 		# Split data into groups by what they were pulled on
# 		for group in data:
# 			if group['by'] not in bys:
# 				bys.append(group['by'])
# 				cells[group['by']] = []
# 			for cell in group['data']:
# 				if cell not in cells[group['by']]:
# 					out = []
# 					for p in group['data'][cell]:
# 						out.append(p[0] if xory == 'x' else p[1])
# 					cells[group['by']].append((cell, out))
# 		bys.sort()
#
# 		print 'Measure:\t%s' % (data[0][xory])
# 		for by in bys:
# 			print by
# 			cells[by].sort()
# 			for c in cells[by]:
# 				c[1].sort()
# 				print '%s\t%s' % (c[0], '\t'.join([self.stringify(v)[0] for v in c[1]]))
#
# 	def xys(self, args, data):
# 		print data
#
# 		# INTERNAL METHOD: STRINGIFY
# 	# Convert to string, return type, and force everything into 4 sig figs
# 	def stringify(self, s, vtype='unknown'):
# 		if isinstance(s, list) or isinstance(s, tuple):
# 			aout = []
# 			rtype = ''
# 			for el in s:
# 				nel, vtype = self.stringify(el, vtype)
# 				rtype = vtype if rtype == '' or rtype == 'int' and vtype == 'float' or vtype == 'str' else rtype
# 				aout.append(nel)
# 			return ('[' + ', '.join(aout) + ']', vtype)
# 		elif isinstance(s, float) or isinstance(s, int):
# 			if vtype == 'unknown': vtype = 'float' if isinstance(s, float) else 'int'
# 			elif vtype == 'int' and isinstance(s, float): vtype = 'float'
# 			return ('%.4g' % (s), vtype)
# 		else:
# 			if vtype == 'unknown': vtype = 'str'
# 			return (s.strip(), vtype)

	# Add the default arguments to the input arguments
	# Make sure that the arguments are the correct types
	def _setargs(self, args):
		out = {}
		# Set defaults of the argument
		for key in self.defaults:
			if type(self.defaults[key]) == list:
				out[key] = self.defaults[key][0]
			else: out[key] = self.defaults[key]

		# Add the input arguments and check them for correction
		for key in args:
			if key in self.defaults:
				if type(self.defaults) == list and args[key].lower() in self.defaults[key]:
					out[key] = args[key].lower()
				else:
					out[key] = args[key]
		return out
