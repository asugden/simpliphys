from importlib import import_module
#from copy import deepcopy
import os, types

from ABF import ABF
from Grapher import Grapher
from Analyzer import Analyzer
from types import MethodType

class PremadePlotter(Analyzer):
	def __init__(self, database, graphpath='2-graphs/'):
		self.db = database
		self.gwd = graphpath
		self.grapher = Grapher('.')

	defaults = {
		'type': [], # | 'bar' # Type of graph,
		'xmin': None,
		'xmax': None,
		'ymin': None,
		'ymax': None,
		'title': '',
		'xtitle': '',
		'ytitle': '',
		'font': ['Gotham', 'Helvetica Neue', 'Helvetica', 'Arial'],
		'half-width': False, # sets width to half of a slide
		'format': ['pdf', 'png'], # | 'png'
	}

	def load(self):
		if not hasattr(self, 'tans'):
			# The trace, file, and meta analyzers by screened protocol
			self.tans = self._linkanalyzers(self._getanalyzers('splots_trace'))
			for key in self.tans: self.defaults['type'].append(key)

			self.mans = self._linkanalyzers(self._getanalyzers('splots_meta'))
			for key in self.tans: self.defaults['type'].append(key)

  	# Analyze protocol p, return dict to be updated
	def chart(self, args, data):
		self.load()

		# Check both types of plotting
		tptype, tscreen, tcls = self._getplottype(args, self.tans)
		mptype, mscreen, mcls = self._getplottype(args, self.mans)
		screen = mscreen if tcls == -1 else tscreen

		# Kick back if no matching plot type was found
		if tcls == -1 and mcls == -1:
			if 'type' in args:
				print 'WARNING: plot type not found'
			print 'Plot -types are:\t%s' % ('\t'.join(self.defaults['type']))
			return False

		# Double-check that the arguments match
		args = self._setargs(args)

		# Get all protocols that match the screen
		ps = []
		cells = []
		for cell in data:
			if cell not in cells:
				cells.append(cell)
			for p in data[cell]:
				if self._compare_screen_protocol(screen, p):
					ps.append(p)

		if tcls == -1: self._chart_meta(args, ps, cells, mptype, mcls)
		else: self._chart_trace(args, ps, tptype, tcls)

	def _chart_trace(self, args, ps, ptype, cls):
		for p in ps:
			f, trs = self._getfiletraces(p, args)
			for tr in trs:
				args['save'] = self._save_path(ptype, p, tr)
				c = object.__new__(cls)

				# Inject methods such as getpair()
				setattr(c, '_analyzer_module', self)
				setattr(c, '_an_file', f)
				setattr(c, '_an_protocol', p)
				setattr(c, '_an_trace', tr)
				c.getpair = MethodType(self._get_pair_trace_for_modules, c)

				# Call init after injection
				c.__init__(args, f, p, tr, self.grapher)

	# Meta style charts are hard to deal with. None of the naming for save files is taken
	# care of.
	def _chart_meta(self, args, ps, cells, ptype, cls):
		if not os.path.exists(self.gwd + ptype):
			os.mkdir(self.gwd + ptype)

		c = object.__new__(cls)

		setattr(c, 'path', self.gwd+ptype+'/')
		setattr(c, '_analyzer_module', self)
		c.file = MethodType(self._get_file_for_modules, c)
		c.__init__(args, ps, cells, self.grapher)


	# Add the default arguments to the input arguments
	# Make sure that the arguments are the correct types
	def _setargs(self, args):
		out = {}
		# Set defaults of the argument
		for key in self.defaults:
			if type(self.defaults[key]) == list:
				out[key] = self.defaults[key][0]
			else: out[key] = self.defaults[key]

		# Add the input arguments and check them for correction
		for key in args:
			out[key] = args[key]
		return out

	# Get the plot type and return its name, the screening parameters, and the class
	def _getplottype(self, args, plotmodules):
		if 'type' in args:
			for key in plotmodules:
				if key == args['type'].strip().lower():
					return key, plotmodules[key]['screens'], plotmodules[key]['class']
		return '', [], -1

	def _save_path(self, type, p, tr):
		if not os.path.exists(self.gwd + type):
			os.mkdir(self.gwd + type)
		return '%s%s/%s-C%2i-T%2i' % (self.gwd, type, p['name'], p['cell-number'], tr)

	# Link all analyzer classes them with the appropriate protocols and what they set
	def _linkanalyzers(self, anclasses):
		out = {}
		for aname, c in anclasses:
			# Get the keys upon which the class is screened
			screens = getattr(c, 'screen')
			aname = aname.lower().replace('_', '-')
			out[aname] = {'screens': screens, 'class': c}
		return out

	# Check that all analyzer classes have the correct requirements
	def _screenanalyzer(self, module):
		classes = [(m, getattr(module, m)) for m in dir(module) if isinstance(getattr(module, m, None), type) and m[0] != '_']
		cutclasses = [(m, c) for m, c in classes if hasattr(c, 'screen') and 'protocol' in getattr(c, 'screen')]
		return cutclasses
