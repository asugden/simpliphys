import math, os, re
import numpy as np
from IOProtocols import getprotocols
from IOAnalysis import getanalysis, setanalysis
from Parser import Parser

class Metadata():
	def __init__(self, path_to_data='/Users/arthur/Documents/Lab-Connors/analysis/data/', path_to_analysis='1-analysis/'):
		self.data_path = path_to_data
		self.anls_path = path_to_analysis

		self.protocols = getprotocols(self.data_path)

		self.seld = np.array([])
		self.seltors = []
		self.upd = {}
		self.saved = {}

		self.loaded = []

	# ADD ANALYSIS KEY-VAL PAIRS
	# Update a protocol with a dict
	def update(self, protocol, keyvals):
		if keyvals == {}: return False
		#print '\tUpdating cell %s' % (protocol['cell-id'])
		#for key in keyvals: print '\t\t%s' % (key)
		ind = self._index(protocol)
		if ind < 0: print 'WARNING: Protocol not found for update'
		else:
			if ind not in self.upd: self.upd[ind] = []
			for key in keyvals:
				if key not in self.protocols[ind] or self.protocols[ind][key] != keyvals[key]:
					if key not in self.upd[ind]: self.upd[ind].append(key)
					self.protocols[ind][key] = keyvals[key]

	# Update all protocols that match the cell of a given protocol
	def updatecell(self, protocol, keyvals):
		for p in self.protocols:
			if p['cell-id'] == protocol['cell-id']:
				self.update(p, keyvals)

	# Save updated protocols to appropriate analysis files
	def saveanalysis(self):
		setanalysis(self.anls_path, self.protocols, self.upd)
		self.upd = {}

	# Load a particular type of analysis
	# Loading all analysis in advance is slow as all-git-out. Better to load when needed
	def load(self, p):
		type = p['experiment'].replace(' ', '-')
		if type not in self.loaded:
			self.loaded.append(type)
			self.protocols = getanalysis(self.anls_path, type, self.protocols)

	# Get all protocols from a cell
	def getcell(self, p):
		indices = self._cellsprotocols(p)
		out = [self.protocols[ind] for ind in indices]
		return out

	# SELECTORS
	# Select protocols by dict keywords and ranges
	def select(self, args): self.seld = self._select(args, 'select')
	def add(self, args): self.seld = np.union1d(self.seld, self._select(args, 'add'))
	def subset(self, args): self.seld = np.intersect1d(self.seld, self._select(args, 'subset'))
	def subtract(self, args): self.seld = np.setdiff1d(self.seld, self._select(args, 'subtract'))
	# What were the selectors for the files
	def selectors(self): return self.seltors

	# SAVING SELECTORS
	# Save selectors as groups
	def save(self, name=''):
		if name == '': return self.groups()
		print '\tSaved selected group as %s' % (name)
		self.saved[name] = np.copy(self.seld)

	def recall(self, name=''):
		if name == '': return self.groups()
		elif name in self.saved:
			print '\tRecalled selected group %s' % (name)
			self.seld = np.copy(self.saved[name])
		else: print '\tNo selected group by name %s' % (name)

	def groups(self):
		keys = []
		for key in self.saved: keys.append(key)
		keys.sort()
		if len(keys) == 0: print '\tNo saved selected groups'
		else:
			print '\tSaved groups:'
			for key in keys: print '\t\t%s' % (key)

	# SELECTED
	# List selected files by cell.
	# FIX: Add function
	def selected(self, searchkey=''):
		selectedcells = self.get_selected_unseparated()
		hidden = ['updated', 'skip', 'notes']

		out = {}
		for p in selectedcells:
			for key in p:
				if key not in hidden:
					if key not in out:
						out[key] = [p[key]]
					else:
						if p[key] not in out[key]:
							out[key].append(p[key])

		keylist = [key for key in out]
		keylist.sort()

		if len(searchkey) > 0 and searchkey in keylist:
			out[searchkey].sort()
			print '\tSelected %s:\n\t%s' % (searchkey.lower(), '\n\t'.join(map(str, out[searchkey])))
		else:
			for key in keylist:
				outlen = len(out[key])
				if outlen == 1: outstr = str(out[key][0])
				else:
					out[key].sort()
					outstr = ', '.join(map(str, out[key]))

				if len(outstr) < 120: print '\t%s: %s' % (key, outstr)
				else: print '\t%s: [%i entries] %s ... %s' % (key, outlen, outstr[:20], outstr[-20:])

	# SELECTABLE
	# List keys (and possibly vals) that can be selected
	def selectable(self):
		keylist = []
		for p in self.protocols:
			for key in p:
				if key not in keylist:
					keylist.append(key)
		keylist.sort()
		print 'Selectors:\t\t(use values to show possible values)'
		for key in keylist: print '\t%s' % (key)

	# VALUES
	# List values for specific key
	def values(self, key):
		values = []
		found = False
		for p in self.protocols:
			if key in p:
				found = True
				if p[key] != 'none' and p[key] not in values:
					values.append(p[key])

		if not found: print 'WARNING: selector %s not found' % (key)
		elif len(values) == 0: print 'No values found'
		else:
			values.sort()
			print '\tPossible values for selector %s:' % (key)
			for value in values:
				print '\t\t' + str(value)

	# GET SELECTED VALUES FOR PARTICULAR ARGUMENTS
	# List either unique arguments or multiple values per cell
	def selectedvalues(self, args):
		selectedcells = self.get_selected_unseparated()
		hidden = ['updated', 'skip', 'notes']
		parser = Parser()

		for key in args:
			out = {}
			for p in selectedcells:
				if key not in hidden and key in p:
					if p['cell-id'] not in out: out[p['cell-id']] = []
					if p[key] not in out[p['cell-id']]:
						out[p['cell-id']].extend(p[key]) if isinstance(p[key], list) else out[p['cell-id']].append(p[key])

			cells = sorted([c for c in out])
			if sum([len(out[c]) for c in cells]) == len(cells) and 'unique' not in args:
				parser.columns(list(set([out[c][0] for c in cells])), 'Selected %ss' % (key))
			else:
				print '\tSelected %ss' % (key)
				for cell in cells:
					parser.columns([i for i in out[cell]], 'Cell %s' % (cell), inset=1)

			print ''

	# Escaped paths
	# Return escaped paths for the subset of protocols matching arguments.
	def subsetpaths(self, args):
		if args == {}: ps = self.seld
		else: ps = np.intersect1d(self.seld, self._select(args, 'subset'))

		out = []
		for sel in ps: out.append(self.protocols[sel])

		cells = sorted(list(set([(p['cell-id'], os.path.dirname(p['path'])) for p in out])))
		return cells

	# SHOW
	# Respond with number of protocols selected
	def show(self): print '\tSelected {:g}/{:g} protocols'.format(len(self.seld), len(self.protocols))

	# GET SELECTED PROTOCOLS
	# Used by analyze
	def get_selected(self, arg='cell'):
		out = {}
		for sel in self.seld:
			if self.protocols[sel]['cell-id'] in out: out[self.protocols[sel]['cell-id']].append(self.protocols[sel])
			else: out[self.protocols[sel]['cell-id']] = [self.protocols[sel]]
		return out

	def get_selected_unseparated(self):
		return [self.protocols[sel] for sel in self.seld]


	# NAME
	# Return file name from path
	def name(self, path): # Get name from path or return if already name
		end = path.rfind('.') if path.rfind('.') > 0 else len(path)
		return path[path.rfind('/') + 1:end]

	# ==================================================================================
	# INTERNAL

	# SELECT PROTOCOLS BASED ON ARGUMENTS
	# Takes key, val pairs
	def _select(self, args, type):
		arglist = [key for key in args]
		self._selectors(args, type)
		out = self._selectarg(arglist[0], args[arglist[0]])
		for i in range(1, len(args)):
			out = np.intersect1d(out, self._selectarg(arglist[i], args[arglist[i]]))
		return out

	def _selectors(self, args, type):
		if type == 'select': self.seltors = []
		out = []
		for arg in args: out.append((arg, args[arg]))
		self.seltors.append(('+' if type == 'add' else '-' if type == 'subtract' else '', out))

	# INTERNAL METHOD: SELECT PROTOCOLS BY ARGUMENT
	# Subset of _select, selects based on single argument. Assumes all arguments in all files
	# FIX: make sure that it can handle mismatching exp/cell metadata files
	def _selectarg(self, key, val):
		out = []
		found = False
		# FIX

		if key == 'date':
			ymd = [[], [], []]
			for j in range(3):
				if isinstance(val[j], int):
					for i in range(len(self.protocols)):
						if self.protocols[i]['date'][j] == val[j]: ymd[j].append(i)
				else:
					for i in range(len(self.protocols)):
						if self.protocols[i]['date'][j] >= val[j][0] and self.protocols[i]['date'][j] <= val[j][0]: ymd[j].append(i)
			out = np.intersect1d(np.sort(np.array(ymd[0])), np.sort(np.array(ymd[1])))
			return np.intersect1d(np.sort(np.array(ymd[2])), out)
		elif key == 'nd' and type(val) == list:
			for i in range(len(self.protocols)):
				if key in self.protocols[i]:
					found = True
					if self.protocols[i][key] == val: out.append(i)
		elif isinstance(val, list) or isinstance(val, tuple):
			if isinstance(val[0], str) or len(val) > 2:
				for i in range(len(self.protocols)):
					if key in self.protocols[i]:
						found = True
						if self.protocols[i][key] in val: out.append(i)
			else:
				for i in range(len(self.protocols)):
					if key in self.protocols[i]:
						found = True
						if self.protocols[i][key] >= val[0] and self.protocols[i][key] <= val[1]: out.append(i)
		elif isinstance(val, str):
			for i in range(len(self.protocols)):
				if key in self.protocols[i]:
					found = True
					if val.lower() == str(self.protocols[i][key]).lower().replace(' ', '') or val.lower() == str(self.protocols[i][key]).lower(): out.append(i)
		else:
			for i in range(len(self.protocols)):
				if key in self.protocols[i]:
					found = True
					if self.protocols[i][key] == val: out.append(i)
		if found: return np.sort(np.array(out))
		else:
			print 'WARNING: %s not found in any protocol' % (key)
			return np.array([])

	# SETTING VALUES
	# Generate index of files for any updating
	def _index(self, protocol):
		if isinstance(protocol, dict): protocol = protocol['name'] + protocol['cell-id']
		if not hasattr(self, 'protocol_index'):
			self.protocol_index = {}
			for j, p in enumerate(self.protocols):
				self.protocol_index[p['name'] + p['cell-id']] = j
		if protocol in self.protocol_index: return self.protocol_index[protocol]
		else: return -1

	# Return pair protocol
	def pair(self, protocol):
		if protocol['pair'] > 0 and 'pair-id' in protocol:
			if not hasattr(self, 'protocol_index'): self._index(protocol)
			p = protocol['name'] + protocol['pair-id']
			if p in self.protocol_index: return self.protocols[self.protocol_index[p]]

		return False

	# Return first protocol for the same cell that matches keyvals
	def cellprotocol(self, protocol, keyvals):
		for p in self.protocols:
			if p['cell-id'] == protocol['cell-id']:
				matched = True
				for key in keyvals:
					if key not in p or p[key] != keyvals[key]:
						matched = False
				if matched: return p
		return False

	# Get all protocols for a cell
	def _cellsprotocols(self, protocol):
		if isinstance(protocol, dict): protocol = protocol['cell-id']
		if not hasattr(self, 'cell_index'):
			self.cell_index = {}
			for j, p in enumerate(self.protocols):
				if p['cell-id'] not in self.cell_index: self.cell_index[p['cell-id']] = [j]
				else: self.cell_index[p['cell-id']].append(j)
			self.all_cells = [key for key in self.cell_index]
		if protocol in self.cell_index: return self.cell_index[protocol]
		else: return []


if __name__ == '__main__':
	m = Metadata()
#	c = CLI()
#	c.cmdloop()
