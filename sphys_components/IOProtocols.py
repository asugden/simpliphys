# INGEST.py
# Reads in metadata, UGLY
# Last updated 7/25/14

import math, os, re
import numpy as np
from Parser import Parser
from datetime import date as dtdate

class Ingest():
	def __init__(self):
		self.parser = Parser()
		self.exprules = self._defparse('sphys_components/meta-experiment.txt')
		self.cellrules = self._defparse('sphys_components/meta-cell.txt')
		self.daterules = self._daterulesparse('sphys_components/meta-dates.txt')
		self.cells = []
		self.exps = []
		self.protocols = []

	def get(self, path):
		self._getfiles(path)
		self._combexps()
		return self.protocols

	# A few functions to check file by path
	def isabf(self, path): return True if path[-4:] == '.abf' else False # Is .abf file?
	def iscfs(self, path): return True if path[-4:] == '.cfs' else False # Is .cfs file?
	def isatf(self, path): return True if path[-4:] == '.atf' else False # Is .atf file?
	def iscell(self, path): return True if re.match('.*\/meta-([0-9]+)([AaBb]{1})?\.txt$', path) and not self.isexperiment(path) else False
	def isexperiment(self, path): return True if re.match('.*\/[0-9]{6}-([0-9]-)?meta\.txt$', path) else False
	def name(self, path): return path[path.rfind('/') + 1:path.rfind('.') if path.rfind('.') > 0 else len(path)] # Get name from path or return if already name

	# Retrieve all files with a recursive function based on the key directory from Metadata
	def _getfiles(self, path):
		fs = os.listdir(path)
		abfs = []
		cells = []
		for file in fs:
			file = path + file
			if self.isexperiment(file): self._addexperiment(file)
			elif self.iscell(file): cells.append(file)
			elif self.isabf(file) or self.iscfs(file) or self.isatf(file): abfs.append(file)
			elif os.path.isdir(file): self._getfiles(file + '/')
		if len(cells) > 0:
			for cell in cells: self._addcell(cell, abfs, len(cells))
		return self

	# Add a new experiment metadata file and check it against metadata defaults and rules
	def _addexperiment(self, path):
		vals = self._metaparse(path, self.exprules)
		vals = self._validate(vals, self.exprules)

		if 'errors' in vals:
			print 'ERROR: invalid experimental metadata for ' + path
			for er in vals['errors']: print '\t' + er[0] + ': ' + er[1]
			return

		vals['date'] = self.parser.date(path[path.rfind('/') + 1:path.rfind('/') + 7])
		vals['strdate'] = self.parser.strdate(vals['date'])

		if 'injected' in vals and vals['injected'] != (0, 0, 0):
			expdate = dtdate(vals['date'][0] + 2000, vals['date'][1], vals['date'][2])
			injdate = dtdate(vals['injected'][0] + 2000, vals['injected'][1], vals['injected'][2])
			ndays = expdate - injdate
			if ndays.days < 100:
				vals['expression-time'] = ndays.days

		self.exps.append(vals)
		return self

	# Add a new cell metadata file and check it against metadata defaults and rules
	# Clean protocols, add date, add amplifier and num from path, etc.
	def _addcell(self, path, fs, ncells=1):
		if len(fs) == 0:
			print 'ERROR: no files for ' + path
			return -1
		fs.sort()

		vals = self._metaparse(path, self.cellrules)
		cnm = self.name(path)[5:]
		if cnm[-1].lower() == 'a':
			vals['amplifier'] = 'A'
			num = int(cnm[:-1])
		elif cnm[-1].lower() == 'b':
			vals['amplifier'] = 'B'
			num = int(cnm[:-1])
		else: num = int(cnm)

		vals = self._validate(vals, self.cellrules)
		if 'errors' in vals:
			print 'ERROR: invalid metadata for ' + path
			for er in vals['errors']: print '\t' + er[0] + ': ' + er[1]
			return
		if len(fs) != len(vals['protocols']):
			print 'ERROR: protocol length mismatch for ' + path
			return

		prots = []
		bridge = vals['bridge']
		for i in range(len(fs)):
			if 'bridge' in vals['protocols'][i][1]: bridge = vals['protocols'][i][1]['bridge']
			date = self._dateparse(fs[i])
			try:
				date[0]
			except:
				print self.parser.filename(fs[i])
			prots.append({
				'name': self.parser.filename(fs[i]),
				'path': fs[i],
				'date': date[0],
				'file-number': date[1],
				'protocol': vals['protocols'][i][0].lower(),
				'bridge': bridge,
			})
			for key in vals['protocols'][i][1]: prots[-1][key] = vals['protocols'][i][1][key]
			if 'vclamp' in prots[-1]:
				prots[-1]['clamp'] = 'voltage'
				prots[-1]['clamped-at'] = prots[-1]['vclamp']
			else:
				prots[-1]['clamp'] = 'current'
				prots[-1]['clamped-at'] = 0

		vals['protocols'] = prots
		vals['cell-number'] = num
		vals['date'] = self._dateparse(fs[0])[0]
		vals['strdate'] = self.parser.strdate(vals['date'])
		vals['cell-id'] = vals['strdate'] + '-' + '%02i' % (vals['cell-number'])
		vals['has-pair'] = False

		self.cells.append(vals)
		return self

	# Add experiment data to cells and add protocols to self.protocols
	def _combexps(self):
		self.cellsbydate = {}
		for i in range(len(self.cells)):
			cell = self.cells[i]['strdate']
			if cell in self.cellsbydate: self.cellsbydate[cell].append(i)
			else: self.cellsbydate[cell] = [i]
		for exp in self.exps:
			if exp['strdate'] in self.cellsbydate:
				cells = self.cellsbydate[exp['strdate']]
				for cell in cells:
					if  exp['subset'] == [-1, -1] or (self.cells[cell]['cell-number'] >= exp['subset'][0] and self.cells[cell]['cell-number'] <= exp['subset'][1]):
						for key in exp:
							if key != 'subset': self.cells[cell][key] = exp[key]

		i = 0
		while i < len(self.cells):
			cell = self.cells[i]
			if cell['pair'] > 0 and i < len(self.cells) - 1 and self.cells[i+1]['pair'] == cell['pair']:
				pair = self.cells[i+1]
				for j in range(len(cell['protocols'])):
					n1 = self._keys_cell_to_protocol(cell, cell['protocols'][j])
					n2 = self._keys_cell_to_protocol(pair, pair['protocols'][j])

					n1, n2 = self._add_pair_to_protocols(n1, n2)
					if n1 != {}: self.protocols.append(n1)
					if n2 != {}: self.protocols.append(n2)
				i += 1
			else:
				for protocol in cell['protocols']:
					n = self._keys_cell_to_protocol(cell, protocol)
					if n != {}: self.protocols.append(n)
			i += 1

		return self

	# Takes a cell and protocol and mashes them together
	def _keys_cell_to_protocol(self, cell, protocol):
		out = {}
		skipkeys = ['protocols', 'bridge']
		for key in cell:
			if key not in skipkeys:
				out[key] = cell[key]
		for key in protocol:
			if key not in skipkeys:
				out[key] = protocol[key]
		if 'lost' in out and out['lost']: return {}
		if 'skip' in out and isinstance(out['skip'], int): out['skip'] = [out['skip']]
		out['protocol'] = out['protocol'].lower()
		return out

	# Adds pair information to protocols
	def _add_pair_to_protocols(self, p1, p2):
		if p1 != {} and p2 != {}:
			skip = []
			if 'skip' in p1: skip = np.union1d(skip, p1['skip'])
			if 'skip' in p2: skip = np.union1d(skip, p2['skip'])
			skip = [int(i) for i in skip]

			p1['has-pair'] = True
			p1['pair-id'] = p2['cell-id']
			p1['pair-type'] = p2['type']
			p1['pair-clamp'] = p2['clamp']
			p1['pair-skip'] = skip
			if 'vclamp' in p2: p1['pair-vclamp'] = p2['vclamp']

			p2['has-pair'] = True
			p2['pair-id'] = p1['cell-id']
			p2['pair-type'] = p1['type']
			p2['pair-clamp'] = p1['clamp']
			p2['pair-skip'] = skip
			if 'vclamp' in p1: p2['pair-vclamp'] = p1['vclamp']

		return (p1, p2)

	# Parse definition file
	def _defparse(self, path):
		fp = open(path, 'r')
		lines = fp.read().split('\n')
		fp.close()
		out = {}
		for line in lines:
			if len(line) > 0 and line.strip()[0] != '#' and line.find(':') > -1:
				line = filter(lambda x: False if x == '' or re.match('(:|\t| )', x) else True, re.split('(:| |\t)', line))
				if line[1].lower() == 'list': vals = {'type':'list', 'required':True if line[-1] == '[]' else False}
				else:
					vals = {'type': self.parser.str(line[1])}
					if len(line) > 2 and line[2][0] == '(' and line[2][-1] == ')': vals['range'] = getattr(self.parser, vals['type'] + '_range')(line[2][1:-1])
					if line[-1] == '[]': vals['required'] = True
					else:
						vals['required'] = False
						vals['default'] = getattr(self.parser, vals['type'])(line[-1][1:-1])
				out[line[0].lower()] = vals
		return out

	def _daterulesparse(self, path):
		daterules = []
		srch = re.compile('\[(Y|M|D|N)+\]')
		with open(path, 'r') as fp:
			for line in fp:
				rule = line.strip()
				if len(rule) > 0 and rule[0] != '#':
					pos = 0
					while re.search(srch, rule[pos:]):
						found = re.search(srch, rule[pos:])
						beg, end = pos + found.start(0), pos + found.end(0)
						rplstr = '(?P<%s>\d{%i})' % (rule[beg+1].lower(), end - beg - 2)
						rule = rule[:beg] + rplstr + rule[end:]
						pos = beg + len(rplstr)
					daterules.append(re.compile(rule))
		return daterules

	def _dateparse(self, path):
		rule = 0
		while rule < len(self.daterules):
			matched = re.search(self.daterules[rule], os.path.split(path)[1])
			if matched:
				d = matched.groupdict()
				return ((int(d['y'])%2000, int(d['m']), int(d['d'])), int(d['n']))
			rule += 1

	# Parse metadata file
	def _metaparse(self, path, rules):
		fp = open(path, 'r')
		lines = fp.read().split('\n')
		fp.close()

		vals = {}
		l = []
		listkey = ''
		for line in lines:
			if listkey != '' and line != '':
				if line.find(':') > -1:
					vals[listkey] = l
					l = []
					listkey = ''
				else: l.append(self.parser.keyvallist(line))

			if listkey == '' and line != '':
				line = line.split(':')
				if len(line) == 2 and line[1].strip().lower() != 'none':
					lkey = line[0].strip()
					if lkey in rules and rules[lkey]['type'] == 'list':
						listkey = lkey
						l.append(self.parser.keyvallist(line[1]))
					elif lkey in rules:
						vals[lkey] = getattr(self.parser, rules[lkey]['type'])(line[1])
					else:
						vals[lkey] = self.parser.unknown(line[1])

		if listkey != '': vals[listkey] = l
		return vals

	# Validate file with rules from definition file and add defaults
	def _validate(self, vals, rules):
		errors = []
		for key in rules:
			if key in vals and ('default' not in rules[key] or vals[key] != rules[key]['default']):
				if 'range' in rules[key]:
					if rules[key]['type'] == 'str':
						if vals[key].lower() not in rules[key]['range']: errors.append((key, 'Not in range'))
					elif vals[key] < rules[key]['range'][0] or vals[key] > rules[key]['range'][1]: errors.append((key, 'Not in range'))
			else:
				if rules[key]['required']: errors.append((key, 'Required, not found'))
				elif 'default' in rules[key]: vals[key] = rules[key]['default']
		if len(errors) > 0: return {'errors':errors}
		else: return vals

def getprotocols(path):
	i = Ingest()
	return i.get(path)
