# FILE.PY
# Contains everything to read the stfio file and complicated manipulations- spectrogram,
# correlation, spectrum, spike finding
# Inconsistent in seconds vs milliseconds. Input in ms, output in seconds
# Traces are 0 indexed

import numpy as np
import os, math, warnings
from suppress_stdout_stderr import suppress_stdout_stderr

# READFILE
# Initialize with path to abf file
class ABF:
	def __init__(self, path, skip=[], amp=0, clamp='voltage'):
		import stfio

		self.path = path
		if not os.path.exists(self.path):
			print 'ERROR: File %s not found'
			return

		with warnings.catch_warnings():
			warnings.simplefilter("ignore")
			with suppress_stdout_stderr():
				if path[-3:] != 'abf': self.r = stfio.read(path, ftype=path[-3:].lower())
				else: self.r = stfio.read(path)

		self.dt = self.r.dt if self.r.dt > 0 else 0.05
		self.skips = []
		self.traces = []
		self.amplifier(amp)
		self.clamp(clamp)
		self.rectypes = []
		self.read()

	# DATE
	# Get date by name of file
	# Deprecated
	#def date(self):
	#	dt = self.name(self.path).split('_')
	#	return {'year':int(dt[0])%2000, 'month':int(dt[1]), 'day':int(dt[2]), 'date':'%2i/%2i/%2i' % (dt[1], dt[2], dt[0]), 'osdate':os.path.getctime(self.path)}

	# NAME
	# Get name of file (remove path)
	def name(self, path=''):
		if path == '': path = self.path
		end = path.rfind('.') if path.rfind('.') > 0 else len(path)
		return path[path.rfind('/') + 1:end]

	# NUMBER OF TRACES
	# Get number of traces
	def n(self): return len(self.traces) - len(self.skips)

	# TRACES TO SKIP
	# Skip the following traces. Skipped traces will be inaccessible to all outside classes
	# Traces are 0-indexed
	def skip(self, n):
		if isinstance(n, int): self.skips = [n]
		else: self.skips = n
		return self

	# TRACES
	# Get the traces themselves
	def gettraces(self): return self.traces

	# AMPLIFIER
	# Set amplifier
	def amplifier(self, tell=''):
		"""Set the current amplifier to be read from or return the current amplifier.
		Currently limited to a maximum of 2 amplifiers.

		Tell can be an integer or either case of A and B. If tell is left blank, the
		function will return the current amplifier as an integer (so that mod math can be
		applied if desired).
		"""
		if isinstance(tell, int): self.amp = tell%2
		elif isinstance(tell, str) and len(tell) > 0:
			self.amp = 1 if tell.lower().strip()[0] == 'b' else 0
		else: return self.amp
		return self

	# GUESS AMPLIFIER
	# Guess which amplifier is being used
	def guessamplifier(self):
		self._rectypes()
		if np.amax(self.r[2][0].asarray()) > 100 or np.amin(self.r[2][0].asarray()) < -130: amp = 0
		elif np.amax(self.r[2][0].asarray()) > 100 or np.amin(self.r[2][0].asarray()) < -130: amp = 1
		elif np.std(self.r[2][0].asarray()) < 5 and np.std(self.r[3][0].asarray()) < 5: amp = 0
		elif np.std(self.r[0][0].asarray()) < 5 and np.std(self.r[1][0].asarray()) < 5: amp = 1
		else: amp = 2
		return 'A' if amp == 0 else 'B' if amp == 1 else 'AB'

	# CLAMP TYPE
	# Set clamp type as current or voltage
	def clamp(self, tell):
		if isinstance(tell, int): self._clamp = 'current' if tell%2 == 0 else 'voltage'
		else: self._clamp = 'current' if tell.strip().lower()[0] == 'i' or tell.strip().lower()[0] == 'c' else 'voltage'
		return self

	# GUESS CLAMP
	# Guess whether it is current or voltage clamp for the current amplifier
	def guessclamp(self):
		self._rectypes()
		if np.std(self.r[self.vrecs[self.amp]][0].asarray()[0:900]) < 0.15:
			guess = 'voltage'
		return guess

	# GET HOLDING POTENTIAL OR INJECTED CURRENT
	# Returns vclamp hold within 5, cclamp hold within 1
	def holding(self):
		if self._clamp != 'current' and self._clamp != 'voltage':
			self.clamp(clamptype)
		self._rectypes()

		if self._clamp == 'current':
			hold = int(round(np.mean(self.r[self.crecs[self.amp]][0].asarray()[:900])))
		else:
			hold = int(5*(round(np.mean(self.r[self.vrecs[self.amp]][0].asarray()[:900])/5)))
		return hold

	# READ TRACES
	def read(self, clamptype='', amplifier='', skip=[]):
		self._rectypes()
		if len(skip) > 0: self.skip(skip)
		self.traces = []

		if amplifier != '' or self.amp < 0 or self.amp > 1:
			self.amplifier(amplifier)
		if clamptype != '' or (self._clamp != 'current' and self._clamp != 'voltage'):
			self.clamp(clamptype)

		if self._clamp == 'current':
			tracen = self.vrecs[self.amp]
		else:
			tracen = self.crecs[self.amp]
		for i in range(len(self.r[tracen])):
			if i not in self.skips:
				self.traces.append(self.r[tracen][i].asarray())

		return self

	# BASELINE
	# Measure the baseline
	# Does not average
	def baseline(self, traceN=0, t=0.1, tunits='s', length=50):
		traceN = max(min(traceN, len(self.traces) - 1), 0) # clamp traceN to possible values
		if tunits == 's': t = t*1000 # Convert to ms
		tr = self._gettrange((max(t-length, 0), t), tunits='ms') # Convert to positions
		return (np.mean(self.traces[traceN][tr[0]:tr[1]]), np.std(self.traces[traceN][tr[0]:tr[1]])) # Return average

	# GET
	# Get a subset of a trace
	def get(self, traceN, trange, tunits='s'):
		traceN = max(min(traceN, len(self.traces) - 1), 0) # clamp traceN to possible values
		tr = self._gettrange(trange, tunits)
		return self.traces[traceN][tr[0]:tr[1]]

	# ==================================================================================
	# PLOTTING

	# PLOT
	# trace number (-1 is all, num is single, > length is average), time range in tunits, width in pixels (-1 is no simplification)
	def plot(self, traceN=0, trange=(-1, -1), width=1900, tunits='ms', lowpass=-1):
		tr = self._gettrange(trange, tunits)
		self.read()

		if traceN < 0 or traceN > len(self.traces):
			ts, out = self._getsimple(self.traces[0][tr[0]:tr[1]], width)
			for i in range(1, len(self.traces)):
				ts, out = np.add(out, self._getsimple(self.traces[i][tr[0]:tr[1]], width))
			if tunits == 's': return (ts*(self.dt/1000) + tr[0]*self.dt/1000, out/len(self.traces))
			else: return (ts*self.dt + tr[0]*self.dt, out/len(self.traces))
		else:
			if lowpass < 1: ts, out = self._getsimple(self.traces[traceN][tr[0]:tr[1]], width)
			else: ts, out = self._getsimple(self.lowpass(traceN, trange, tunits, freq=lowpass), width)
			if tunits == 's': return (ts*(self.dt/1000) + tr[0]*self.dt/1000, out)
			else: return (ts*self.dt + tr[0]*self.dt, out)

	# POWER SPECTRUM
	# time range in milliseconds, frequency range in Hz
	def powerspectrum(self, traceN=0, trange=(1000, 4000), frange=(10, 100), tunits='ms'):
		if tunits == 's': trange = (trange[0]*1000, trange[1]*1000)
		trange = self._gettrange(trange)

		if traceN < 0 or traceN > len(self.traces):
			freqs, power = self._getspectrum(self.traces[0][trange[0]:trange[1]], frange)
			for i in range(1, len(self.traces)):
				t, f, p = self._getspectrum(self.traces[i][trange[0]:trange[1]], frange)
				power = np.add(power, p)
			power = power/len(self.traces)
			return (freqs, power)
		else:
			return self._getspectrum(self.traces[traceN][trange[0]:trange[1]], frange)

	# SPECTROGRAM
	# which trace (-1 is all, num is single, > length is average), milliseconds per section (with 95% overlap), frequency range in Hz
	# Input in milliseconds (ms, trange), output in seconds (times)
	def spectrogram(self, traceN=-1, ms=500, frange=(10, 100), overlap=0.90, trange=(-1, -1), tunits='ms'):
		tr = self._gettrange(trange, tunits)

		if traceN < 0 or traceN > len(self.traces):
			times, freqs, power = self._getsgram(self.traces[0][tr[0]:tr[1]], ms, frange, overlap)
			for i in range(1, len(self.traces)):
				t, f, p = self._getsgram(self.traces[i][tr[0]:tr[1]], ms, frange, overlap)
				power = np.add(power, p)
			power = power/len(self.traces)
			times = times + tr[0]*self.dt/1000
			return (times, freqs, power)
		else:
			times, freqs, power = self._getsgram(self.traces[traceN][tr[0]:tr[1]], ms, frange, overlap)
			times = times + tr[0]*self.dt/1000
			return (times, freqs, power)

	# CORRELATION- CROSS OR AUTO, DEPENDING ON AMP
	# Gives the correlation
	def correlation(self, tr, amp1='a', amp2='b', clamp1='', clamp2='', trange=(1000, 4000), tunits='ms', highpass=3, width=1500):
		"""Return the time and the cross-correlation or autocorrelation, as determined by
		the amplifier value.

		Requires the trace number. Amp1 and Amp2 set the amplifiers for the correlation.
		They can both be set to the same amplifier for autocorrelation. Clamp1 and Clamp2
		set their respective clamps, current or voltage. If they are left blank, they
		default to 1. whatever the file was opened as, 2. the best guess, 3. current
		clamp. Trange and tunits set the time range and time units [ms, s, us],
		respectively. Highpass is a highpass forward-backward filter in Hz. Width allows
		you to simplify the number of points being printed to match a screen resolution.
		Set width to -1 to avoid simplification.

		Returns a tuple of the time in seconds and the cross-correlation."""

		# Save the current clamp to reset
		initclamp, initamp = self._clamp, self.amp
		trange = self._gettrange(trange, tunits)
		tr = self._gettracerange(tr)

		# Naturally select the type of clamp that the file was opened to
		if clamp1 == '': clamp1 = self._clamp
		if clamp2 == '': clamp2 = self._clamp

		# Extract the two traces to be compared
		self.amplifier(amp1).clamp(clamp1).read()
		tr1 = self.traces[tr]

		self.amplifier(amp2).clamp(clamp2).read()
		tr2 = self.traces[tr]

		# Pass those traces to the helper function that actually gets the correlation
		# and applies the highpass filter
		out = self._getcorr(tr1, tr2, trange, highpass)

		# Rest the original clamp and amplifier
		self._clamp = initclamp
		self.amp = initamp

		# Simplify the output number of points if necessary
		if width > -1:
			t, cor = self.simplify(out[0], out[1], width)
			out = (t, cor)

		return out

	# Outward is up, positive-wards
	# Inward is down, negative-wards
	# Direction can be 'outward, inward' or -1 or +1
	def fastpsc(self, traceN=-1, trange=(200, 300), tunits='ms', direction='outward', args={}):
		defargs = {
			'synaptic-delay-max': 15, # Delay in ms by which event must be found
			'synaptic-delay-min': 1,
			'min-sigma': 2.5, # Event must lie beyond min-sigma*standard deviations
			'min-current': 10, # Minimum current of PSC
			'min-dadt': 2, # Minimum change in lowpass filtered current, dA/dt required
			'min-dadt-n':3, # Must have min-dadt for n samples
			'skip-ms': 0, # Skip the first n milliseconds for stimulus artifact
			'flat-ms': 8, # End of PSC after the value has reached baseline for 10 ms
			'trange': (self._ms(trange[0], tunits), self._ms(trange[1], tunits)),
			'troubleshoot': True, # Turn on to print error messages
		}
		for key in args: defargs[key] = args[key]

		if isinstance(direction, int): polarity = direction/abs(direction)
		else: polarity = 1 if direction == 'outward' else -1

		# Get the search time range
		startms = defargs['trange'][0] + defargs['skip-ms']
		srch = self._gettrange((startms, startms+defargs['synaptic-delay-max']), tunits='ms')

		# Get baseline standard devation
		bl, std = self.baseline(traceN, trange[0], tunits=tunits)

		# Get lowpass data for finding dA/dt and force pscs to be positive
		lp = polarity*(self.lowpass(traceN, freq=1000) - bl)
		lpddt = lp[1:] - lp[:-1]
		# And get a running minimum of window min-dadt-n
		lpddtmin = np.zeros(len(lpddt))
		skip = int(math.floor(defargs['min-dadt-n']/2.0))
		for i in range(len(lpddt) - skip*2):
			lpddtmin[i+skip] = np.min(lpddt[i:i+2*skip+1])

		mincur = max(defargs['min-current'], std*defargs['min-sigma'])

		# Check if the event started too early
		startedearly = False
		if defargs['skip-ms'] < defargs['synaptic-delay-min']:
			badtr = self._gettrange((startms, defargs['trange'][0] + defargs['synaptic-delay-min']), tunits='ms')
			if np.max(np.abs(lpddtmin[badtr[0]:badtr[1]])) > defargs['min-dadt']: startedearly = True
			elif np.max(np.abs(lp[badtr[0]:badtr[1]])) > mincur: startedearly = True

		if defargs['troubleshoot']:
			if startedearly: print '\tPSC rejected for starting too early'
			if np.max(lpddtmin[srch[0]:srch[1]]) <= defargs['min-dadt']: print '\tPSC rejected for too low a change in current (dA/dt too small)'
			elif np.max(lp[srch[0]:srch[1]]) <= mincur or np.max(polarity*(self.traces[traceN][srch[0]:srch[1]] - bl)) <= mincur: print '\tPSC rejected for too low of an amplitiude'

		# Check for psc if not started early
		if not startedearly and np.max(lpddtmin[srch[0]:srch[1]]) > defargs['min-dadt']:
			if np.max(lp[srch[0]:srch[1]]) > mincur and np.max(polarity*(self.traces[traceN][srch[0]:srch[1]] - bl)) > mincur:
				defargs['start-in-samples'] = np.where(lpddtmin[srch[0]:srch[1]] > defargs['min-dadt'])[0][0]
				defargs['start-ms'] = startms
				defargs['baseline'] = bl
				defargs['stdev'] = std
				defargs['polarity'] = polarity
				tr = self._gettrange((startms, defargs['trange'][1]), tunits='ms')
				return self._measurepsc(polarity*(self.traces[traceN][tr[0]:tr[1]] - bl), lp[tr[0]:tr[1]], lpddt[tr[0]:tr[1]], defargs)


		return {
			'psc': False,
			'charge': 0,
			'charge-fit': 0,
			'baseline': bl,
			'baseline-stdev': std,
			'extreme': 0,
		}

	# Find the end of the PSC, its max, and fit it
	def _measurepsc(self, d, lp, lpddt, args):
		# Find where Running mean of lowpass data returns to baseline
		flatsamps = int(1.0/self.dt)
		smoothlp = np.convolve(lp, np.ones(flatsamps)/flatsamps)[flatsamps-1:]
		esbl = np.where(smoothlp[args['start-in-samples']:] <= 0)[0]

		# Find where running mean of lowpass derivative returns to 0
		flatsamps = int(args['flat-ms']/2/self.dt)
		smoothlpddt = np.convolve(lpddt, np.ones(flatsamps)/flatsamps)[flatsamps-1:]
		esddt = np.where(np.abs(smoothlpddt[args['start-in-samples']:]) < 0.001)[0]

		# Find the end of the event
		endsampbl = esbl[0] if len(esbl) > 0 else len(d)
		endsampddt = len(d)
		# Look for two 0s nearby each other
		if len(esddt) > 1:
			mindist = args['flat-ms']/4/self.dt
			maxdist = args['flat-ms']/self.dt

			i = 0
			fnd = np.where((esddt - esddt[i] >= mindist) & (esddt - esddt[i] <= maxdist))[0]
			while len(fnd) == 0 and i < len(esddt) - 1:
				i += 1
				fnd = np.where((esddt - esddt[i] >= mindist) & (esddt - esddt[i] <= maxdist))[0]

			if len(fnd) > 0: endsampddt = esddt[i]
		endsamp = args['start-in-samples'] + min(endsampbl, endsampddt)
		fitq, fit = self._fitpsc(d[:endsamp], args['synaptic-delay-max'])

		# Calculate the rise slope
		rsx = np.linspace(0, 0.5, int(0.5/self.dt))
		rsy = lp[args['start-in-samples']:args['start-in-samples'] + int(0.5/self.dt)]
		if len(rsy) != len(rsx): rs = 0
		else: rs = np.polyfit(rsx, rsy, 1)[0]

		return {
			'psc': True,
			'charge': np.sum(d[:endsamp])*self.dt/1000,
			'charge-fit': fitq,
			'end-calc': 'time-limit' if endsampbl == endsampddt and endsampbl == len(d) else 'ret-to-baseline' if endsampbl < endsampddt else 'flattened',
			'onset-ms': args['start-ms'] + fit['offset-ms'],
			'offset-ms': args['start-ms'] + endsamp*self.dt,
			'baseline': args['baseline'],
			'baseline-stdev': args['stdev'],
			'extreme': np.max(d[:endsamp]),
			'rise-phase-fit': fit['d1'],
			'fall-phase-fit': fit['d2'],
			'a-fit': fit['a'],
			'polarity': args['polarity'],
			'rise-slope': rs,
		}

	def _fitpsc(self, y, maxdelay=10):
		from scipy import optimize

		# Create X array in units of ms, not samples
		x = np.array([i*self.dt/1000. for i in range(len(y))])
		# Fit a curve based off of the equation we use in Neuron
		fitfun = lambda ad, x: np.where(x > ad[3], ad[0]*(np.exp(-(x - ad[3])/ad[1]) - np.exp(-(x - ad[3])/ad[2])), 0)
		errfun = lambda ad, x, y: fitfun(ad, x) - y if ad[2] < ad[1] and ad[3] < maxdelay/1000.0 else 9e9	# Error function
		# Actually fit using least square optimization
		adcalc, success = optimize.leastsq(errfun, np.array((np.max(y), 0.01, 0.001, 5.0/1000)), args=(x, y))
		area = adcalc[0]*(adcalc[1] - adcalc[2]) # integrated 0 to infinity, area is a(d1 - d2)
		return area, {'a': adcalc[0], 'd1': adcalc[1], 'd2': adcalc[2], 'offset-ms': adcalc[3]*1000.0}

	def plotpsc(self, a, d1, d2, onset, offset, width=1900):
		x = (np.arange(width, dtype=np.float32)/width)*(offset - onset)/1000
		y = a*(np.exp(-x/d1) - np.exp(-x/d2))
		x = x*1000 + onset
		return x, y

	# PSC
	# Calculate the peak value and the
	def psc(self, traceN=-1, trange=(200, 300), guess='outward', tunits='ms', skipms=0):
		from scipy import optimize

		startms, endms = self._ms(trange[0], tunits), self._ms(trange[1], tunits)

		bl, std = self.baseline(traceN, startms, tunits='ms')
		trange = self._gettrange((startms + skipms, endms), 'ms')

		mn = abs(np.min(self.traces[traceN][trange[0]:trange[1]]) - bl)
		mx = np.max(self.traces[traceN][trange[0]:trange[1]]) - bl

		if isinstance(guess, int): guess = 'outward' if guess >= 0 else 'inward'
		if guess == '': guess = 'inward' if mn > mx else 'outward'
		elif guess == 'inward' and mx > 2*mn and mx > 5*std: guess = 'outward'
		elif mn > 2*mx and mn > 5*std: guess = 'inward'

		def theoryfit(y, sec, direction): # trace, baseline
			x = np.array([i*(sec/len(y)) for i in range(len(y))])
			#fitfun = lambda ad, x: ad[0]*(np.exp(-x/ad[1]) - np.exp(-x/ad[2]))		# Fitting function
			#errfun = lambda ad, x, y: fitfun(ad, x) - y if ad[2] < ad[1] else 9e9	# Error function
			#adcalc, success = optimize.leastsq(errfun, np.array((500 if direction == 'inward' else -500, 0.01, 0.001)), args=(x, y))

			fitfun = lambda ad, x: np.where(x > ad[3], ad[0]*(np.exp(-(x - ad[3])/ad[1]) - np.exp(-(x - ad[3])/ad[2])), 0)
			errfun = lambda ad, x, y: fitfun(ad, x) - y if ad[2] < ad[1] and ad[3] < 0.01 else 9e9	# Error function
			adcalc, success = optimize.leastsq(errfun, np.array((500 if direction == 'inward' else -500, 0.01, 0.001, 0)), args=(x, y))
			area = adcalc[0]*(adcalc[1] - adcalc[2]) # integrated 0 to infinity, area is a(d1 - d2)
			return area, adcalc

		if mn > 4*std + bl or mx > 4*std + bl:
			fitq, pars = theoryfit(self.traces[traceN][trange[0]:trange[1]] - bl, (trange[1] - trange[0])*self.dt/1000, guess)
		else:
			fitq = 0
			pars = [0, 0, 0, 0]

		return {
			'charge': np.sum((self.traces[traceN][trange[0]:trange[1]] - bl)*self.dt/1000),
			'charge-fit': fitq,
			'charge-a': pars[0],
			'charge-d1': pars[1],
			'charge-d2': pars[2],
			'charge-offset-ms': pars[3]*1000,
			'baseline': bl,
			'baseline-stdev': std,
			'polarity': 1 if guess == 'outward' else -1,
			'extreme': mn if guess == 'inward' else mx,
		}


	# Measure psp or psc
	# Give baseline in terms of milliseconds
	# Polarity is negative or positive. 0 is no guess. Abs(polarity) > 1 is forced
	def psx(self, traceN, trange=(200, 300), tunits='ms', polarity=-1, baseline=4,
			minstdev=2, mincurrent=15, minvoltage=0.75, minwidthms=2, minsearchms=10,
			saferange=(-1,-1), safestdev=2.0, safeunits='ms'):
		if tunits == 's': trange = (trange[0]*1000, trange[1]*1000)
		bl, std = self.baseline(traceN, trange[0], tunits='ms', length=baseline)

		# Set the out parameters in case of a zero-sized psx
		out = {
			'charge': 0,
			'charge-fit': 0,
			'charge-a': 0,
			'charge-d1': 0,
			'charge-d2': 0,
			'charge-onset-ms': 0,
			'baseline': bl,
			'baseline-stdev': std,
			'polarity': polarity,
			'extreme': 0,
		}

		def boolpsx(obj, polarity, time_ms, safethreshold=0):
			from scipy import signal
			tr = obj._gettrange(time_ms, 'ms')
			searchw = int(minwidthms/2.0/obj.dt)

			found = -1
			vs = obj.traces[traceN]
			thresh = np.maximum(minvoltage if obj._clamp == 'current' else mincurrent, np.maximum(minstdev*std, safethreshold))
			sigs = signal.argrelextrema(vs[tr[0]:tr[1]], np.greater if polarity > 0 else np.less, order=searchw)[0]
			for s in sigs:
				if found < 0:
					s += tr[0]
					if polarity > 0 and np.min(vs[s-searchw:s+searchw]) > bl and vs[s] - bl > thresh: found = s
					elif polarity < 0 and np.max(vs[s-searchw:s+searchw]) < bl and vs[s] - bl < thresh: found = s

			return found

		if saferange != (-1, -1):
			if safeunits == 's': saferange = (saferange[0]*1000, saferange[1]*1000)
			sbl, sstdev = self.baseline(traceN, saferange[1], tunits='ms', length=saferange[1] - saferange[0])
			safethresh = safestdev*sstdev
		else: safethresh = 0

		if abs(polarity) > 1:
			if boolpsx(self, polarity, (trange[0], trange[1] if trange[1] < trange[0] + minsearchms else trange[0] + minsearchms), safethresh) < 0: return out
			polarity = int(polarity/abs(polarity))
		else:
			mnp = boolpsx(self, -1, (trange[0], trange[1] if trange[1] < trange[0] + minsearchms else trange[0] + minsearchms), safethresh)
			mxp = boolpsx(self, 1, (trange[0], trange[1] if trange[1] < trange[0] + minsearchms else trange[0] + minsearchms), safethresh)
			if mn < 0 and mx < 0: return out
			if polarity != 0:
				if polarity < 0 and mn < 0 and abs(self.traces[traceN][mx] - bl) < 5*std: return out
				elif polarity < 0 and abs(self.traces[traceN][mx] - bl) >= 5*std: polarity = 1
				elif polarity > 0 and mx < 0 and abs(self.traces[traceN][mn] - bl) < 5*std: return out
				elif polarity > 0 and abs(self.traces[traceN][mn] - bl) >= 5*std: polarity = -1
			else:
				polarity = -1 if mn > -1 and (mx < 0 or abs(self.traces[traceN][mn] - bl) > abs(self.traces[traceN][mx] - bl)) else 1

		strange = self._gettrange(trange, 'ms')
		extreme = abs(np.min(self.traces[traceN][strange[0]:strange[1]]) - bl) if polarity < 0 else np.max(self.traces[traceN][strange[0]:strange[1]]) - bl

		def theoryfit(y, len_in_secs, max_with_polarity): # trace, baseline
			from scipy import optimize
			x = np.array([i*(len_in_secs/len(y)) for i in range(len(y))])
			fitfun = lambda ad, x: np.where(x > ad[3], ad[0]*(np.exp(-(x - ad[3])/ad[1]) - np.exp(-(x - ad[3])/ad[2])), 0)
			errfun = lambda ad, x, y: fitfun(ad, x) - y if ad[2] < ad[1] and ad[3] < 0.01 else 9e9	# Error function
			adcalc, success = optimize.leastsq(errfun, np.array((max_with_polarity, 0.01, 0.001, 0)), args=(x, y))
			area = adcalc[0]*(adcalc[1] - adcalc[2]) # integrated 0 to infinity, area is a(d1 - d2)
			return area, adcalc

		fitq, pars = theoryfit(self.traces[traceN][strange[0]:strange[1]] - bl, (strange[1] - strange[0])*self.dt/1000, extreme*polarity)

		out['charge'] = np.sum((self.traces[traceN][trange[0]:trange[1]] - bl)*self.dt/1000)
		out['charge-fit'] = fitq
		out['charge-a'] = pars[0]
		out['charge-d1'] = pars[1]
		out['charge-d2'] = pars[2]
		out['charge-onset-ms'] = pars[3]*1000
		out['polarity'] = polarity
		out['extreme'] = extreme
		return out

	# SMOOTH
	# Odd function out-- takes input of a trace. This is necessary because file has sampling rate
	def smooth(self, data, freq=1000):
		window_length = int(1000/self.dt/freq) & (-2) + 1 # Force odd
		cutdata = np.r_[data[window_length - 1:0:-1], data, data[-1:-window_length:-1]]
		window = np.hamming(window_length)
		smoothed = np.convolve(window/window.sum(), cutdata, mode='valid')
		if len(smoothed) > len(data):
			difflen = len(smoothed) - len(data)
			smoothed = smoothed[int(math.floor(difflen/2.)):-int(math.ceil(difflen/2.))]
		return smoothed

	# FIND SPIKES
	# Find all spikes by deviations of width maxspikewidth (ms) minspikeheight (mV) above the background
	# Output in s
	def spikes(self, traceN=0, trange=(-1, -1), tunits='s', maxspikewidth=3, minspikeheight=10):
		from scipy import signal

		tr = self._gettrange(trange, tunits)
		ext = signal.argrelextrema(self.traces[traceN][tr[0]:tr[1]], np.greater)[0]
		d = int((float(maxspikewidth)/2)/self.dt)

		spikets = []
		for p in ext:
			if self.traces[traceN][tr[0] + p] >= self.traces[traceN][tr[0] + p - d] + minspikeheight and self.traces[traceN][tr[0] + p] >= self.traces[traceN][tr[0] + p + d] + minspikeheight:
				spikets.append(tr[0] + p)

		while len(spikets) > 1 and np.min(np.array(spikets)[1:] - np.array(spikets)[:-1]) < maxspikewidth/self.dt:
			minisi = np.argmin(np.array(spikets)[1:] - np.array(spikets)[:-1])
			topop = minisi if self.traces[traceN][spikets[minisi]] < self.traces[traceN][spikets[minisi + 1]] else minisi + 1
			spikets.pop(topop)

		return np.array(spikets)*self.dt/1000

	# MASK ESCAPE SPIKES IN VOLTAGE CLAMP
	# Find escape spikes via dv/dt
	def maskescapes(self, traceN, trange=(-1,-1), tunits='s'):
		pass

	# FIND AVERAGE FWHM
	# Takes spike times and calculates the average spike width (i.e. full-width at half maximum)
	# dV/dT = 5V/s
	def fwhm(self, traceN, times, tunits='s'):
		dvdt = 5
		dvdt = float(dvdt)*self.dt # convert from V/s to mV/sample

		if isinstance(times, float) or isinstance(times, int) or isinstance(times, np.float64): times = np.array([times])
		else: times = np.array(times)
		if tunits == 's': times = times*1000/self.dt
		elif tunits == 'ms': times = times/self.dt
		delta = 10/self.dt

		def moving_average(x, n): return np.convolve(x, np.ones((n,))/n)[(n - 1):]

		widths = []
		for t in times:
			diff = self.traces[traceN][t - delta:t - 2] - self.traces[traceN][t - delta - 1:t - 3]
			first = -np.argmax(diff[::-1] < dvdt)
			spikemin = self.traces[traceN][t + first - 2]
			halfheight = (self.traces[traceN][t] - spikemin)/2 + spikemin
			t1 = np.interp(halfheight, self.traces[traceN][t + first - 3:t+1], np.arange(-first + 3, -1, -1))

			last = np.argmax(self.traces[traceN][t:t + delta] < halfheight)
			t2 = np.interp(halfheight, self.traces[traceN][t:t + last + 2][::-1], np.arange(last + 2)[::-1])
			widths.append((t2 + t1)*self.dt)
		return np.average(widths)

	# FIND SPIKE HEIGHT OFF OF DVDT
	def spikeheight(self, traceN, t, tunits='s'):
		dvdt = 5
		dvdt = float(dvdt)*self.dt # convert from V/s to mV/sample

		t = t*1000/self.dt if tunits == 's' else t/self.dt if tunits == 'ms' else t

		delta = 10/self.dt
		diff = self.traces[traceN][t - delta:t - 2] - self.traces[traceN][t - delta - 1:t - 3]
		first = -np.argmax(diff[::-1] < dvdt)
		spikemin = self.traces[traceN][t + first - 2]

		return self.traces[traceN][t] - spikemin

	def lowpass(self, traceN, trange=(-1, -1), tunits='ms', freq=3000):
		from scipy import signal

		tr = self._gettrange(trange, tunits)
		b, a = signal.butter(1, freq/(1000/self.dt/2), btype='low')
		t1 = signal.filtfilt(b, a, self.traces[traceN])
		return t1[tr[0]:tr[1]]

	# FIND AVERAGE OF REGION
	# Super simple
	def average(self, traceN, trange, tunits='ms'):
		tr = self._gettrange(trange, tunits)
		return (np.average(self.traces[traceN][tr[0]:tr[1]]), np.std(self.traces[traceN][tr[0]:tr[1]]))

	# FIND MIN AND MAX OF SMOOTHED TRACE
	def minmax(self, traceN, trange, tunits='ms', smooth_frequency=1000):
		tr = self._gettrange(trange, tunits)
		smth = self.smooth(self.traces[traceN][tr[0]:tr[1]], smooth_frequency)
		return (np.min(smth), np.max(smth))

	# FIND ESCAPE SPIKES
	# Find all escape spikes, return longest safe region or mask regions. Mask regions IN SAMPLES, safe region IN MILLISECONDS
	def escapespikes(self, trace, masks=False, trange=(1000, 4000)):
		safetyzone = 1/self.dt # 1 ms converted into samples
		tran = self._getts(trange)

		if masks: tr = self.traces[trace]
		else: tr = self.traces[trace][tran[0]:tran[1]]

		mn = np.mean(tr)
		std = np.std(tr)
		spikes = [0]
		p = 0
		while p < len(tr) and np.argmax(tr[p:] < mn - 5*std) > 0:
			p1 = np.argmax(tr[p:] < mn - 5*std)
			p2 = np.argmin(tr[p1:] < mn - 5*std)
			if p2*self.dt < 5: spikes.append(p + p1 - safetyzone, p + p1 + p2 + safetyzone)
			p += p1 + p2

		if masks: return spikes[1:]
		else:
			spikes.append(len(tr))
			spikes = np.array(spikes)
			deltas = spikes[1:] - spikes[:-1]
			t = np.argmax(deltas)
			return (trange[0] + spikes[t]*self.dt, trange[0] + spikes[t]*self.dt)

	# FIND MEMBRANE TAU
	# Takes length in units of ms, returns tau and voltage difference
	def tau(self, traceNs, trange=(100, 700), tunits='ms', length=100):
		from scipy import optimize

		tr = self._gettrange(trange, tunits)
		if isinstance(traceNs, int): traceNs = [traceNs]
		n = len(traceNs)

		up = 0
		dn = 0
		trace = np.zeros(tr[1] - tr[0])
		for t in traceNs:
			bl, std = self.baseline(t, trange[0], tunits=tunits)
			up += np.average(self.traces[t][0:tr[0]])/n
			dn += np.average(self.traces[t][tr[1] - 100/self.dt:tr[1]])/n
			trace += self.traces[t][tr[0]:tr[1]]/n

		t = np.arange(tr[1] - tr[0])*self.dt
		fitfun = lambda tau, t, Vi, Vc: Vi + Vc*(1 - np.exp(-t/tau))
		errfun = lambda tau, t, Vi, Vc, y: fitfun(tau, t, Vi, Vc) - y
		taucalc, success = optimize.leastsq(errfun, np.array([10]), args=(t, up, dn - up, trace))

		return (float(taucalc), dn - up)

	def simplify(self, y, x=[], width=1500):
		"""
		Simplify a vector for printing without sacrificing accuracy at a fixed resolution.

		Give as input a vector and the resulting width that you would like to view. Think
		of width as a value in pixels that should be similar to the horizontal resolution
		of your screen. You can also give two vectors, x and y, and you will have the
		simplified x and y returned.

		The max and min of your vector are calculated for each position, so the resulting
		vector will be of length width*2.
		"""

		if len(x) > 0: x, y = y, x
		step = int(math.floor(len(y)/width))
		if step < 2: return y if len(x) == 0 else x, y

		ox, oy = [], []
		for i in range(0, len(y), step):
			sub = y[i:i+step]
			mni = np.argmin(sub)
			mxi = np.argmax(sub)
			if mni < mxi:
				oy.extend([sub[mni], sub[mxi]])
				if len(x) > 0: ox.extend([x[i+mni], x[i+mxi]])
			else:
				oy.extend([sub[mxi], sub[mni]])
				if len(x) > 0: ox.extend([x[i+mxi], x[i+mni]])

		oy.append(y[-1])
		if len(x) > 0: ox.append(x[-1])

		return np.array(oy) if len(x) == 0 else np.array(ox), np.array(oy)

	# ==================================================================================
	# INTERNAL

	# INTERNAL METHOD: GET SIMPLIFIED TRACE
	def _getsimple(self, trace, width):
		step = int(math.floor(len(trace)/width))
		if step < 2:
			return (np.array([i for i in range(len(trace))]), np.array(trace))
		else:
			t = [0]
			out = [trace[0]]
			t = []
			out = []
			for i in range(0, len(trace), step):
				sub = trace[i:i+step]
				mni = np.argmin(sub)
				mxi = np.argmax(sub)
				if mni < mxi:
					t.extend([i+mni, i+mxi])
					out.extend([sub[mni], sub[mxi]])
				else:
					t.extend([i+mxi, i+mni])
					out.extend([sub[mxi], sub[mni]])
			t.append(len(trace) - 1)
			out.append(trace[-1])
			return (np.array(t), np.array(out))

	# INTERNAL METHOD: GET SPECTRUM
	# Get power spectrum/periodogram
	def _getspectrum(self, trace, frange):
		from scipy import signal
		freqs, power = signal.periodogram(trace, 1000/self.dt)
		power = power[np.argmax(freqs >= frange[0]):np.argmin(freqs <= frange[1])]
		freqs = freqs[np.argmax(freqs >= frange[0]):np.argmin(freqs <= frange[1])]
		return (freqs, power)

	# INTERNAL METHOD: GET SPECTROGRAM
	# Spectrogram code
	def _getsgram(self, trace, ms, frange, overlap):
		import matplotlib.mlab as mlab

		power, freqs, times = mlab.specgram(trace, NFFT=int(ms/self.dt), Fs=int(1000./self.dt), noverlap=int((ms/self.dt)*overlap))
		power = np.delete(power, np.array([i for i in range(np.argmin(freqs <= frange[1]), len(power))]), 0)
		power = np.delete(power, np.array([i for i in range(0, np.argmax(freqs >= frange[0]))]), 0)
		freqs = freqs[np.argmax(freqs >= frange[0]):np.argmin(freqs <= frange[1])]
		return (times, freqs, power)

	# INTERNAL METHOD: GET CROSS CORRELATIONS
	def _getcorr(self, t1, t2, trange, highpass=-1):
		from scipy import signal

		if highpass > -1:
			b, a = signal.butter(1, highpass/(1000/self.dt/2), btype='high')
			t1 = signal.filtfilt(b, a, t1)
			t2 = signal.filtfilt(b, a, t2)

		t1 = t1[trange[0]:trange[1]]
		t2 = t2[trange[0]:trange[1]]

		t1 = (t1 - np.mean(t1))/(np.std(t1)*len(t1))
		t2 = (t2 - np.mean(t2))/np.std(t2)

		corr = np.correlate(t1, t2, mode='same')
		return (np.array([i*(self.dt/1000) - len(corr)/2*(self.dt/1000) for i in range(len(corr))]), corr)

	# INTERNAL METHOD: CONVERT TIME RANGE TO SAMPLES
	# Fix trange
	def _gettrange(self, trange, tunits='ms'):
		"""
		Convert a time range into sample numbers given a time range tuple and units.

		If either time point is unset or None, it will default to the first and last
		samples, respectively.
		"""

		if trange[0] == None: trange = (-1, trange[1])
		if trange[1] == None: trange = (trange[0], -1)
		if tunits == 's': trange = (trange[0]*1000, trange[1]*1000)

		t1 = trange[0]
		t2 = trange[1]
		t1 = 0 if t1 < 0 or t1 > len(self.traces[0])*self.dt else int(t1/self.dt)
		t2 = len(self.traces[0]) if t2 < 0 or t2 > len(self.traces[0])*self.dt else int(t2/self.dt)
		return (t1, t2)

	# INTERNAL METHOD: CONVERT TIME TO MILLISECONDS
	def _ms(self, t, tunits):
		"""Convert the input time to milliseconds given the input time and units."""
		if tunits == 'ms': return t
		if tunits == 'us': return t/1000.0
		if tunits == 's': return t*1000

	# Return the bounded trace range
	def _gettracerange(self, tr):
		"""Given the trace, tr, return a bounded range that is guaranteed to exist."""
		if tr < 0: return 0
		elif tr > len(self.traces) - 1: return len(self.traces) - 1
		else: return tr

	# INTERNAL METHOD: FIND RECORD TYPES: mV vs pA
	# Scan through each recording type and split voltage and current traces
	def _rectypes(self):
		if len(self.rectypes) == 0:
			self.rectypes = []
			self.crecs = []
			self.vrecs = []
			for i in range(len(self.r)):
				if self.r[i].yunits.lower().find('pa') > -1 or self.r[i].yunits.lower().find('pA') > -1:
					self.rectypes.append(1)
					self.crecs.append(i)
				elif self.r[i].yunits.lower().find('mv') > -1 or self.r[i].yunits.lower().find('mV') > -1:
					self.rectypes.append(0)
					self.vrecs.append(i)
				else:
					self.rectypes.append(-1)
		return self

	# INTERNAL METHOD: LIST TRACES TO ANALYZE
	def _getts(self, traceN=-1):
		if traceN < 0 or traceN > len(self.traces): return [i for i in range(len(self.traces))], []
		else: return [traceN], []

	# INTERNAL METHOD: AVERAGE OUTPUT IF NECESSARY
	def _average(self, results):
		if not isinstance(results, list) or len(results) < 2: return results[0]
		else:
			if isinstance(results[0], tuple):
				v1 = list(results[0])
				for i in range(1, len(results)): v1[-1] = np.add(v1[-1], results[i][-1])
				v1[-1] = v1[-1]/len(results)
				return v1
			else:
				v1 = results[0]
				for i in range(1, len(results)): v1 = np.add(v1, results[i])
				return v1/len(results)

if __name__ == '__main__':
	fl = '/Volumes/Gillie/A Lab Data/cx36ko/140520 Cx36KO Pair 12/2014_05_20_0140.abf'
	#fl = '/Volumes/Gillie/-Data/Cx36KO Gamma/140520 Cx36WT Pair 1/2014_05_20_0000.abf'
	f = ABF(fl)
#	f.spikes(6)
	f1, p1 = f.powerspectrum(traceN=1, trange=(1000, 1819.2), frange=(30, 80), tunits='ms')
	f2, p2 = f.powerspectrum(traceN=1, trange=(1819.2, 2638.4), frange=(30, 80), tunits='ms')
	f3, p3 = f.powerspectrum(traceN=1, trange=(1000, 2638.4), frange=(30, 80), tunits='ms')
	print f1, p1
	print f2, p2
	print f3, p3
