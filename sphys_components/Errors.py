class AnalysisError(Exception):
	"""Base class for errors in analysis-- usually the types of errors a user will see."""
	pass

class SaveError(AnalysisError):
	"""Error in saving data or preparing data for saving."""
	pass
