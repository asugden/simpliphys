import numpy as np
from copy import deepcopy


def allsingles(data):
	out = True
	for measurement in data:
		for cell in measurement['data']:
			if len(measurement['data'][cell]) > 1: out = False
	return out
		

def datatomatrix(data, xory=1, normalize=True, nanminusones=True):
	cells = []
	measurements = []
	for measurement in data:
		measure = measurement['y'] if xory else measurement['x']
		if measure not in measurements: measurements.append(measure)
		for cell in measurement['data']:
			if cell not in cells:
				cells.append(cell)
				
	cells.sort()
	measurements.sort()
	cindx = {}
	mindx = {}
	for i in range(len(cells)): cindx[cells[i]] = i
	for i in range(len(measurements)): mindx[measurements[i]] = i
	
	vs = np.ones((len(cells), len(measurements)))
	vs[:] = np.nan
	for measurement in data:
		m = mindx[measurement['y']] if xory else mindx[measurement['x']]
		for cell in measurement['data']:
			c = cindx[cell]
			vs[c, m] = measurement['data'][cell][0][xory] if measurement['data'][cell][0][xory] != -1 or not nanminusones else np.nan
			
	if normalize:
		mns = np.nanmin(vs, axis=0)
		mxs = np.nanmax(vs, axis=0)
		rngs = mxs - mns
		vs = vs - mns
		vs = vs/rngs
		
	return (cells, measurements, vs)

def firstonly(data):
	fndcells = []
	fndcindx = {}
	
	out = []
	for measurement in data:
		added = False
		for cell in measurement['data']:
			if cell not in fndcells:
				if not added:
					out.append(deepcopy(measurement))
					out[-1]['data'] = {}
					added = True
					
				fndcells.append(cell)
				out[-1]['data'][cell] = []
				fndcindx[cell] = len(out) - 1
				
	return (out, fndcindx)