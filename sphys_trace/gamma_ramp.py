import numpy as np
from collections import deque

# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class GammaRamp(object):
	def __init__(self, f, p, tr):
		# Set the range of frequencies that defines gamma oscillations
		self.gamma_range = (30, 80)
		# Set the size of the gamma power spectrum window, should avoid curvature
		self.gamma_peak_length_ms = 819.2
		# Set the size of the spectrogram window
		self.spectrogram_length_ms = 500
		# Set the minimum ratio of max spectrogram considered actual gamma
		self.spectrogram_continuity_min = 0.5

		self.fr = self._fullramp(f, p, tr)
		self.sr = self._scottramp16(f, p, tr)
		self.pbs = self._peakbandsubset(f, p, tr)
		self.gp = self._grampath(f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v', 'white ramp 0.5-5v', 'white ramp 5v', 'blue ramp 1-5v', 'blue ramp 0.5-5v', 'blue ramp 5v', 'white ramp 4v', 'scott ramp'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def gamma_max(self, f, p, tr): return self.pbs['max']
	def gamma_band(self, f, p, tr): return self.pbs['band']
	def gamma_mean(self, f, p, tr): return self.pbs['mean']
	def gamma_pbs_time(self, f, p, tr): return self.pbs['time']
	def gamma_max_freq(self, f, p, tr): return self.pbs['max-freq']

	def gamma_scott_max(self, f, p, tr): return self.sr['max']
	def gamma_scott_band(self, f, p, tr): return self.sr['band']
	def gamma_scott_mean(self, f, p, tr): return self.sr['mean']
	def gamma_scott_time(self, f, p, tr): return self.sr['time']

	def gamma_full_max(self, f, p, tr): return self.fr['max']
	def gamma_full_band(self, f, p, tr): return self.fr['band']
	def gamma_full_max_freq(self, f, p, tr): return self.fr['max-freq']

	def gamma_rhythm_max(self, f, p, tr): return self.gp['rhythm-max']
	def gamma_rhythm_mean(self, f, p, tr): return self.gp['rhythm-mean']

	def gamma_path_freq(self, f, p, tr): return self.gp['grampath-freq-diff']
	def gamma_path_length(self, f, p, tr): return self.gp['grampath-length']

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _times(self, p, tr):
		times = {
			'white ramp 1-5v': [(1.518, 4), (1.267, 4), (1.181, 4), (1.139, 4), (1.112, 4)],
			'white ramp 0.5-5v': [(1.717, 4), (1.518, 4), (1.371, 4), (1.267, 4), (1.209, 4), (1.181, 4), (1.154, 4), (1.139, 4), (1.132, 4), (1.112, 4)],
			'white ramp 4v': [(1.139, 4) for i in range(10)],
			'white ramp 5v': [(1.112, 4) for i in range(10)],
			'blue ramp 1-5v': [(1, 4) for i in range(5)],
			'blue ramp 0.5-5v': [(1, 4) for i in range(10)],
			'blue ramp 5v': [(1, 4) for i in range(10)],
			'scott ramp': [(4, 7) for i in range(30)],
		}
		return times[p['protocol']][tr]

	# Correctly sum the band-power by removing the /Hz normalization, summing, and adding
	# back the /Hz normalization
	def _bandpower(self, freqs, power):
		perhz = freqs[1] - freqs[0]
		bandhz = freqs[-1] - freqs[0]
		power = np.copy(power)*perhz
		return np.sum(power)/bandhz


	# Measure across the full gamma ramp
	def _fullramp(self, f, p, tr):
		freqs, power = f.powerspectrum(tr, trange=self._times(p, tr), frange=self.gamma_range, tunits='s')
		return {
			'max':np.max(power),
			'band':self._bandpower(freqs, power),
			'max-freq':freqs[np.argmax(power)]
		}

	# Measure from 1 second after ramp onset to 1 second + 1.6384 to match scott
	def _scottramp16(self, f, p, tr):
		origrange = self._times(p, tr)
		range16 = (origrange[0] + 1.0, origrange[0] + 2.6384)
		freqs, power = f.powerspectrum(tr, trange=range16, frange=self.gamma_range, tunits='s')
		return {
			'max':np.max(power),
			'band':self._bandpower(freqs, power),
			'time':range16,
			'mean':np.mean(power),
		}

	# Find the region of highest power of length gamma_peak_length_ms and measure it
	def _peakbandsubset(self, f, p, tr):
		times, freqs, power = f.spectrogram(tr, ms=self.gamma_peak_length_ms, frange=self.gamma_range, trange=self._times(p, tr), overlap=0.98, tunits='s')
		time = times[np.argmax(np.sum(power, axis=0))]
		ts = (time - self.gamma_peak_length_ms/2000, time + self.gamma_peak_length_ms/2000)

		freqs, power = f.powerspectrum(tr, trange=ts, frange=self.gamma_range, tunits='s')
		return {
			'time': ts,
			'band':self._bandpower(freqs, power),
			'max': np.max(power),
			'max-freq': freqs[np.argmax(power)],
			'mean': np.mean(power),
		}

	# Extract all parameters from the spectrogram including rhythmicity
	def _grampath(self, f, p, tr):
		times, freqs, power = f.spectrogram(tr, ms=self.spectrogram_length_ms, frange=self.gamma_range, trange=self._times(p, tr), overlap=0.95, tunits='s')
		shape = self._follow_grampath(times, freqs, power)

		band = np.sum(power, axis=0)
		max = np.max(power, axis=0)

		return {
			'rhythm-mean': np.mean(max/band),
			'rhythm-max': np.max(max/band),
			'grampath-freq-diff': shape['grampath-freq-diff'],
			'grampath-length': shape['grampath-length'],
		}

	# Extract the curvature, length, of the path of max power through the spectrogram
	# Gives two values-- the first is for saving, the second for plotting
	def _follow_grampath(self, times, freqs, power):
		def newf(power, t, f, lenfreqs):
			fmin = f - 3 if f - 3 > 0 else 0
			fmax = f + 3 if f + 3 < lenfreqs else lenfreqs - 1
			return np.argmax(power[fmin:fmax, t]) + fmin

		tmax = np.argmax(np.max(power, axis=0))
		fmax = np.argmax(power, axis=0)[tmax]
		max = np.max(power)

		pathfreqs = deque()
		pathpower = deque()
		pathfreqs.append(fmax)
		pathpower.append(max)

		outmax = max
		t = tmax
		f = fmax
		while t > 0 and outmax > self.spectrogram_continuity_min*max:
			f = newf(power, t-1, f, len(freqs))
			outmax = power[f][t-1]
			if outmax > self.spectrogram_continuity_min*max:
				pathfreqs.appendleft(f)
				pathpower.appendleft(outmax)
			t -= 1
		tmin = t + 1

		outmax = max
		t = tmax
		f = fmax
		while t < power.shape[1] - 1 and outmax > self.spectrogram_continuity_min*max:
			f = newf(power, t+1, f, len(freqs))
			outmax = power[f][t+1]
			if outmax > self.spectrogram_continuity_min*max:
				pathfreqs.append(f)
				pathpower.append(outmax)
			t += 1
		tmax = t - 1

		pathfreqs = np.array(pathfreqs)
		pathpower = np.array(pathpower)
		fdiffs = np.abs(pathfreqs[1:] - pathfreqs[:-1])
		pdiffs = np.abs(pathpower[1:] - pathpower[:-1])

		return {
			'grampath-freq-diff': np.mean(fdiffs),
			'grampath-length': float(len(pathfreqs))/power.shape[1]
		}#, {
		#	'grampath-freqs': freqs[pathfreqs],
		#	'grampath-freq-range': freqs[np.max(pathfreqs)] - freqs[np.min(pathfreqs)],
		#	'grampath-power-diff': np.mean(pdiffs)/max,
		#	'grampath-time-range': (times[tmin], times[tmax]),
		#}
