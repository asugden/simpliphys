import numpy as np

# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class BlueLaserMilliwatts(object):
#	def __init__(self, f, p, tr): pass
	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['blue pulses'],#, 'blue pulses no analog', 'blue ramp 1-5v', 'blue ramp 0.5-5v', 'blue ramp 5v'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def mw_laser_blue(self, f, p, tr):
		return self._3_11_2013_bluelaser(self._volts(p, tr), p['nd'])

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _3_11_2013_bluelaser(self, v, nd):
		laser = [
			[0.011, 0.023, 0.037], # All
			[0.073, 0.150, 0.238], # 4 16
			[0.51, 1.04, 1.68], # 8
			[3.82, 7.76, 12.6], # None
		]
		nds = [(4,8,16), (4,16), (8), ()]
		nd = self._nd(nd)
		return np.interp(v, [i*1.75 + 1.5 for i in range(3)], laser[nds.index(nd)])

	# BLUE LASER
	# Return the blue laser intensity in millwatts given volts and neutral density filters
	def _bluelaser(self, v, nd):
		# Values from 2012 calibration file for low mag. WRONG
		laser = [
			[0.001, 0.003, 0.004, 0.006, 0.008, 0.010, 0.012, 0.014, 0.017, 0.019, 0.021], # All
			[0.003, 0.008, 0.014, 0.020, 0.026, 0.032, 0.039, 0.046, 0.055, 0.062, 0.070], # 8 16
			[0.005, 0.016, 0.028, 0.040, 0.051, 0.062, 0.076, 0.090, 0.108, 0.122, 0.136], # 4 16
			[0.013, 0.039, 0.065, 0.094, 0.116, 0.151, 0.178, 0.210, 0.252, 0.283, 0.321], # 4 8
			[0.019, 0.057, 0.099, 0.143, 0.181, 0.222, 0.269, 0.321, 0.382, 0.437, 0.479], # 16
			[0.040, 0.118, 0.201, 0.292, 0.367, 0.451, 0.552, 0.657, 0.785, 0.888, 0.986], # 8
			[0.080, 0.235, 0.407, 0.584, 0.739, 0.913, 1.091, 1.322, 1.574, 1.784, 1.973], # 4
			[0.290, 0.840, 1.440, 2.050, 2.640, 3.260, 3.920, 4.700, 5.580, 6.370, 7.030], # None
		]
		nds = [(4,8,16), (8,16), (4,16), (4,8), (16), (8), (4), ()]
		nd = self._nd(nd)
		return np.interp(v, [i*0.5 for i in range(11)], laser[nds.index(nd)])


	def _volts(self, p, tr):
		voltages = {
			'blue pulses': [1.5, 3.25, 5],
			'blue pulses no analog': [5.21 for i in range(3)],
			'blue ramp 1-5v': [i+1 for i in range(5)],
			'blue ramp 0.5-5v': [(i+1)/2 for i in range(10)],
			'blue ramp 5v': [5 for i in range(10)],
		}
		return voltages[p['protocol']][tr]

	def _nd(self, n):
		out = []
		if isinstance(n, str):
			n = n.lower()
			if n.find('all') > -1: out = [4, 8, 16]
			if n.find('4') > -1: out.append(4)
			if n.find('8') > -1: out.append(8)
			if n.find('16') > -1: out.append(16)
			return tuple(out)
		elif isinstance(n, tuple): return n
		elif isinstance(n, int) and (n == 4 or n == 8 or n == 16): return (n)
		elif isinstance(n, list): return tuple(n)
		else: return ()
