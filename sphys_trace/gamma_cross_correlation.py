import numpy as np

# Updated: 150307
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class GammaRampCrossCorrelation(object):
	def __init__(self, f, p, tr):
		self.cc = {}
		self.cc = self._crosscorrelation(f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':[
			'white ramp 1-5v',
			'white ramp 0.5-5v',
			'white ramp 5v',
			'blue ramp 1-5v',
			'blue ramp 0.5-5v',
			'blue ramp 5v',
			'white ramp 4v',
			'scott ramp',
		],
		'has-pair':[True],
		'vclamp':[-40],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def gamma_crosscorr_max(self, f, p, tr):
		return self.cc['max'] if 'max' in self.cc else 0

	def gamma_crosscorr_max_t(self, f, p, tr):
		return self.cc['max-t'] if 'max-t' in self.cc else 99999

	def gamma_crosscorr_fwhm(self, f, p, tr):
		return self.cc['fwhm'] if 'fwhm' in self.cc else -1

	def gamma_crosscorr_sidelobe(self, f, p, tr):
		return self.cc['sidelobe'] if 'sidelobe' in self.cc else 0

	def pair_vclamp(self, f, p, tr):
		return self.cc['pair-vclamp'] if 'pair-vclamp' in self.cc else 99999


	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _times(self, p, tr):
		times = {
			'white ramp 1-5v': [(1.518, 4), (1.267, 4), (1.181, 4), (1.139, 4), (1.112, 4)],
			'white ramp 0.5-5v': [(1.717, 4), (1.518, 4), (1.371, 4), (1.267, 4), (1.209, 4), (1.181, 4), (1.154, 4), (1.139, 4), (1.132, 4), (1.112, 4)],
			'white ramp 4v': [(1.139, 4) for i in range(10)],
			'white ramp 5v': [(1.112, 4) for i in range(10)],
			'blue ramp 1-5v': [(1, 4) for i in range(5)],
			'blue ramp 0.5-5v': [(1, 4) for i in range(10)],
			'blue ramp 5v': [(1, 4) for i in range(10)],
			'scott ramp': [(4, 7) for i in range(30)],
		}
		return times[p['protocol']][tr]

	def _crosscorrelation(self, f, p, tr):
		import scipy.signal as signal

		amp1 = f.amplifier()
		amp2 = (amp1 + 1)%2

		t, ccorl = f.correlation(tr, amp1, amp2, trange=self._times(p, tr), tunits='s', highpass=3)
		mx = np.max(ccorl)
		mn = np.min(ccorl)

		polarity = -1 if abs(mn) > abs(mx) else 1
		mx = max(abs(mn), abs(mx))
		ccorl = polarity*ccorl

		maxp = np.where(ccorl == mx)[0][0]
		minLp = signal.argrelmin(ccorl[:maxp], order=30)
		minRp = maxp + signal.argrelmin(ccorl[maxp:], order=30)

		if len(minLp) == 0 or len(minRp) == 0 or len(minLp[0]) == 0 or len(minRp[0]) == 0:
			return {}

		minL = minLp[0][-1]
		minR = minRp[0][0]

		pairp, pairtr = self.getpair()
		pvcl = pairp['vclamp'] if 'vclamp' in pairp else 99999
		return {
			'max': polarity*mx,
			'max-t': t[np.argmax(ccorl)]*1000.0,
			'sidelobe': polarity*min(ccorl[minL], ccorl[maxp]),
			'fwhm': self._fwhm(t, ccorl, minL, maxp, minR)*1000.0,
			'pair-vclamp': pvcl,
		}

	def _fwhm(self, t, ccorl, p1, p2, p3):
		l = ccorl[p1]
		r = ccorl[p3]
		mx = ccorl[p2]

		hml = (mx - l)/2. + l
		hmr = (mx - r)/2. + r

		last = p1 + np.argmax(ccorl[p1:p2] < hml)
		y1 = np.interp(hml, [ccorl[last], ccorl[last+1]], [t[last], t[last+1]])

		last = p2 + np.argmin(ccorl[p2:p3] > hmr)
		y2 = np.interp(hmr, [ccorl[last], ccorl[last+1]], [t[last], t[last+1]])

		return y2 -  y1
