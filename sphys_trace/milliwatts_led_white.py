import numpy as np

# Updated: 141110
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class WhiteLEDMilliwatts(object):
	def __init__(self, f, p, tr): pass
	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v', 'white ramp 0.5-5v', 'white ramp 5v', 'white ramp 4v'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def milliwatts_led_white(self, f, p, tr):
		return self._whiteled750mA(self._volts(p, tr), p['nd'])

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	# BLUE LASER
	# Return the blue laser intensity in millwatts given volts and neutral density filters
	def _whiteled750mA(self, v, nd):
		led = [
			[0.004, 0.008, 0.011, 0.014, 0.017, 0.019, 0.021, 0.023, 0.025, 0.027], # All
			[0.029, 0.057, 0.08, 0.1, 0.119, 0.135, 0.15, 0.165, 0.178, 0.19], # 4, 16
			[0.057, 0.113, 0.159, 0.200, 0.236, 0.269, 0.299, 0.328, 0.355, 0.379], # 4, 8
			[0.19, 0.39, 0.55, 0.69, 0.82, 0.93, 1.04, 1.14, 1.23, 1.31], # 8
			[1.5, 2.9, 4.1, 5.2, 6.2, 7.0, 7.8, 8.6, 9.3, 9.9], # None
		]
		nds = [(4,8,16), (4,16), (4,8), (8), ()]
		nd = self._nd(nd)
		return np.interp(v, [i*0.5 for i in range(10)], led[nds.index(nd)])


	def _volts(self, p, tr):
		voltages = {
			'white ramp 1-5v': [i+1 for i in range(5)],
			'white ramp 0.5-5v': [(i+1)/2 for i in range(10)],
			'white ramp 5v': [5 for i in range(10)],
			'white ramp 4v': [4 for i in range(10)],
		}
		return voltages[p['protocol']][tr]

	def _nd(self, n):
		out = []
		if isinstance(n, str):
			n = n.lower()
			if n.find('all') > -1: out = [4, 8, 16]
			if n.find('4') > -1: out.append(4)
			if n.find('8') > -1: out.append(8)
			if n.find('16') > -1: out.append(16)
			return tuple(out)
		elif isinstance(n, tuple): return n
		elif isinstance(n, int) and (n == 4 or n == 8 or n == 16): return (n)
		elif isinstance(n, list): return tuple(n)
		else: return ()
