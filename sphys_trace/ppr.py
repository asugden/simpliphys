# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class PPR(object):
	def __init__(self, f, p, tr):
		self._ipsc(f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['paired pulses'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def paired_pulse_ratio(self, f, p, tr):
		return self.ipsc2pars['extreme']/self.ipsc1pars['extreme']

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _ipsc(self, f, p, tr):
		direction = self._ipsc_direction_from_potential(p)
		t1 = 200
		t2 = t1 + p['ipi']
		t3 = t2 + p['ipi']

		#self.ipsc1pars = f.fastpsc(tr, (t1, t2), 'ms', direction, args={'skip-ms':1})
		#self.ipsc2pars = f.fastpsc(tr, (t2, t3), 'ms', direction, args={'skip-ms':1})
		self.ipsc1pars = f.psc(tr, (t1, t2), direction, 'ms', skipms=1)
		self.ipsc2pars = f.psc(tr, (t2, t3), direction, 'ms', skipms=1)

	def _ipsc_direction_from_potential(self, p):
		return 'inward' if p['vclamp'] < -10 else 'outward'

