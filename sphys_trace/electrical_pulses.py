# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class ElectricalPulses(object):
	def __init__(self, f, p, tr):
		self._ipsc(f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['paired pulses', 'electrical pulses'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def electrical_pulses_nozeros_extreme(self, f, p, tr):
		return abs(self.ipsc1pars['extreme'])

	def estim_ua(self, f, p, tr):
		if 'estimthreshold' in p and 'tmultiple' in p:
			return p['tmultiple']*p['estimthreshold']
		elif 'threshold' in p:
			multipliers = [i+1 for i in range(10)]
			return p['threshold']*multipliers[tr]
		else:
			print '\tWARNING: No electrical stim value found for protocol %s. Check metadata.' % (p['name'])


	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _ipsc(self, f, p, tr):
		direction = self._ipsc_direction_from_potential(p)
		ipi = 50
		start = 200
		if p['protocol'] == 'paired pulses':
			ipi = min(50, p['ipi'])
			start = 300
		self.ipsc1pars = f.psc(tr, (start, start+ipi), direction, 'ms', skipms=1)

	def _ipsc_direction_from_potential(self, p):
		return 'inward' if p['vclamp'] < -10 else 'outward'

