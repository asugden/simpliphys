import numpy as np

# Updated: 141108
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class Stepsd50Trace(object):
	def __init__(self, f, p, tr):
		self.c = self._current(f, p, tr)
		self.pars = self._stepsd50(f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol': [['a steps d50', {'amplifier':'a'}], ['b steps d50', {'amplifier':'b'}]],
	}

	# Optional protocol-specific screen. Use a sub-dict for a specific screen.
	# Works the same way as "screen"

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def current(self, f, p, tr): return self.c

	def spike_n(self, f, p, tr): return self.pars['n']

	def spike_freq(self, f, p, tr): return self.pars['freq']

	def spike_freq_stdev(self, f, p, tr): return self.pars['freq-stdev']

	def spike_freq_adaptation(self, f, p, tr): return self.pars['freq-adaptation']

	def spike_height_stdev(self, f, p, tr): return self.pars['height-stdev']

	def spike_height_adaptation(self, f, p, tr): return self.pars['height-adaptation']

	def spike_width_trace(self, f, p, tr): return self.pars['width']

	def spike_height_trace(self, f, p, tr): return self.pars['spike-height']

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	# Return the times
	def _times(self): return (100, 700)

	# Get the current value for trace
	def _current(self, f, p, tr):
		# Switch to analyzing current
		f.read('voltage')

		# Average over the whole time
		cur = f.average(tr, self._times(), 'ms')[0] - f.average(tr, (0, self._times()[0]), 'ms')[0]

		# Current rounded to the nearest 10 picoamps
		cur = int(round(cur/10))*10

		# Switch back to current clamp
		f.read('current')
		return cur

	# By-trace measurements of the steps d50
	def _stepsd50(self, f, p, tr):
		out = {
			'n': 0,
			'freq': -1,
			'freq-stdev': -1,
			'freq-adaptation': -1,
			'height-stdev': -1,
			'height-adaptation': -1,
			'width': -1,
			'spike-height': -1,
		}

		# Return results if current < 0
		if self.c <= 0: return out

		# Get the spikes for the trace
		spikes = f.spikes(tr, self._times(), 'ms')
		out['n'] = len(spikes)
		if len(spikes) > 0: out['width'] = f.fwhm(tr, [spikes[0]])

		# Calculate nothing else if < 3 spikes
		if len(spikes) < 3: return out

		# Get the spike heights and set the easy stuff
		spikeheights = [f.spikeheight(tr, spike) for spike in spikes]
		out['freq-stdev'] = np.std(1.0/(spikes[1:] - spikes[:-1]))
		out['height-stdev'] = np.std(spikeheights)
		out['spike-height'] = spikeheights[0]

		# Try to combine the last spikes if possible
		if len(spikes) > 6:
			out['freq'] = 1.0/np.median(spikes[-3:] - spikes[-4:-1])
			out['freq-adaptation'] = out['freq']/(1.0/(spikes[1] - spikes[0]))
			out['height-adaptation'] = np.median(spikeheights[-3:])/spikeheights[0]
		else:
			out['freq'] = 1.0/np.median(spikes[1:] - spikes[:-1])
			out['freq-adaptation'] = (spikes[-1] - spikes[-2])/(spikes[1] - spikes[0])
			out['height-adaptation'] = spikeheights[-1]/spikeheights[0]

		return out

