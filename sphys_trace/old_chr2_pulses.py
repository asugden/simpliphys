# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class OldChR2Pulses(object):
	def __init__(self, f, p, tr):
		self._ipsc(f, p, tr)

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['blue pulses', 'blue pulses no analog'],
		'genotype':['somchr2', 'vgatchr2', 'pvchr2', 'vipchr2', 'gad2chr2'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def inh_pulses_nozeros_extreme(self, f, p, tr):
		return abs(self.ipscpars['extreme'])

	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _ipsc(self, f, p, tr):
		direction = self._ipsc_direction_from_potential(p)
		self.ipscpars = f.psc(tr, (200.5, 300), direction)

	def _ipsc_direction_from_potential(self, p):
		return 'inward' if p['vclamp'] < -10 else 'outward'

