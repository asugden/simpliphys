# Updated: 141023
# It is required that defined classes inherit from object. This allows for insertion of
# extra functions such as get_pair.
class TestPairing(object):
	def __init__(self, f, p, tr):
		# Set the range of frequencies that defines gamma oscillations
		self.gamma_range = (30, 80)
		# Set the size of the gamma power spectrum window, should avoid curvature
		self.gamma_peak_length_ms = 819.2
		# Set the size of the spectrogram window
		self.spectrogram_length_ms = 500
		# Set the minimum ratio of max spectrogram considered actual gamma
		self.spectrogram_continuity_min = 0.5

		print self.getpair()

	# ================================================================================== #
	# REQUIRED PARAMETER

	# Set the screening parameters by which protocols are selected
	# Screening by protocol is required. Any other screening is optional
	screen = {
		'protocol':['white ramp 1-5v'],
		'cell-id':['150302-11'],
	}

	# ================================================================================== #
	# KEYS TO SET
	# Any non-local functions (i.e. not beginning with an underscore) have _ replaced with
	# - and are set as keys

	def cross_correlation_peak(self, f, p, tr):pass


	# ================================================================================== #
	# LOCAL FUNCTIONS
	# Functions beginning with underscore are not set as keys for protocols

	def _times(self, p, tr):
		times = {
			'white ramp 1-5v': [(1.518, 4), (1.267, 4), (1.181, 4), (1.139, 4), (1.112, 4)],
			'white ramp 0.5-5v': [(1.717, 4), (1.518, 4), (1.371, 4), (1.267, 4), (1.209, 4), (1.181, 4), (1.154, 4), (1.139, 4), (1.132, 4), (1.112, 4)],
			'white ramp 4v': [(1.139, 4) for i in range(10)],
			'white ramp 5v': [(1.112, 4) for i in range(10)],
			'blue ramp 1-5v': [(1, 4) for i in range(5)],
			'blue ramp 0.5-5v': [(1, 4) for i in range(10)],
			'blue ramp 5v': [(1, 4) for i in range(10)],
			'scott ramp': [(4, 7) for i in range(30)],
		}
		return times[p['protocol']][tr]

