#!/usr/bin/env python
"""
Simpliphys is an interactive prompt that allows you to select subsets of data to analyze,
plot, or print. Modules can be added to sphys_trace, sphys_file, and sphys_cell.
"""
import cmd, os, readline
import numpy as np

from sphys_components.Analyzer import Analyzer
from sphys_components.Grapher import Grapher
from sphys_components.Parser import Parser
from sphys_components.Printer import Printer
from sphys_components.Tracer import Tracer
from sphys_components.Cluster import ClusterDP

from sphys_components.PremadePlotter import PremadePlotter

from sphys_components.Metadata import Metadata


class CLI(cmd.Cmd):
	"""
	Class CLI is the interactive program itself, dependent on Analyzer, Metadata, and
	Grapher modules, in addition to others.
	"""
	def __init__(self):
		cmd.Cmd.__init__(self)
		self.prompt = 'c> '
		self.intro  = 'Electrophysiological analysis. Ready, begin.'
		if os.path.exists('.physalize_history'): readline.read_history_file('.physalize_history')
		readline.set_history_length(500)
		self.db = Metadata() # Add path to metadata here!
		self.an = Analyzer(self.db, testing=True)
		self.parse = Parser()
		self.grapher = Grapher()
		self.pmplotter = PremadePlotter(self.db, '2-graphs/') # Add alternate path to graphs here!
		self.graphvalues = []
		self.labels = {}

		self._reminder()

	def _reminder(self):
#		print ''
		selectors = ['select', 'add', 'subset', 'subtract', 'save', 'recall', 'selectable', 'values', 'selected', 'open']
		commands = ['pull', 'clear', 'print', 'graph', 'cluster', 'points', 'label', 'quit', 'analyze', 'unlabel', 'trace', 'chart', 'open']
		self._print_columns(selectors, 'SELECTORS')
		self._print_columns(commands, 'COMMANDS')
		print ''

	def _print_columns(self, l, title=''):
		from math import ceil

		if len(l) < 1: return False
		if len(title) > 0: print '%s:' % (title)

		l.sort()
		column_width = max(len(word) for word in l) + 2  # padding
		ncolumns = 75/column_width
		rowed = [[l[i + ncolumns*j] for i in range(ncolumns) if len(l) > i + ncolumns*j] for j in range(int(ceil(float(len(l))/ncolumns) + 0.5))]

		for row in rowed:
			print '\t' + ''.join(word.ljust(column_width) for word in row)

	# ==================================================================================
	# SELECTORS
	def do_select(self, args):
		"""Select protocols by a list of arguments. Replaces current selection."""
		if len(args) == 0: self.db.selectable()
		else:
			self.db.select(self.parse.keyvals(args))
			self.db.show()
			self._check_analysis()

	def do_add(self, args):
		"""Select protocols by a list of arguments. Adds to current selection."""
		if len(args) == 0: self.db.selectable()
		else:
			self.db.add(self.parse.keyvals(args))
			self.db.show()
			self._check_analysis()

	def do_subset(self, args):
		"""Select protocols by a list of arguments. Subset of current selection."""
		if len(args) == 0: self.db.selectable()
		else:
			self.db.subset(self.parse.keyvals(args))
			self.db.show()

	def do_subtract(self, args):
		"""Removes protocols from current selection."""
		if len(args) == 0: self.db.selectable()
		else:
			self.db.subtract(self.parse.keyvals(args))
			self.db.show()

	def do_save(self, args):
		"""Save selected group by name. No argument shows saved groups."""
		self.db.save(args)

	def do_recall(self, args):
		"""Recall selected group by name. No argument shows saved groups."""
		self.db.recall(args)
		self.db.show()

	def do_selectable(self, args):
		"""List of selectable arguments."""
		self.db.selectable()

	def do_values(self, args):
		"""List values for specific selector."""
		if len(args) == 0: self.db.selectable()
		else: self.db.values(args.lower().strip())

	def do_selected(self, args):
		"""Show values for each selector for the selected protocols. If you add keys, it will print all of the values for that key. Use -unique flag to force listing by cell-id."""
		if len(args) == 0: self.db.selected(args)
		else:
			args = self.parse.keyvals(args)
			self.db.selectedvalues(args)

	def do_open(self, args):
		"""Opens the enclosing folders for all selected files. Subsets selected files with arguments. Specific to Macs."""
		from commands import getoutput
		from re import escape as rescape
		cells = self.db.subsetpaths(self.parse.keyvals(args))
		if len(cells) > 12:
			'\tWARNING: Too many paths to open. Select a subset no greater than one dozen.'
		else:
			for c in cells:
				print '\tOpening path to cell %s' % (c[0])
				print getoutput('open %s' % (rescape(c[1])))


	# Check how many of the selected files have been analyzed
	def _check_analysis(self):
		unaned, unset = self.an.check(self.db.get_selected())
		if unaned > 0:
			print '\t%i of which are unanalyzed' % (unaned)
			if len(unset) < 13:
				print ''
				self._print_columns(unset)

	# ==================================================================================
	# COMMANDS

	def do_analyze(self, args):
		"""Analyze all selected files. Automatically done by "pull". Use the -force flag to reanalyze files."""
		args = self.parse.keyvals(args)
		self.an.analyze(self.db.get_selected(), True if 'force' in args else False)
		self.db.show()

	def do_test(self, args):
		"""Runs all analysis modules prefixed 'test' and does not save the results."""
		args = self.parse.keyvals(args)
		self.an.analyze(self.db.get_selected(), test=True)

	def do_pull(self, args):
		"""Pull X and Y values for printing or graphing."""
			# List the default values for analysis
		default = {
			'trace': -1,
			'x': '1',
			'y': '',
			'color': 'default',
			'by': 'region',
			'xscale': 1,
			'yscale': 1,
			'force': False,
		}

		if len(args.strip()) > 2 and args.strip()[:3] == 'def': self._print_default(default)
		else:
			args = self.parse.keyvals(args)
			for key in args: default[key] = args[key]

			addtograph = self.an.pull(default, self.db.get_selected())
			if addtograph: self._addpulled(addtograph)

	def do_graph(self, args):
		"""Graph pulled results or xs and ys. Label individual cells with a different type and color with label. Type default for defaults."""
		g = Grapher('2-graphs/pullplots/')
		if len(args.strip()) > 2 and args.strip().lower()[:3] == 'def': self._print_default(g.defaults)
		else:
			args = self.parse.keyvals(args)
			if len(self.labels) > 0 and 'labels' not in args: args['labels'] = self.labels
			g.graph(args, self.graphvalues)

	def do_label(self, args):
		"""Assign labels or reassign labels. """
		if len(args.strip()) == 0: self._print_labels()
		else:
			args = self.parse.keyvals(args)
			for key in args:
				found = False
				for lkey in self.labels:
					if lkey == key:
						self.labels[key] = self.grapher.color(args[key])
						found = True

				if not found:
					self.labels[key] = self.grapher.color(args[key])
					print '\tAdded new label %s set to color %s' % (key, self.labels[key])
				else:
					print '\tReassigned label %s to color %s' % (key, self.grapher.color(args[key]))

	def do_unlabel(self, args):
		"""Strip all labels, equivalent to clear -labels."""
		self.labels = {}
		print '\tLabels cleared.'

	def do_points(self, args):
		from time import time # For data name in "do_points"

		"""Add points to be graphed. Set by to equal values to combine. Type default for defaults."""
		default = {
			'y':[],
			'x':[],
			'color':'gray',
			'by':''
		}
		args = args.strip()
		if len(args) > 2 and args[:3].lower() == 'def': self._print_default(default)
		else:
			args = self.parse.keyvals(args)
			if 'y' not in args: self._print_default(default)
			if self.parse.isnum(args['y']): args['y'] = [args['y']]
			if self.parse.isnum(args['x']): args['x'] = [args['x']]
			for key in args: default[key] = args[key]

			if len(args['y']) > 0:
				now = str(time())
				dpoint = 'd' + now
				by = now if default['by'] == '' else default['by']

				d = [{'data':{dpoint:[]}, 'color':default['color'], 'by':by}]
				if len(default['x']) > 0 and len(default['x']) == len(default['y']):
					for x, y in zip(default['x'], default['y']):
						d[0]['data'][dpoint].append((x, y))
				else: print '\tERROR: x and y differed in length'
				self._addpulled(d, True)

	# Clear the pulled points
	def do_clear(self, args):
		"""Clear pulled results. If labels in args, clear labels."""
		args = self.parse.keyvals(args)
		if 'labels' in args:
			self.labels = {}
			print '\tLabels cleared.'
		else:
			self.graphvalues = []
			print '\tPulled results cleared.'


	def do_cluster(self, args):
		"""Cluster the pulled data and return the labels."""
		cdp = ClusterDP()
		if args.strip()[:3] == 'def': self._print_default(cdp.defaults)
		else:
			args = self.parse.keyvals(args)
			args['data'] = self.graphvalues
			cdp.run(args)
			lbls, centers = cdp.get_clusters_by_id()
			self._add_labels(lbls)

			if lbls == {}: print '\tNo data to cluster'
			else:
				print '\tCenters of %i clusters, label:' % (len(centers))
				for c in centers:
					count = 0
					for lbl in lbls:
						if lbls[lbl] == c: count += 1
					print '\t\t%s (%2i entries): %s' % (centers[c], count, str(c))

	def do_print(self, args):
		"""Print a matrix of X, Y values for a single type of pulled value or print a matrix of just Xs or just Ys pulled on different values. Includes labels if they exist."""
		p = Printer(self.graphvalues, self.labels)
		if len(args.strip()) > 2 and args.strip().lower()[:3] == 'def': self._print_default(p.defaults)
		else:
			args = self.parse.keyvals(args)
			p.run(args)

	def do_chart(self, args):
		"""Plot premade plots for the selected files. Type defaults for types"""
		self.pmplotter.load()
		if len(args.strip()) > 2 and args.strip().lower()[:3] == 'def': self._print_default(self.defaults)
		else:
			args = self.parse.keyvals(args)
			self.do_analyze('')
			self.pmplotter.chart(args, self.db.get_selected())

	def do_trace(self, args):
		"""Show the trace for each of the paths. Scales correctly. Use defaults for defaults."""
		tr = Tracer()
		if args.strip()[:3] == 'def':
			self._print_default(tr.defaults)
		else:
			args = self.parse.keyvals(args)
			args = self.parse.cleanargs(args, tr.defaults)
			tr.run(args, self.db.get_selected())

	# Add pulled data and combine if possible
	def _addpulled(self, vals, comb=True):
		for nval in vals:
			i = 0
			found = -1
			while i < len(self.graphvalues) and found < 0 and comb:
				match = True
				for key in nval:
					if key != 'data' and nval[key] != self.graphvalues[i][key]: match = False
				if match:
					found = i
					for key in nval['data']: self.graphvalues[i]['data'][key] = nval['data'][key]
				i += 1

			if found < 0: self.graphvalues.append(nval)

		self._print_pulled()

	# Print pulled ata
	def _print_pulled(self):
		conds = len(self.graphvalues)
		cells = 0
		for subset in self.graphvalues:
			groupcells = 0
			for key in subset['data']: groupcells += 1
			print '\t{:2d} cells from group {:s} colored {:s}'.format(groupcells, subset['by'], subset['color'])
			cells += groupcells
		print '\tPulled results from {:d} total cells across {:d} conditions.'.format(cells, conds)

	# Print a list of default values
	def _print_default(self, default):
		print 'Options and default values'
		list = [key for key in default]
		list.sort()
		for key in list: print '\t' + str(key) + ': ' + str(default[key])

	# Print the list of labels
	def _print_labels(self):
		lbls = [key for key in self.labels]
		lbls.sort()
		print '\tName, label:'
		for key in lbls:
			print '\t\t%s: %s' % (key, self.labels[key])

	# Add a label to the labels
	def _add_labels(self, args):
		for key in args:
			self.labels[key] = args[key]


	# ==================================================================================
	# SYSTEM

	def do_exit(self, args):
		"""Exit POR CLI"""
		readline.write_history_file('.physalize_history')
		return -1

	def do_quit(self, args): return self.do_exit(args)
	def do_q(self, args): return self.do_exit(args)

	def do_shell(self, args):
		"""Shell command, map of !"""
		os.system(args)


if __name__ == '__main__':
	c = CLI()
	c.cmdloop()
